package com.epam.news.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 7/7/2016.
 */
public class ErrorListCreator {

    public static List<String> getErrorList(String error) {
        List<String> errors = new ArrayList<>();
        errors.add(error);
        return errors;
    }

}
