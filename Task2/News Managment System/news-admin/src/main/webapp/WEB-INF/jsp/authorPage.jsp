<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="newsContainer" style="padding: 20px; border: dashed black 1px; height: 83%; width: 75%;">

    <c:forEach var="element" items="${authors}">
        <c:if test="${empty element.expired}">
            <div style=" margin-bottom: 45px; margin-top: 45px;" id="div${element.id}">
                <label for="title" style="font-weight:bold"><spring:message code="author"/></label>
                <input id="title${element.id}" type="text" value="${element.name}" style="width:70%" disabled/>

                <form style="display: inline-block; " action="<c:url value="/updateAuthor?id=${element.id}" />"
                      method="post">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <input type="hidden" name="name" id="name${element.id}"/>
                    <span style="margin-left:20px;text-decoration: underline;"><button href
                                                                                       style="color: blue; text-decoration: underline; padding: 1px 1px; border: none; background-color: transparent; font-weight:bold; display: none"
                                                                                       class="editAuthor"
                                                                                       onclick="edit(${element.id})">
                        <spring:message code="button.update"/></button></span>
                </form>
                <form style="display: inline-block;" action="<c:url value="/deleteAuthor?id=${element.id}" />"
                      method="post">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <span><button type="submit" href
                                  style="color: blue;text-decoration: underline; border: none; padding: 1px 1px; font-weight:bold; background-color: transparent; display: none"
                                  class="editAuthor" onclick="expireAuthor(${element.id})"><spring:message
                            code="button.expire"/></button></span>
                </form>
                <span style="margin-left:10px; "><button href
                                                         style="color: blue;text-decoration: underline; border: none; padding: 1px 1px; font-weight:bold; background-color: transparent; display: none"
                                                         class="editAuthor" onclick="cancelEditAuthor(${element.id})">
                    <spring:message code="button.cancel"/></button></span>
                <span style="margin-left:20px"><button
                        style="color: blue; font-weight:bold;  border: none; background-color: transparent;"
                        onclick="editAuthor(${element.id})" class="viewAuthor">edit
                </button></span>
            </div>
        </c:if>
    </c:forEach>


    <div style=" margin-bottom: 45px; margin-top:60px">
        <form action="<c:url value="/addAuthor" />" method="post">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <label for="title" style="font-weight:bold"><spring:message code="author.add"/></label>
            <input id="title" type="text" name="authorName" value="" style="width:67%"/>
            <span style="margin-left:20px"><button
                    style="color: blue; font-weight:bold;  border: none; background-color: transparent;"><spring:message
                    code="save"/></button></span>
        </form>
    </div>


    <div class="pages" style="bottom: -10%">
        <div style="display: inline-block;">
            <ul class="pagination">
                <form action="<c:url value="/authorPage" />" method="get" id="pagingForm">
                    <c:forEach var="i" begin="1" end="${count}">
                        <li><button type="submit" onclick="setPage(this)" value="${i}" id = "button${i}">${i}</button></li>
                    </c:forEach>
                    <input type="hidden" name="page" id="pageNum">
                </form>
            </ul>
        </div>
    </div>
</div>
<script>
    $(function(){
        pagingUtil();
    });

    function editAuthor(name){
        var selector = '#div'+name+' .editAuthor';
        var selector2 = '#div'+name+' .viewAuthor';
        var selector3 = '#title' + name;
        $(selector).each(function(){
          $(this).show();
        });
        $(selector2).hide();
        $(selector2).hide();
        $(selector3).prop('disabled', false);

    }

    function edit(name) {
        var selector = '#name' + name;
        var selector2 = '#title' + name;
        $(selector).val($(selector2).val());
    }

    function cancelEditAuthor(name){
        var selector = '#div'+name+' .editAuthor';
        var selector2 = '#div'+name+' .viewAuthor';
        var selector3 = '#title' + name;
        $(selector).each(function(){
            $(this).hide();
        });
        $(selector2).show();
        $(selector3).prop('disabled', true);

    }
</script>
