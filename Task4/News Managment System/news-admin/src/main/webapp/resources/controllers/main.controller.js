(function () {
    'use strict';

    var newsApp = angular.module('newsApp');
    newsApp.controller('MainCtrl', ['$scope', 'Auth', '$rootScope', function ($scope, Auth, $rootScope) {
        init();

        function init() {
            $rootScope.user = Auth.isLoggedIn();
        }

        $scope.isLoggedIn = function () {
            return Auth.isLoggedIn();
        }

    }]);
})();

