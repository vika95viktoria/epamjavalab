package com.epam.news.service.impl;

import com.epam.news.dao.CommentDAO;
import com.epam.news.domain.Comment;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.CommentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Viktoryia_Khlystun on 5/31/2016.
 */
@Service
public class CommentServiceImpl implements CommentService {

    private static final Logger logger = Logger.getLogger(CommentServiceImpl.class);
    @Autowired
    private CommentDAO commentDAO;

    public void setCommentDAO(CommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

    /**
     * Add new comment to news
     *
     * @param newsId
     * @param comment
     * @throws ServiceException
     */
    public void addComment(long newsId, Comment comment) throws ServiceException {
        try {
            commentDAO.create(comment, newsId);
        } catch (DAOException e) {
            logger.error("Failed to add comment " + e);
            throw new ServiceException(e);
        }
    }

    /**
     * Delete comment from discussion
     *
     * @param comment
     * @throws ServiceException
     */
    public void deleteComment(Comment comment) throws ServiceException {
        try {
            commentDAO.delete(comment);
        } catch (DAOException e) {
            logger.error("Failed to delete comment " + e);
            throw new ServiceException(e);
        }
    }

    /**
     * Return comment info by id
     *
     * @param id
     * @return
     * @throws ServiceException
     */
    public Comment findCommentById(long id) throws ServiceException {
        Comment comment;
        try {
            comment = commentDAO.findCommentById(id);
        } catch (DAOException e) {
            logger.error("Failed to find comment by id " + id + " " + e);
            throw new ServiceException(e);
        }
        return comment;
    }

    /**
     * Edit comment text
     *
     * @param comment
     * @throws ServiceException
     */
    public void update(Comment comment) throws ServiceException {
        try {
            commentDAO.update(comment);
        } catch (DAOException e) {
            logger.error("Failed to update comment " + e);
            throw new ServiceException(e);
        }
    }
}
