(function () {
    'use strict';

    var newsApp = angular.module('newsApp');

    newsApp.factory('TagService', ['$http', '$q', function ($http, $q) {


        return {
            getTags: getTags,
            getCount: getCount,
            createTag: createTag,
            updateTag: updateTag,
            deleteTag: deleteTag
        };


        function getTags(page) {
            var deferred = $q.defer();
            $http.get('/admin/tagPage', {
                    params: {
                        page: page
                    }
                })
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Users');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }

        function getCount() {
            var deferred = $q.defer();
            $http.get('/admin/tagCount')
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching count of Tags');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }

        function createTag(tagName) {
            var deferred = $q.defer();
            $http.post('/admin/addTag', $.param({
                tagName: tagName
            }), {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while adding Tag ' + tagName);
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }

        function updateTag(tag) {
            var deferred = $q.defer();
            $http.post('/admin/updateTag', tag).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while updating Tag ' + tag.id);
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }

        function deleteTag(tag) {
            var deferred = $q.defer();
            $http.post('/admin/deleteTag', tag).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while deleting Tag ' + tag.id);
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }


    }]);
})();