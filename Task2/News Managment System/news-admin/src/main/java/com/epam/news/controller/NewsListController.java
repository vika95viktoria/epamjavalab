package com.epam.news.controller;

import com.epam.news.domain.Author;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.search.SearchCriteria;
import com.epam.news.service.AuthorService;
import com.epam.news.service.NewsService;
import com.epam.news.service.TagService;
import com.epam.news.util.RequestParser;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 7/4/2016.
 */
@Controller
public class NewsListController {

    private static final Logger logger = Logger.getLogger(NewsListController.class);
    @Autowired
    private NewsService newsService;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private TagService tagService;

    /**
     * Returns newsList page with list of news corresponding to incoming parameters such as page and/or search criteria
     *
     * @param session
     * @param page
     * @param authorName
     * @param tagNamesList
     * @return
     */
    @RequestMapping(value = {"/", "/newsList"}, method = RequestMethod.GET)
    public ModelAndView newsList(HttpSession session, @RequestParam(required = false) Integer page, @RequestParam(required = false, value = "author") String authorName, @RequestParam(required = false, value = "tag") String[] tagNamesList) {
        SearchCriteria searchCriteria = null;
        List<String> tagNames = new ArrayList<>();
        authorName = RequestParser.plusParser(authorName);
        if (tagNamesList != null) {
            tagNames = Arrays.asList(tagNamesList);
        }
        if (!tagNames.isEmpty() || authorName != null) {
            searchCriteria = new SearchCriteria();
            if (authorName != null && !"".equals(authorName)) {
                Author author = new Author();
                author.setName(authorName);
                searchCriteria.setAuthor(author);
            }
            if (!tagNames.isEmpty()) {
                List<Tag> tags = new ArrayList<>();
                for (String tagName : tagNames) {
                    Tag tag = new Tag();
                    tag.setName(tagName);
                    tags.add(tag);
                }
                searchCriteria.setTags(tags);
            }
        }

        ModelAndView model = new ModelAndView();
        List<News> newsList = new ArrayList<>();
        List<Tag> tagList = new ArrayList<>();
        List<Author> authorList = new ArrayList<>();
        int newsCount = 0;
        if (page != null) {
            page = page - 1;
        } else {
            page = 0;
        }
        try {
            if (searchCriteria != null) {
                newsList = newsService.searchForNews(searchCriteria, 4 * page, 4 * page + 5);
                newsCount = newsService.getCountOfNews(searchCriteria);
            } else {
                newsList = newsService.getNewsOrderedByComments(4 * page, 4 * page + 5);
                newsCount = newsService.getCountOfNews();
            }
            tagList = tagService.findAll();
            authorList = authorService.findAll();
        } catch (ServiceException e) {
            logger.error(e);
        }
        int pageCount = newsCount / 4;
        if (newsCount % 4 != 0) {
            pageCount++;
        }
        session.setAttribute("news", newsList);
        model.addObject("authors", authorList);
        model.addObject("tags", tagList);
        model.addObject("count", pageCount);
        model.setViewName("newsList");
        return model;
    }

    /**
     * Returns newsView page
     * @param id
     * @param var
     * @return
     */
    @RequestMapping(value = "/newsView", method = RequestMethod.GET)
    public ModelAndView openViewPage(@RequestParam java.lang.Long id, @RequestParam String var) {
        ModelAndView model = new ModelAndView();
        News news = new News();
        try {
            news = newsService.findNewsById(id);
        } catch (ServiceException e) {
            logger.error(e);
        }
        model.addObject("var", var);
        model.addObject("news", news);
        model.setViewName("newsView");
        return model;
    }

    /**
     * Deeletes news from database and redirect to the newsList page
     * @param newsIds
     * @return
     */
    @RequestMapping(value = "/deleteNews", method = RequestMethod.POST)
    public String addNews(@RequestParam(value = "deleteId", required = false) Long[] newsIds) {
        try {
            for (Long id : newsIds) {
                newsService.deleteNews(id);
            }
        } catch (ServiceException e) {
            logger.error(e);
        }
        return "redirect:/newsList";
    }
}
