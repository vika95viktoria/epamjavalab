<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:setBundle basename="pagecontent"/>
<div
        style="width: 95%; top: 2%; padding: 10px; position: relative; border: dashed black 1px; height: 82%; margin: 0 auto;">
    <div style="float: left">
        <a href style="color: blue;" onclick="goBack()"><fmt:message
                key="back"/></a>
    </div>
    <br>

    <div style="width: 98%; margin: 0 auto; padding-top: 20px">
        <span class="newsTitle">${news.title}</span> <span>( </span>
        <c:if test="${empty language || language=='EN'}">
            <span>by</span>
        </c:if>
        <c:forEach var="elem" items="${news.authors}" varStatus="loop">
            ${elem.name}
            <c:if test="${fn:length(news.authors) != loop.index+1}">
                <span>, </span>
            </c:if>
        </c:forEach>
        <span>)</span> <a href="" class="newsDate">${news.creationDate}</a> <br>
        <br> <br> <span> ${news.fullText} </span>

        <div class="clearfix">
            <div style="width: 50%; float: left;">
                <c:forEach var="elem" items="${news.comments}" varStatus="loop">
                    <br>

                    <a href="" style="float: left; color: black">${elem.creationDate}</a>
                    <br>

                    <div class="comment">
                        <span>${elem.text}</span>
                    </div>
                </c:forEach>
                <form action="news?command=postComment" method="post">
					<textarea style="width: 99%; margin-top: 20px;" rows="10"
                              name="comment">
</textarea>
                    <input type="hidden" name="newsId" value="${news.id}"/> <input
                        type="hidden" name="var" value="${var}">
                    <button type="submit" class="loginButton"
                            style="float: right; margin-top: 20px">
                        <fmt:message key="comment.post"/>
                    </button>
                </form>
            </div>
        </div>
    </div>
    <div>
        <c:if test="${var!=0}">
            <div style="float: left; margin-top: 40px;">
                <a
                        href="news?command=newsView&id=${sessionScope.news[var-1].id}&var=${var-1}"
                        style="color: blue;"><fmt:message key="previous"/></a>
            </div>
        </c:if>
        <c:if
                test="${news.id != sessionScope.news[fn:length(sessionScope.news)-1].id}">
            <div style="float: right; margin-top: 40px;">
                <a
                        href="news?command=newsView&id=${sessionScope.news[var+1].id}&var=${var+1}"
                        style="color: blue;"><fmt:message key="next"/></a>
            </div>
        </c:if>
    </div>
</div>

<script>
    function goBack() {
        window.history.back();
    }
</script>