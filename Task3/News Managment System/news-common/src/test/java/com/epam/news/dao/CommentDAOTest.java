package com.epam.news.dao;

import com.epam.news.domain.Comment;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Viktoryia_Khlystun on 6/3/2016.
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/testContext.xml"})
@DatabaseSetup(value = "/data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class CommentDAOTest {
    private static CommentDAO commentDAO;
    private static ApplicationContext context;


    @BeforeClass
    public static void setUpAuthorDAO() {
        context = new ClassPathXmlApplicationContext("testContext.xml");
        commentDAO = (CommentDAO) context.getBean("CommentDAO");
    }

    @AfterClass
    public static void closeContext() {
        ((ConfigurableApplicationContext) context).close();
    }


    @Test
    public void findCommentByNewsId() throws DAOException {
        List<Comment> comments = commentDAO.findCommentsByNewsId(1);
        assertEquals(comments.size(), 2);
    }

}
