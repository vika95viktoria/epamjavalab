package com.epam.news.util;

import com.epam.news.domain.Author;
import com.epam.news.domain.Comment;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 6/6/2016.
 */
public class BuilderTestInfo {

    /**
     * create list of news for testing service methods
     *
     * @return
     */
    public static List<News> buildNewsSet() {
        List<News> news = new ArrayList<>();
        News newsPiece = new News();
        Date date = new Date();
        newsPiece.setId(1);
        newsPiece.setTitle("New title");
        newsPiece.setShortText("Short text");
        newsPiece.setFullText("Full text");
        newsPiece.setCreationDate(date);
        newsPiece.setModificationDate(date);
        News newsPiece2 = new News();
        Date date2 = new Date();
        newsPiece2.setId(2);
        newsPiece2.setTitle("New title2");
        newsPiece2.setShortText("Short text2");
        newsPiece2.setFullText("Full text2");
        newsPiece2.setCreationDate(date2);
        newsPiece2.setModificationDate(date2);
        newsPiece.setAuthors(buildAuthorSet());
        newsPiece.setTags(buildTagSet());
        news.add(newsPiece);
        news.add(newsPiece2);
        return news;
    }

    /**
     * create list of tags for testing service methods
     *
     * @return
     */
    public static List<Tag> buildTagSet() {
        List<Tag> tags = new ArrayList<>();
        Tag tag = new Tag();
        tag.setId(1);
        tag.setName("Weather");
        Tag tag2 = new Tag();
        tag2.setId(2);
        tag2.setName("Cats");
        tags.add(tag);
        tags.add(tag2);
        return tags;
    }

    /**
     * create list of authors for testing service methods
     *
     * @return
     */
    public static List<Author> buildAuthorSet() {
        List<Author> authors = new ArrayList<>();
        Author author = new Author();
        author.setId(1);
        author.setName("John Doe");
        authors.add(author);
        return authors;
    }

    /**
     * create list of comments for testing service methods
     *
     * @return
     */
    public static List<Comment> buildCommentSet() {
        List<Comment> comments = new ArrayList<>();
        Date date = new Date();
        Comment comment = new Comment();
        comment.setId(1);
        comment.setText("Cool");
        comment.setCreationDate(date);
        Comment comment2 = new Comment();
        comment2.setId(2);
        comment2.setText("Nice");
        comment2.setCreationDate(date);
        comments.add(comment);
        comments.add(comment2);
        return comments;
    }

    /**
     * create list of ids for testing service methods
     *
     * @return
     */
    public static List<Long> buildSetOfMostCommenting() {
        List<Long> ids = new ArrayList<>();
        ids.add(2L);
        ids.add(1L);
        return ids;
    }

}
