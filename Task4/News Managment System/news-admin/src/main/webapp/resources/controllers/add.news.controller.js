(function () {
    'use strict';

    var newsApp = angular.module('newsApp');
    newsApp.controller('AddNewsPageController', ['$scope', 'NewsService', '$location', '$routeParams', 'NewsViewService', function ($scope, NewsService, $location, $routeParams, NewsViewService) {
        init();
        fetchInfo();

        var id = 0;

        function init() {
            $scope.inputTags = [];
            $scope.inputAuthors = [];
            $scope.errors = [];
            $scope.newsModel = new Object();
            $scope.newsModel.id = 0;
            if (typeof $routeParams.id !== "undefined") {
                id = $routeParams.id;
            }
            else {
                $scope.minDate = new Date();
            }
        }

        function fetchInfo() {

            if (id != 0) {
                NewsViewService.getNewsById(id)
                    .then(
                        function (d) {
                            $scope.newsModel.version = d.version;
                            $scope.newsModel.id = d.id;
                            $scope.newsModel.title = d.title;
                            $scope.newsModel.shortText = d.shortText;
                            $scope.newsModel.fullText = d.fullText;
                            $scope.newsModel.modificationDate = new Date(d.modificationDate);
                            $scope.newsModel.creationDate = new Date(d.creationDate);
                            angular.forEach($scope.inputTags, function (value) {
                                if (containValue(value, d.tags)) {
                                    value.ticked = true;
                                }
                            });
                            angular.forEach($scope.inputAuthors, function (value) {
                                if (containValue(value, d.authors)) {
                                    value.ticked = true;
                                }
                            });
                        },
                        function (errResponse) {
                            console.error('Error while fetching Tags');
                        }
                    );
            }
            NewsService.getAllTags()
                .then(
                    function (d) {
                        $scope.inputTags = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Tags');
                    }
                );

            NewsService.getAllAuthors()
                .then(
                    function (d) {
                        $scope.inputAuthors = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Users');
                    }
                );
        }

        function containValue(checkedValue, tags) {
            var flag = false;
            angular.forEach(tags, function (value) {
                if (value.name === checkedValue.name) {
                    flag = true;
                }
            });
            return flag;
        }

        $scope.createNews = function () {
            var tags = [];
            var authors = [];
            if ($scope.outputTags.length > 0) {
                angular.forEach($scope.outputTags, function (value) {
                    var obj = {id: value.id, name: value.name, version: value.version};
                    tags.push(obj);
                });
            }
            if ($scope.outputAuthors.length > 0) {
                angular.forEach($scope.outputAuthors, function (value) {
                    var obj = {id: value.id, name: value.name, version: value.version, expired: value.expired};
                    authors.push(obj);
                });
            }
            $scope.newsModel.tags = tags;
            $scope.newsModel.authors = authors;
            if ($scope.newsModel.id == 0) {
                NewsService.addNews($scope.newsModel)
                    .then(
                        function (d) {
                            $location.path("/newsView/" + d);
                        },
                        function (errResponse) {
                            $scope.errors = errResponse.data;
                            console.error('Error while adding news');
                        }
                    );
            } else {
                NewsService.editNews($scope.newsModel)
                    .then(
                        function (d) {
                            $location.path("/newsView/" + d);
                        },
                        function (errResponse) {
                            $scope.errors = errResponse.data;
                            console.error('Error while editing news');
                        }
                    );
            }
        }
    }]);
})();

