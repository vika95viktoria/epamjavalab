(function () {
    'use strict';

    var newsApp = angular.module('newsApp');
    newsApp.controller('NewsController', ['$scope', 'NewsService', '$rootScope', 'Auth', function ($scope, NewsService, $rootScope, Auth) {


        init();
        fetchInfo();


//// init function
        function init() {
            $scope.news = [];
            $scope.inputTags = [];
            $scope.inputAuthors = [];
            $scope.tagNamesList = [];
            $scope.tags = [];
            $scope.currentPage = 1;
            $rootScope.user = Auth.isLoggedIn();

        }

        function getNews(page) {
            if (typeof page !== "undefined") {
                $scope.currentPage = page;
            }
            NewsService.getNews(page, $scope.authorName, $scope.tagNamesList)
                .then(
                    function (d) {
                        var count = 1;
                        angular.forEach(d, function (value) {
                            value.position = count + ($scope.currentPage - 1) * 4;
                            value.searchAuthor = $scope.authorName;
                            value.searchTags = $scope.tagNamesList;
                            count = count + 1;
                        });
                        $scope.news = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching News');
                    });
            getCount();
        }

        function getCount() {
            NewsService.getCount($scope.authorName, $scope.tagNamesList)
                .then(
                    function (d) {
                        $scope.count = Math.ceil(d / 4);
                        $scope.newsCount = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching count of News');
                    }
                );
        }

        function fetchInfo() {
            getNews();

            NewsService.getAllTags()
                .then(
                    function (d) {
                        $scope.inputTags = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Tags');
                    }
                );

            NewsService.getAllAuthors()
                .then(
                    function (d) {
                        $scope.inputAuthors = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Users');
                    }
                );
        }


///   DOM event
///
        $scope.searchNews = function (page) {
            if (page != $scope.currentPage) {
                if (typeof page !== "undefined") {
                    $scope.currentPage = page;
                } else {
                    $scope.currentPage = 1;
                }
            }
            angular.forEach($scope.tags, function (value) {
                $scope.tagNamesList.push(value.name);
            });
            getNews(page, $scope.authorName, $scope.tagNamesList);
            $scope.tagNamesList = [];
        }

        $scope.resetFilter = function () {
            $scope.tagNamesList = [];
            angular.forEach($scope.inputTags, function (value) {
                value.ticked = false;
            });
            $scope.tags = [];
            $scope.authorName = '';
            getNews();
        }

        $scope.getIndex = function (elem) {
            return $scope.news.indexOf(elem);
        }

        $scope.deleteNews = function () {
            var toBeDeleted = [];
            angular.forEach($scope.news, function (element) {
                if (!!element.checked) toBeDeleted.push(element.id);
            })
            if (toBeDeleted.length > 0) {
                NewsService.deleteNews(toBeDeleted)
                    .then(
                        function (d) {
                            var count = 1;
                            angular.forEach(d, function (value) {
                                value.position = count + ($scope.currentPage - 1) * 4;
                                value.searchAuthor = $scope.authorName;
                                value.searchTags = $scope.tagNamesList;
                                count = count + 1;
                            });
                            $scope.news = d;
                            $scope.currentPage = 1;
                            getCount();
                        },
                        function (errResponse) {
                            console.error('Error while fetching Users');
                        }
                    );
            }
        }
    }]);
})();