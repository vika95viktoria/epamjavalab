package com.epam.news.dao;

import com.epam.news.domain.News;
import com.epam.news.model.NewsModelList;
import com.epam.news.search.SearchCriteria;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
public interface NewsDAO {
    News findNewsById(long id);

    long create(News news);

    long update(News news);

    void delete(News news);

    void delete(Long[] newsIds);

    List<NewsModelList> getMostCommenting(int start);

    int getCountOfNews();

    List<NewsModelList> getListOfNews(SearchCriteria searchCriteria, int start, int end);

    int getCountOfListNews(SearchCriteria searchCriteria);

    News findNewsByTitle(String title);

    News getNextNews(int position, int index);

    News getNextNews(SearchCriteria searchCriteria, int position, int index);
}
