(function () {
    'use strict';

    var newsApp = angular.module('newsApp');
    newsApp.controller('TagPageController', ['$scope', 'TagService', function ($scope, TagService) {
        init();
        fetchInfo();

        function init() {
            $scope.tags = [];
            $scope.currentPage = 1;
            $scope.editMode = false;
            $scope.error = null;
        }

        function fetchInfo() {
            getTags();
            getCount();
        }

        function getTags(page) {
            if (typeof page !== "undefined") {
                $scope.currentPage = page;
            }
            TagService.getTags(page)
                .then(
                    function (d) {
                        $scope.tags = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Tags');
                    }
                );
        }

        $scope.getInfo = function (page) {
            getTags(page);
        }
        function getCount() {
            TagService.getCount()
                .then(
                    function (d) {
                        $scope.count = Math.ceil(d / 7);
                    },
                    function (errResponse) {
                        console.error('Error while fetching count of Tags');
                    }
                );
        }

        $scope.turnToEditMode = function () {
            $scope.editMode = true;
        }
        $scope.turnOffEditMode = function () {
            $scope.editMode = false;
        }

        $scope.createTag = function () {
            TagService.createTag($scope.tagName)
                .then(
                    function (d) {
                        $scope.tagName = '';
                        getCount();
                    },
                    function (errResponse) {
                        console.error('Error while fetching count of Tags');
                    }
                );
        }
        $scope.updateTag = function (tag, name) {
            if (name != null && name != '') {
                tag.name = name;
                TagService.updateTag(tag)
                    .then(
                        function (d) {
                            angular.forEach($scope.tags, function (value) {
                                if (value.id === tag.id) {
                                    value.name = tag.name;
                                    value.version = value.version + 1;
                                }
                            });
                            $scope.editMode = false;
                            $scope.error = null;
                        },
                        function (errResponse) {
                            console.error('Error while fetching count of Tags');
                            $scope.error = errResponse.data[0];
                        }
                    );
            } else {
                $scope.error = "Field can't be empty"
            }
        }

        $scope.deleteTag = function (tag) {
            TagService.deleteTag(tag)
                .then(
                    function (d) {
                        var index = $scope.tags.indexOf(tag);
                        $scope.tags.splice(index, index + 1);
                        $scope.editMode = false;
                        $scope.error = null;
                        getCount();
                    },
                    function (errResponse) {
                        console.error('Error while deleting Tag');
                        $scope.error = errResponse.data[0];
                    }
                );
        }


    }]);
})();
