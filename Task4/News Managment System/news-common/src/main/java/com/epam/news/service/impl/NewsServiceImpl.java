package com.epam.news.service.impl;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.dao.CommentDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.dao.TagDAO;
import com.epam.news.domain.Comment;
import com.epam.news.domain.News;
import com.epam.news.model.NewsModel;
import com.epam.news.model.NewsModelList;
import com.epam.news.search.SearchCriteria;
import com.epam.news.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
@Transactional
@Component
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsDAO newsDAO;
    @Autowired
    private TagDAO tagDAO;
    @Autowired
    private AuthorDAO authorDAO;
    @Autowired
    private CommentDAO commentDAO;

    public void setNewsDAO(NewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }

    public void setTagDAO(TagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }

    public void setAuthorDAO(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    public void setCommentDAO(CommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }


    /**
     * Return ordered list of news with all info
     *
     * @return
     */

    public List<NewsModelList> getNewsOrderedByComments(int start) {
        return newsDAO.getMostCommenting(start);
    }


    /**
     * Create news with tags and authors
     *
     * @param newsModel
     */
    public long createNews(NewsModel newsModel) {
        News news = new News(newsModel);
        return newsDAO.create(news);
    }

    /**
     * Delete news and related records in database
     */
    public void deleteNews(Long[] newsIds) {
        if (newsIds.length > 1) {
            newsDAO.delete(newsIds);
        } else {
            newsDAO.delete(findNewsById(newsIds[0]));
        }
    }

    public News getNewsFromChain(SearchCriteria searchCriteria, int position, int index) {
        return newsDAO.getNextNews(searchCriteria, position, index);
    }

    public News getNewsFromChain(int position, int index) {
        return newsDAO.getNextNews(position, index);
    }

    /**
     * Return complete object of news with all related objects like tags, authors and comments
     *
     * @param newsId
     * @return
     */
    public News findNewsById(Long newsId) {
        News news = newsDAO.findNewsById(newsId);
        List<Comment> comments = commentDAO.findCommentsByNewsId(newsId);
        news.setComments(comments);
        return news;
    }

    /**
     * Return count of news
     *
     * @return
     */
    public int getCountOfNews() {
        return newsDAO.getCountOfNews();
    }

    /**
     * Return count of news according to search criteria
     *
     * @param searchCriteria
     * @return
     */
    public int getCountOfNews(SearchCriteria searchCriteria) {
        return newsDAO.getCountOfListNews(searchCriteria);
    }

    /**
     * Edit news info
     *
     * @param newsModel
     */
    public long updateNews(NewsModel newsModel) {
        News news = new News(newsModel);
        return newsDAO.update(news);
    }

    /**
     * return list of news fitted to serach parameters
     *
     * @param searchCriteria
     * @return
     */
    public List<NewsModelList> searchForNews(SearchCriteria searchCriteria, int start, int end) {
        return newsDAO.getListOfNews(searchCriteria, start, end);
    }

    /**
     * find news by it's title
     *
     * @param title
     * @return
     */
    public News findNewsByTitle(String title) {
        News news = newsDAO.findNewsByTitle(title);
        List<Comment> comments = commentDAO.findCommentsByNewsId(news.getId());
        news.setComments(comments);
        return news;
    }
}
