package com.epam.news.controller;

import com.epam.news.domain.Tag;
import com.epam.news.service.TagService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 7/4/2016.
 */

@Controller
public class TagController {

    @Autowired
    private TagService tagService;

    /**
     * returns tag page with list of tags corresponding to the page
     *
     * @param page
     * @return
     */

    @RequestMapping(value = "/tagPage", method = RequestMethod.GET)
    public ModelAndView tagPage(@RequestParam(required = false) Integer page) {
        ModelAndView model = new ModelAndView();
        int tagCount = 0;
        if (page != null) {
            page = page - 1;
        } else {
            page = 0;
        }
        List<Tag> tagList = tagService.findAll(7 * page);
        tagCount = tagService.getCountOfTags();
        int pageCount = tagCount / 7;
        if (pageCount * 7 < tagCount) {
            pageCount++;
        }
        model.addObject("tags", tagList);
        model.addObject("count", pageCount);
        model.setViewName("tagPage");
        return model;
    }

    /**
     * deletes tag from database and redirect to the tagPage
     * @param tag
     * @return
     */
    @RequestMapping(value = "/deleteTag", method = RequestMethod.POST)
    public String deleteTag(@ModelAttribute Tag tag, RedirectAttributes redirectAttributes) {
        try {
            tagService.delete(tag);
        } catch (IllegalArgumentException | ObjectNotFoundException e) {
            redirectAttributes.addFlashAttribute("error", "Tag has already been  deleted.");
        }
        return "redirect:/tagPage";
    }

    /**
     * Updates the value of tag and redirect to the tagPage
     * @param tag
     * @return
     */
    @RequestMapping(value = "/updateTag", method = RequestMethod.POST)
    public String updateTag(@ModelAttribute Tag tag, RedirectAttributes redirectAttributes) {
        try {
            tagService.save(tag);
        } catch (JpaOptimisticLockingFailureException | HibernateOptimisticLockingFailureException e) {
            redirectAttributes.addFlashAttribute("error", "Tag has been edited or deleted. Try again, please");
        }
        return "redirect:/tagPage";
    }

    /**
     * Adds new tag to the database and redirect to the tagPage
     * @param tagName
     * @return
     */
    @RequestMapping(value = "/addTag", method = RequestMethod.POST)
    public String addAuthor(@RequestParam String tagName) {
        Tag tag = new Tag();
        tag.setName(tagName);
        tagService.save(tag);
        return "redirect:/tagPage";
    }
}
