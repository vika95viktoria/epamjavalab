package com.epam.news.controller;

import com.epam.news.exception.NewsConcurrentModificationException;
import com.epam.news.model.NewsModel;
import com.epam.news.service.NewsService;
import com.epam.news.util.NewsCreateValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.hibernate5.HibernateOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 7/4/2016.
 */
@Controller
public class NewsController {

    @Autowired
    private NewsService newsService;

    /**
     * Adds news to database and redirect to newsView page without links to previous and next news
     *
     * @param newsModel
     * @return
     */
    @RequestMapping(value = "/addNews", method = RequestMethod.POST)
    public ResponseEntity addNews(@RequestBody NewsModel newsModel) {
        NewsCreateValidator validator = new NewsCreateValidator();
        List<String> errors = validator.validate(newsModel);
        if (!errors.isEmpty()) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(errors);
        } else {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(newsService.createNews(newsModel));
        }
    }

    /**
     * Edits existing record in database and redirect to newsView page with links to previous and next news
     *
     * @param newsModel
     * @return
     */
    @RequestMapping(value = "/editNews", method = RequestMethod.POST)
    public ResponseEntity editNews(@RequestBody NewsModel newsModel) throws NewsConcurrentModificationException {
        NewsCreateValidator validator = new NewsCreateValidator();
        List<String> errors = validator.validate(newsModel);
        long id;
        if (!errors.isEmpty()) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(errors);
        } else {
            try {
                id = newsService.updateNews(newsModel);
            } catch (JpaOptimisticLockingFailureException | HibernateOptimisticLockingFailureException e) {
                throw new NewsConcurrentModificationException(e);
            }
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(id);
        }


    }

    @ExceptionHandler(NewsConcurrentModificationException.class)
    public ResponseEntity handleCustomException(NewsConcurrentModificationException ex) {
        List<String> errors = new ArrayList<>();
        errors.add("News have been edited or deleted. Reload page and try again, please");
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(errors);

    }


}
