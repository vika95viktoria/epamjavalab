package com.epam.news.controller;

import com.epam.news.domain.Author;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;
import com.epam.news.service.NewsService;
import com.epam.news.service.TagService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 6/20/2016.
 */

@Controller
public class AdminController {

    private static final Logger logger = Logger.getLogger(AdminController.class);
    @Autowired
    private NewsService newsService;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private TagService tagService;

    /**
     * Returns login page, in case of wrong credentials displays an error
     *
     * @param error
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error) {
        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid username and password!");
        }
        model.setViewName("home");
        return model;

    }

    /**
     * Opens page for adding news
     * @return
     */
    @RequestMapping(value = "/addNews", method = RequestMethod.GET)
    public ModelAndView openAddNewsPage() {
        ModelAndView model = new ModelAndView();
        List<Author> authorList = new ArrayList<>();
        List<Tag> tagList = new ArrayList<>();
        try {
            authorList = authorService.findAll();
            tagList = tagService.findAll();
        } catch (ServiceException e) {
            logger.error(e);
        }
        model.addObject("authors",authorList);
        model.addObject("tags",tagList);
        model.setViewName("addNews");
        return model;
    }

    /**
     * Opens page for editing news with prefilled fields with news info
     * @param id
     * @param var
     * @return
     */

    @RequestMapping(value = "/editNews", method = RequestMethod.GET)
    public ModelAndView openEditPage(@RequestParam Long id, @RequestParam String var) {
        ModelAndView model = new ModelAndView();
        List<Author> authorList = new ArrayList<>();
        List<Tag> tagList = new ArrayList<>();
        News news = new News();
        try {
            news = newsService.findNewsById(id);
            authorList = authorService.findAll();
            tagList = tagService.findAll();
        } catch (ServiceException e) {
            logger.error(e);
        }
        model.addObject("authors",authorList);
        model.addObject("var",var);
        model.addObject("news",news);
        model.addObject("tags",tagList);
        model.addObject("edit",true);
        model.setViewName("addNews");
        return model;
    }


}
