package com.epam.news.dao.impl;

import com.epam.news.dao.AbstractDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.search.SearchCriteria;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.epam.news.util.DAOConstants.*;

/**
 * Created by Viktoryia_Khlystun on 5/26/2016.
 */
@Component
public class NewsDAOImpl extends AbstractDAO implements NewsDAO {
    private static final String SQL_SEARCH_UTIL_START = "SELECT * FROM (SELECT ROWNUM ROW_NUM, SUBQ.* FROM(select * from(select  COMMENTS.NEWS_ID,NEWS.MODIFICATION_DATE as mDate, COUNT(COMMENTS.COMMENT_ID) as count from COMMENTS join NEWS on NEWS.NEWS_ID = COMMENTS.NEWS_ID GROUP BY COMMENTS.NEWS_ID, NEWS.MODIFICATION_DATE union select NEWS.NEWS_ID, NEWS.MODIFICATION_DATE as mDate,0 as count  from NEWS where NEWS.NEWS_ID not in (select news_id from COMMENTS) ORDER BY count DESC, mDate DESC)";
    private static final String SQL_SEARCH_UTIL_END = "where news_id in (";
    private static final String SQL_SEARCH_NEWS = "select news_id from NEWS where news_id in (";
    private static final String SQL_COUNT_OF_SEARCH_NEWS = "select count(*) as c from NEWS where news_id in (";
    private static final String SQL_SEARCH_NEWS_BY_AUTHOR = "(select news_id from news_author where author_id in (select author_id from AUTHOR where AUTHOR_NAME like ?))";
    private static final String SQL_SEARCH_NEWS_BY_TAG = "(select news_id from NEWS_TAG where TAG_ID in (select tag_id from TAG where TAG_NAME in (";
    private static final String SQL_SELECT_ALL_NEWS = "select * from news";
    private static final String SQL_SELECT_NEWS_BY_ID = "select news_id,title,short_text, full_text,creation_date, modification_date from news where news_id in( ";
    private static final String SQL_SELECT_MOST_COMMENTING_UTIL = "SUBQ WHERE ROWNUM <?)  where ROW_NUM > ?";
    private static final String SQL_SELECT_MOST_COMMENTING = "SELECT * FROM (SELECT ROWNUM ROW_NUM, SUBQ.* FROM (select  COMMENTS.NEWS_ID,NEWS.MODIFICATION_DATE as mDate, COUNT(COMMENTS.COMMENT_ID) as count from COMMENTS join NEWS on NEWS.NEWS_ID = COMMENTS.NEWS_ID GROUP BY COMMENTS.NEWS_ID, NEWS.MODIFICATION_DATE union select NEWS.NEWS_ID, NEWS.MODIFICATION_DATE as mDate,0 as count  from NEWS where NEWS.NEWS_ID not in (select news_id from COMMENTS) ORDER BY count DESC, mDate DESC) SUBQ WHERE ROWNUM <?)  where ROW_NUM > ?";
    private static final String SQL_GET_NEWS_COUNT = "select count(*) as count from NEWS";
    private static final String SQL_DELETE_NEWS_BY_ID = "delete news where news_id = ?";
    private static final String SQL_SELECT_ALL_NEWS_BY_ID = "select news_id,title,short_text, full_text,creation_date, modification_date from news where news_id = ?";
    private static final String SQL_CREATE_NEWS = "insert into news (news_id,title,short_text, full_text,creation_date, modification_date) values (NEWS_SEQ.nextval, ?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE_NEWS = "update news set title = ?, short_text = ?, full_text = ?, modification_date = ? where news_id = ?";
    private static final String SQL_SELECT_NEWS_BY_TITLE = "select news_id,title,short_text, full_text,creation_date, modification_date from news where title = ? ";


    /**
     * Return news info without comments, tags and authors. Only news description
     *
     * @param id
     * @return
     * @throws DAOException
     */
    public News findNewsById(long id) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        News news = new News();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_SELECT_ALL_NEWS_BY_ID);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                news.setId(resultSet.getLong(NEWS_ID));
                news.setTitle(resultSet.getString(TITLE));
                news.setShortText(resultSet.getString(SHORT_TEXT));
                news.setFullText(resultSet.getString(FULL_TEXT));
                news.setCreationDate(resultSet.getDate(CREATION_DATE));
                news.setModificationDate(resultSet.getDate(MODIFICATION_DATE));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return news;
    }

    /**
     * Find news by title
     * @param title
     * @return
     * @throws DAOException
     */
    public News findNewsByTitle(String title) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        News news = new News();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_SELECT_NEWS_BY_TITLE);
            statement.setString(1, title);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                news.setId(resultSet.getLong(NEWS_ID));
                news.setTitle(resultSet.getString(TITLE));
                news.setShortText(resultSet.getString(SHORT_TEXT));
                news.setFullText(resultSet.getString(FULL_TEXT));
                news.setCreationDate(resultSet.getDate(CREATION_DATE));
                news.setModificationDate(resultSet.getDate(MODIFICATION_DATE));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return news;
    }

    /**
     * Return list of news descriptions without comments, authors and tags
     *
     * @return
     * @throws DAOException
     */
    public List<News> getListOfNews() throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        List<News> news = new ArrayList<News>();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_SELECT_ALL_NEWS);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                News news1 = new News();
                news1.setId(resultSet.getLong(NEWS_ID));
                news1.setTitle(resultSet.getString(TITLE));
                news1.setShortText(resultSet.getString(SHORT_TEXT));
                news1.setFullText(resultSet.getString(FULL_TEXT));
                news1.setCreationDate(resultSet.getDate(CREATION_DATE));
                news1.setModificationDate(resultSet.getDate(MODIFICATION_DATE));
                news.add(news1);

            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return news;
    }

    /**
     * Get list of news, which id is present in param ids
     * @param ids
     * @return
     * @throws DAOException
     */
    public List<News> getSubListOfNews(List<Long> ids) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        List<News> news = new ArrayList<News>();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            String query = createQuery(ids);
            statement = connection.prepareStatement(query);
            for(int i = 0; i<ids.size(); i++){
                statement.setLong(i+1,ids.get(i));
            }
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                News news1 = new News();
                news1.setId(resultSet.getLong(NEWS_ID));
                news1.setTitle(resultSet.getString(TITLE));
                news1.setShortText(resultSet.getString(SHORT_TEXT));
                news1.setFullText(resultSet.getString(FULL_TEXT));
                news1.setCreationDate(resultSet.getDate(CREATION_DATE));
                news1.setModificationDate(resultSet.getDate(MODIFICATION_DATE));
                news.add(news1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return news;
    }

    /**
     * Create a record in database with news info, only news description? not including tags, authors and comments
     *
     * @param news
     * @throws DAOException
     */
    public void create(News news) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_CREATE_NEWS);
            statement.setString(1, news.getTitle());
            statement.setString(2, news.getShortText());
            statement.setString(3, news.getFullText());
            statement.setTimestamp(4, new Timestamp(new Date().getTime()));
            statement.setTimestamp(5, new Timestamp(new Date().getTime()));
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Update news text, title and change modification date
     *
     * @param news
     * @throws DAOException
     */
    public void update(News news) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_UPDATE_NEWS);
            statement.setString(1, news.getTitle());
            statement.setString(2, news.getShortText());
            statement.setString(3, news.getFullText());
            statement.setTimestamp(4, new Timestamp(new Date().getTime()));
            statement.setLong(5, news.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Delete news by id from table with comments deleting on cascade
     *
     * @param id
     * @throws DAOException
     */
    public void delete(long id) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE_NEWS_BY_ID);
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Return sorted list of newsIds desc from most commenting
     *
     * @return
     * @throws DAOException
     */
    public List<Long> getIdOfMostCommenting(long start, long end) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        List<Long> mostCommentingNewsId = new ArrayList<Long>();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_SELECT_MOST_COMMENTING);
            statement.setLong(1,end);
            statement.setLong(2,start);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                mostCommentingNewsId.add(resultSet.getLong(NEWS_ID));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return mostCommentingNewsId;
    }

    /**
     * Get list of most commenting ids present in param ids in some limits
     * @param start
     * @param end
     * @param ids
     * @return
     * @throws DAOException
     */
    public List<Long> getIdOfMostCommenting(long start, long end,List<Long> ids) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        List<Long> mostCommentingNewsId = new ArrayList<Long>();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            String query = createQueryMostCommenting(ids);
            statement = connection.prepareStatement(query);
            for(int i=0;i<ids.size();i++){
                statement.setLong(i+1,ids.get(i));
            }
            statement.setLong(ids.size()+1,end);
            statement.setLong(ids.size()+2,start);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                mostCommentingNewsId.add(resultSet.getLong(NEWS_ID));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return mostCommentingNewsId;
    }

    /**
     * Return the count of records in database in table news
     *
     * @return
     * @throws DAOException
     */
    public int getCountOfNews() throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        int count = 0;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_GET_NEWS_COUNT);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                count = resultSet.getInt(COUNT);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return count;
    }

    /**
     * Return list of news fitted some search parameters like author name or tags
     *
     * @param searchCriteria
     * @return
     * @throws DAOException
     */
    public List<Long> getListOfNews(SearchCriteria searchCriteria) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        List<Long> news = new ArrayList<Long>();
        List<String> tags = null;
        String author = null;
        if (searchCriteria.getTags() != null) {
            tags = new ArrayList<>();
            for (Tag tag : searchCriteria.getTags()) {
                tags.add(tag.getName());
            }
        }
        if (searchCriteria.getAuthor() != null) {
            author = searchCriteria.getAuthor().getName();
        }
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            String query = createQuery(tags, author);
            statement = connection.prepareStatement(query);
            if (author != null) {
                if (tags != null) {
                    for (int i = 0; i < tags.size(); i++) {
                        statement.setString(i + 1, tags.get(i));
                    }
                    statement.setString(tags.size() + 1, "%" + author + "%");
                } else {
                    statement.setString(1, "%" + author + "%");
                }
            } else if (tags != null) {
                for (int i = 0; i < tags.size(); i++) {
                    statement.setString(i + 1, tags.get(i));
                }
            }
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                news.add(resultSet.getLong(NEWS_ID));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return news;
    }

    /**
     * return count of news according to search criteria
     * @param searchCriteria
     * @return
     * @throws DAOException
     */
    public int getCountOfListNews(SearchCriteria searchCriteria) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        List<Long> news = new ArrayList<Long>();
        List<String> tags = null;
        String author = null;
        if (searchCriteria.getTags() != null) {
            tags = new ArrayList<>();
            for (Tag tag : searchCriteria.getTags()) {
                tags.add(tag.getName());
            }
        }
        if (searchCriteria.getAuthor() != null) {
            author = searchCriteria.getAuthor().getName();
        }
        int count = 0;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            String query = createCountQuery(tags, author);
            statement = connection.prepareStatement(query);
            if (author != null) {
                if (tags != null) {
                    for (int i = 0; i < tags.size(); i++) {
                        statement.setString(i + 1, tags.get(i));
                    }
                    statement.setString(tags.size() + 1, "%" + author + "%");
                } else {
                    statement.setString(1, "%" + author + "%");
                }
            } else if (tags != null) {
                for (int i = 0; i < tags.size(); i++) {
                    statement.setString(i + 1, tags.get(i));
                }
            }
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                count = resultSet.getInt("c");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return count;
    }

    /**
     * util method for creating sql query for search? correspond to incoming parameters
     *
     * @param tags
     * @param author
     * @return
     */
    private String createQuery(List<String> tags, String author) {
        StringBuilder queryBuilder = new StringBuilder(SQL_SEARCH_NEWS);
        if (tags != null) {
            StringBuilder queryBuilderTags = new StringBuilder(SQL_SEARCH_NEWS_BY_TAG);
            for (int i = 0; i < tags.size(); i++) {
                queryBuilderTags.append(" ?");
                if (i != tags.size() - 1) {
                    queryBuilderTags.append(",");
                }
            }
            queryBuilderTags.append(")))");
            queryBuilder.append(queryBuilderTags);
            if (author != null) {
                queryBuilder.append(INTERSECT);
                queryBuilder.append(SQL_SEARCH_NEWS_BY_AUTHOR);
            }
        } else if (author != null) {
            queryBuilder.append(SQL_SEARCH_NEWS_BY_AUTHOR);
        } else {
            queryBuilder.append(NULL);
        }
        queryBuilder.append(")");
        return queryBuilder.toString();
    }

    /**
     * return sql query for getting count of news searched by some parameters
     * @param tags
     * @param author
     * @return
     */

    private String createCountQuery(List<String> tags, String author) {
        StringBuilder queryBuilder = new StringBuilder(SQL_COUNT_OF_SEARCH_NEWS);
        if (tags != null) {
            StringBuilder queryBuilderTags = new StringBuilder(SQL_SEARCH_NEWS_BY_TAG);
            for (int i = 0; i < tags.size(); i++) {
                queryBuilderTags.append(" ?");
                if (i != tags.size() - 1) {
                    queryBuilderTags.append(",");
                }
            }
            queryBuilderTags.append(")))");
            queryBuilder.append(queryBuilderTags);
            if (author != null) {
                queryBuilder.append(INTERSECT);
                queryBuilder.append(SQL_SEARCH_NEWS_BY_AUTHOR);
            }
        } else if (author != null) {
            queryBuilder.append(SQL_SEARCH_NEWS_BY_AUTHOR);
        } else {
            queryBuilder.append(NULL);
        }
        queryBuilder.append(")");
        return queryBuilder.toString();
    }

    /**
     * create sql query for searching news in some range of ids
     * @param ids
     * @return
     */
    private String createQuery(List<Long> ids) {
        StringBuilder queryBuilder = new StringBuilder(SQL_SELECT_NEWS_BY_ID);
            for (int i = 0; i < ids.size(); i++) {
                queryBuilder.append(" ?");
                if (i != ids.size() - 1) {
                    queryBuilder.append(",");
                }
            }
        queryBuilder.append(")");
        return queryBuilder.toString();
    }

    /**
     * create sql query for most commenting news in some range of ids
     * @param ids
     * @return
     */
    private String createQueryMostCommenting(List<Long> ids) {
        StringBuilder queryBuilder = new StringBuilder(SQL_SEARCH_UTIL_START);
        queryBuilder.append(SQL_SEARCH_UTIL_END);
        for (int i = 0; i < ids.size(); i++) {
            queryBuilder.append(" ?");
            if (i != ids.size() - 1) {
                queryBuilder.append(",");
            }
        }
        queryBuilder.append("))");
        queryBuilder.append(SQL_SELECT_MOST_COMMENTING_UTIL);
        return queryBuilder.toString();
    }
}
