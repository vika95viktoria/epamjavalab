package com.epam.news.service.impl;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.dao.CommentDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.dao.TagDAO;
import com.epam.news.domain.Author;
import com.epam.news.domain.Comment;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.model.NewsModel;
import com.epam.news.search.SearchCriteria;
import com.epam.news.service.NewsService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
@Component
public class NewsServiceImpl implements NewsService {

    private static final Logger logger = Logger.getLogger(NewsServiceImpl.class);
    @Autowired
    private NewsDAO newsDAO;
    @Autowired
    private TagDAO tagDAO;
    @Autowired
    private AuthorDAO authorDAO;
    @Autowired
    private CommentDAO commentDAO;

    public void setNewsDAO(NewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }

    public void setTagDAO(TagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }

    public void setAuthorDAO(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    public void setCommentDAO(CommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

    /**
     * return list of news by ids
     * @param ids
     * @return
     * @throws ServiceException
     */

    @Transactional(rollbackFor = ServiceException.class)
    public List<News> getNewsByIds(List<Long> ids) throws ServiceException {
        List<News> news;
        try {
            news = newsDAO.getSubListOfNews(ids);
            final List<Long> idOfMostCommenting = ids;
            Collections.sort(news, new Comparator<News>() {
                @Override
                public int compare(News o1, News o2) {
                    return new Integer(idOfMostCommenting.indexOf(o1.getId())).compareTo(new Integer(idOfMostCommenting.indexOf(o2.getId())));
                }
            });
            for (News news1 : news) {
                List<Tag> tags = tagDAO.findTagsByNewsId(news1.getId());
                List<Author> authors = authorDAO.findAuthorsByNewsId(news1.getId());
                List<Comment> comments = commentDAO.findCommentsByNewsId(news1.getId());
                news1.setTags(tags);
                news1.setAuthors(authors);
                news1.setComments(comments);
            }
        } catch (DAOException e) {
            logger.error("Failed to get news by ids" + e);
            throw new ServiceException(e);
        }
        return news;
    }

    /**
     * Return ordered list of news with all info
     *
     * @return
     * @throws ServiceException
     */
    public List<News> getNewsOrderedByComments(long start, long end) throws ServiceException {
        List<News> news;
        try {
            final List<Long> idOfMostCommenting = newsDAO.getIdOfMostCommenting(start, end);
            news = getNewsByIds(idOfMostCommenting);
        } catch (DAOException e) {
            logger.error("Failed to get ids of most commenting news " + e);
            throw new ServiceException(e);
        }
        return news;
    }


    /**
     * Create news with tags and authors
     *
     * @param newsModel
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    public void createNews(NewsModel newsModel) throws ServiceException {
        News news = new News(newsModel);
        try {
            newsDAO.create(news);
            for (Long tag : newsModel.getTagIds()) {
                tagDAO.addTagToNews(news, tag);
            }
            for (long author : newsModel.getAuthorIds()) {
                authorDAO.addAuthorToNews(news, author);
            }
        } catch (DAOException e) {
            logger.error("Failed to create news " + e);
            throw new ServiceException(e);
        }
    }

    /**
     * Delete news and related records in database
     *
     * @param newsId
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    public void deleteNews(Long newsId) throws ServiceException {
        try {
            authorDAO.deleteAllAuthorsFromNews(newsId);
            tagDAO.deleteAllTagsFromNews(newsId);
            newsDAO.delete(newsId);
        } catch (DAOException e) {
            logger.error("Failed to delete news by id" + newsId + " " + e);
            throw new ServiceException(e);
        }
    }

    /**
     * Return complete object of news with all related objects like tags, authors and comments
     *
     * @param newsId
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    public News findNewsById(Long newsId) throws ServiceException {
        News news;
        try {
            news = newsDAO.findNewsById(newsId);
            List<Tag> tags = tagDAO.findTagsByNewsId(newsId);
            List<Author> authors = authorDAO.findAuthorsByNewsId(newsId);
            List<Comment> comments = commentDAO.findCommentsByNewsId(newsId);
            news.setTags(tags);
            news.setAuthors(authors);
            news.setComments(comments);
        } catch (DAOException e) {
            logger.error("Failed to find news by id " + newsId + " " + e);
            throw new ServiceException(e);
        }
        return news;
    }

    /**
     * Return count of news
     *
     * @return
     * @throws ServiceException
     */
    public int getCountOfNews() throws ServiceException {
        try {
            return newsDAO.getCountOfNews();
        } catch (DAOException e) {
            logger.error("Failed to get count of news " + e);
            throw new ServiceException(e);
        }
    }

    /**
     * Return count of news according to search criteria
     * @param searchCriteria
     * @return
     * @throws ServiceException
     */
    public int getCountOfNews(SearchCriteria searchCriteria) throws ServiceException {
        try {
            return newsDAO.getCountOfListNews(searchCriteria);
        } catch (DAOException e) {
            logger.error("Failed to get count of news by search criteria" + e);
            throw new ServiceException(e);
        }
    }

    /**
     * Edit news info
     *
     * @param newsModel
     * @throws ServiceException
     */
    public void updateNews(NewsModel newsModel) throws ServiceException {
        News news = new News(newsModel);
        try {
            newsDAO.update(news);
            news = findNewsById(news.getId());
            List<Long> authorIds = new ArrayList<>();
            List<Long> tagIds = new ArrayList<>();
            for(Tag tag: news.getTags()){
                tagIds.add(tag.getId());
            }
            for(Author author: news.getAuthors()){
                authorIds.add(author.getId());
            }
            for(Long author: authorIds){
                if (!newsModel.getAuthorIds().contains(author)) {
                    authorDAO.deleteAuthorFromNews(news.getId(),author);
                }
            }
            for (Long author : newsModel.getAuthorIds()) {
                if(!authorIds.contains(author)){
                    authorDAO.addAuthorToNews(news.getId(),author);
                }
            }

            for(Long tag: tagIds){
                if (!newsModel.getTagIds().contains(tag)) {
                    tagDAO.deleteTagFromNews(news.getId(),tag);
                }
            }
            for (Long tag : newsModel.getTagIds()) {
                if(!tagIds.contains(tag)){
                    tagDAO.addTagToNews(news.getId(),tag);
                }
            }
        } catch (DAOException e) {
            logger.error("Failed to update news by id " + news.getId() + " " + e);
            throw new ServiceException(e);
        }
    }

    /**
     * return list of news fitted to serach parameters
     *
     * @param searchCriteria
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    public List<News> searchForNews(SearchCriteria searchCriteria,long start, long end) throws ServiceException {
        List<News> news = new ArrayList<>();
        List<Long> ids = new ArrayList<>();
        try {
            ids = newsDAO.getListOfNews(searchCriteria);
            List<Long> idOfMostCommenting = newsDAO.getIdOfMostCommenting(start, end, ids);
            news = getNewsByIds(idOfMostCommenting);
        } catch (DAOException e) {
            logger.error("Failed to find news according to search criteria " + e);
            throw new ServiceException(e);
        }
        return news;
    }

    /**
     * find news by it's title
     * @param title
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    public News findNewsByTitle(String title) throws ServiceException {
        News news;
        try {
            news = newsDAO.findNewsByTitle(title);
            List<Tag> tags = tagDAO.findTagsByNewsId(news.getId());
            List<Author> authors = authorDAO.findAuthorsByNewsId(news.getId());
            List<Comment> comments = commentDAO.findCommentsByNewsId(news.getId());
            news.setTags(tags);
            news.setAuthors(authors);
            news.setComments(comments);
        } catch (DAOException e) {
            logger.error("Failed to find news by title " + title + " " + e);
            throw new ServiceException(e);
        }
        return news;
    }
}
