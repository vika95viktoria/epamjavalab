package com.epam.news.dao;

import com.epam.news.domain.Author;
import com.epam.news.domain.News;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/testContext.xml"})
@DatabaseSetup(value = "/data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class AuthorDAOTest {
    private final static Logger logger = Logger.getLogger(AuthorDAOTest.class);
    private static AuthorDAO authorDAO;
    private static ApplicationContext context;

    @BeforeClass
    public static void setUpAuthorDAO() {
        context = new ClassPathXmlApplicationContext("testContext.xml");
        authorDAO = (AuthorDAO) context.getBean("AuthorDAO");
    }

    @AfterClass
    public static void closeContext() {
        ((ConfigurableApplicationContext) context).close();
    }


    @Test
    public void findAuthorById() throws Exception {
        Author author = authorDAO.findAuthorById(1);
        assertEquals(author.getId(), 1);
        assertEquals(author.getName(), "Polina Petrova");
    }

    @Test
    public void delete() throws DAOException {
        Author author = authorDAO.findAuthorById(1);
        Date expired = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            expired = formatter.parse(formatter.format(expired));
        } catch (ParseException e) {
            logger.error(e);
        }
        author.setExpired(expired);
        authorDAO.delete(author);
        author = authorDAO.findAuthorById(1);
        assertEquals(expired, author.getExpired());
    }

    @Test
    public void update() throws DAOException {
        Author author = authorDAO.findAuthorById(1);
        author.setName("Alina");
        authorDAO.update(author);
        Author updatedAuthor = authorDAO.findAuthorById(1);
        assertEquals(author.getName(), updatedAuthor.getName());
        assertEquals(author.getId(), updatedAuthor.getId());
    }

    @Test
    public void findAuthorsByNewsId() throws DAOException {
        List<Author> authors = authorDAO.findAuthorsByNewsId(1);
        assertEquals(1, authors.size());
        assertEquals(2, authors.get(0).getId());
    }

    @Test
    public void addNewsAuthor() throws DAOException {
        List<Author> authors = authorDAO.findAuthorsByNewsId(1);
        assertEquals(1, authors.size());
        News news = new News();
        news.setTitle("Paris");
        authorDAO.addAuthorToNews(news, 1);
        authorDAO.addAuthorToNews(1, 3);
        authors = authorDAO.findAuthorsByNewsId(1);
        assertEquals(3, authors.size());

    }

    @Test
    public void deleteNewsAuthor() throws DAOException {
        List<Author> authors = authorDAO.findAuthorsByNewsId(1);
        assertEquals(1, authors.size());
        authorDAO.deleteAuthorFromNews(1);
        authors = authorDAO.findAuthorsByNewsId(1);
        assertEquals(0, authors.size());
    }

}
