package com.epam.news.domain;

import java.util.Date;

/**
 * Created by Viktoryia_Khlystun on 5/25/2016.
 */
public class Comment {
    private long id;
    private String text;
    private Date creationDate;

    public Comment() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
