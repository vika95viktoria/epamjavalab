package com.epam.news.service.impl;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.dao.CommentDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.dao.TagDAO;
import com.epam.news.domain.Author;
import com.epam.news.domain.Comment;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.search.SearchCriteria;
import com.epam.news.service.NewsService;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */

public class NewsServiceImpl implements NewsService {
    private NewsDAO newsDAO;
    private TagDAO tagDAO;
    private AuthorDAO authorDAO;
    private CommentDAO commentDAO;

    public void setNewsDAO(NewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }

    public void setTagDAO(TagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }

    public void setAuthorDAO(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    public void setCommentDAO(CommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

    /**
     * Return list of news entites with all rinfo? including tag list, authors and comments
     *
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    public List<News> getNews() throws ServiceException {
        List<News> news;
        try {
            news = newsDAO.getListOfNews();
            for (News news1 : news) {
                List<Tag> tags = tagDAO.findTagsByNewsId(news1.getId());
                List<Author> authors = authorDAO.findAuthorsByNewsId(news1.getId());
                List<Comment> comments = commentDAO.findCommentsByNewsId(news1.getId());
                news1.setTags(tags);
                news1.setAuthors(authors);
                news1.setComments(comments);
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return news;
    }

    /**
     * Return ordered list of news with all info
     *
     * @return
     * @throws ServiceException
     */
    public List<News> getNewsOrderedByComments() throws ServiceException {
        List<News> news;
        try {
            news = getNews();
            final List<Long> idOfMostCommenting = newsDAO.getIdOfMostCommenting();
            Collections.sort(news, new Comparator<News>() {
                @Override
                public int compare(News o1, News o2) {
                    return new Integer(idOfMostCommenting.indexOf(o1.getId())).compareTo(new Integer(idOfMostCommenting.indexOf(o2.getId())));
                }
            });
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return news;
    }


    /**
     * Create news with tags and authors
     *
     * @param news
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    public void createNews(News news) throws ServiceException {
        try {
            newsDAO.create(news);
            for (Tag tag : news.getTags()) {
                tagDAO.addTagToNews(news, tag.getId());
            }
            for (Author author : news.getAuthors()) {
                authorDAO.addAuthorToNews(news, author.getId());
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Delete news and related records in database
     *
     * @param newsId
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    public void deleteNews(Long newsId) throws ServiceException {
        try {
            authorDAO.deleteAuthorFromNews(newsId);
            tagDAO.deleteTagsFromNews(newsId);
            newsDAO.delete(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Return complete object of news with all related objects like tags, authors and comments
     *
     * @param newsId
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    public News findNewsById(Long newsId) throws ServiceException {
        News news;
        try {
            news = newsDAO.findNewsById(newsId);
            List<Tag> tags = tagDAO.findTagsByNewsId(newsId);
            List<Author> authors = authorDAO.findAuthorsByNewsId(newsId);
            List<Comment> comments = commentDAO.findCommentsByNewsId(newsId);
            news.setTags(tags);
            news.setAuthors(authors);
            news.setComments(comments);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return news;
    }

    /**
     * Return count of news
     *
     * @return
     * @throws ServiceException
     */
    public int getCountOfNews() throws ServiceException {
        try {
            return newsDAO.getCountOfNews();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Edit news info
     *
     * @param news
     * @throws ServiceException
     */
    public void updateNews(News news) throws ServiceException {
        try {
            newsDAO.update(news);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * return list of news fitted to serach parameters
     *
     * @param searchCriteria
     * @return
     * @throws ServiceException
     */
    public List<News> searchForNews(SearchCriteria searchCriteria) throws ServiceException {
        List<News> news;
        try {
            news = newsDAO.getListOfNews(searchCriteria);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return news;
    }
}
