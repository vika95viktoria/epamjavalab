package com.epam.news.service;

import com.epam.news.dao.TagDAO;
import com.epam.news.domain.Tag;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/31/2016.
 */
public interface TagService {
    Tag findTagById(long tagId);

    void setTagDAO(TagDAO tagDAO);

    List<Tag> findAll();

    List<Tag> findAll(long start);

    int getCountOfTags();

    void delete(Tag tag);

    void save(Tag tag);
}
