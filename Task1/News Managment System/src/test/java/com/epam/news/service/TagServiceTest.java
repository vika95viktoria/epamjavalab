package com.epam.news.service;

/**
 * Created by Viktoryia_Khlystun on 6/3/2016.
 */

import com.epam.news.dao.TagDAO;
import com.epam.news.dao.impl.TagDAOImpl;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.impl.TagServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {
    private static TagService mockedTagService;
    private static TagDAO mockedTagDAO;
    private static Tag tag;

    @Before
    public void setUp() throws DAOException {
        mockedTagDAO = mock(TagDAO.class);
        mockedTagService = new TagServiceImpl();
        mockedTagService.setTagDAO(mockedTagDAO);
        tag = new Tag();
        tag.setId(1);
        tag.setName("Weather");
        when(mockedTagDAO.findTagById(1)).thenReturn(tag);

    }

    @After
    public void reset() {
        Mockito.reset(mockedTagDAO);
    }

    @Test
    public void getTagById() throws ServiceException, DAOException {
        Tag mockedTag = mockedTagService.findTagById(1);
        assertEquals(mockedTag.getId(), 1);
        assertEquals(mockedTag.getName(), "Weather");
    }

    @Test
    public void create() throws ServiceException, DAOException {
        mockedTagService.create(tag);
        verify(mockedTagDAO).create(tag);
    }

    @Test
    public void deleteTagsFromNews() throws DAOException, ServiceException {
        mockedTagService.deleteTagsFromNews(1);
        verify(mockedTagDAO).deleteTagsFromNews(1);
    }
}
