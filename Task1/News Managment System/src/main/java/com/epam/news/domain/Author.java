package com.epam.news.domain;

import java.util.Date;

/**
 * Created by Viktoryia_Khlystun on 5/25/2016.
 */
public class Author {
    private long id;
    private String name;
    private Date expired;

    public Author() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getExpired() {
        return expired;
    }

    public void setExpired(Date expired) {
        this.expired = expired;
    }
}
