package com.epam.news.controller.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Viktoryia_Khlystun on 6/23/2016.
 */

@Component
public class ActionFactory {
    private ConcurrentHashMap<String, ActionCommand> getCommandHashMap = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, ActionCommand> postCommandHashMap = new ConcurrentHashMap<>();

    @Autowired
    private NewsListCommand newsListCommand;

    @Autowired
    private NewsPageCommand newsPageCommand;

    @Autowired
    private PostCommentCommand postCommentCommand;

    @Autowired
    private ChangeLanguageCommand changeLanguageCommand;


    /**
     * Initialize command map and fills it in with values of autowired commands
     */
    @PostConstruct
    public void createActionFactory() {
        getCommandHashMap.put("newsView", newsPageCommand);
        getCommandHashMap.put("newsList", newsListCommand);
        postCommandHashMap.put("postComment",postCommentCommand);
        postCommandHashMap.put("changeLanguage", changeLanguageCommand);
    }

    /**
     * return get command corresponding to the key word in request
     *
     * @param action
     * @return
     */
    public ActionCommand defineGetCommand(String action) {
        ActionCommand current = null;
        if(action != null) {
            current = getCommandHashMap.get(action);
        }
        if (current == null) {
            current = newsListCommand;
        }
        return current;
    }

    /**
     * return post command corresponding to the key word in request
     * @param action
     * @return
     */
    public ActionCommand definePostCommand(String action) {
        ActionCommand current = null;
        if(action != null) {
            current = postCommandHashMap.get(action);
        }
        if (current == null) {
            current = newsListCommand;
        }
        return current;
    }
}
