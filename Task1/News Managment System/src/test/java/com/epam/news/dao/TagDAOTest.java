package com.epam.news.dao;

import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Viktoryia_Khlystun on 6/3/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/testContext.xml"})
@DatabaseSetup(value = "/data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class TagDAOTest {
    private static TagDAO tagDAO;
    private static ApplicationContext context;


    @BeforeClass
    public static void setUpAuthorDAO() {
        context = new ClassPathXmlApplicationContext("testContext.xml");
        tagDAO = (TagDAO) context.getBean("TagDAO");
    }

    @AfterClass
    public static void closeContext() {
        ((ConfigurableApplicationContext) context).close();
    }

    @Test
    public void findTagById() throws DAOException {
        Tag tag = tagDAO.findTagById(1);
        assertEquals(1, tag.getId());
        assertEquals("Weather", tag.getName());
    }

    @Test
    public void findTagByNewsId() throws DAOException {
        List<Tag> tags = tagDAO.findTagsByNewsId(1);
        assertEquals(2, tags.size());
    }

    @Test
    public void addNewsTag() throws DAOException {
        List<Tag> tags = tagDAO.findTagsByNewsId(1);
        assertEquals(2, tags.size());
        News news = new News();
        news.setTitle("Paris");
        tagDAO.addTagToNews(news, 3);
        tags = tagDAO.findTagsByNewsId(1);
        assertEquals(3, tags.size());
    }

    @Test
    public void addNewsTagByNewsId() throws DAOException {
        List<Tag> tags = tagDAO.findTagsByNewsId(2);
        assertEquals(1, tags.size());
        tagDAO.addTagToNews(2, 3);
        tags = tagDAO.findTagsByNewsId(2);
        assertEquals(2, tags.size());
    }

    @Test
    public void deleteNewsTag() throws DAOException {
        List<Tag> tags = tagDAO.findTagsByNewsId(2);
        assertEquals(1, tags.size());
        tagDAO.deleteTagsFromNews(2);
        tags = tagDAO.findTagsByNewsId(2);
        assertEquals(0, tags.size());
    }
}
