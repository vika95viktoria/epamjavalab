package com.epam.news.controller;

import com.epam.news.domain.Author;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.service.AuthorService;
import com.epam.news.service.NewsService;
import com.epam.news.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 6/20/2016.
 */

@Controller
public class AdminController {


    @Autowired
    private NewsService newsService;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private TagService tagService;

    /**
     * Returns login page, in case of wrong credentials displays an error
     *
     * @param error
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error) {
        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid username or password!");
        }
        model.setViewName("home");
        return model;

    }

    /**
     * Opens page for adding news
     * @return
     */
    @RequestMapping(value = "/addNews", method = RequestMethod.GET)
    public ModelAndView openAddNewsPage(@RequestParam(required = false) Model modelMap) {
        ModelAndView model = new ModelAndView();
        List<Author> authorList = authorService.findAll();
        List<Tag> tagList = tagService.findAll();
        if (modelMap != null) {
            String error = (String) modelMap.asMap().get("error");
            model.addObject("error", error);
        }
        model.addObject("authors",authorList);
        model.addObject("tags",tagList);
        model.setViewName("addNews");
        return model;
    }

    /**
     * Opens page for editing news with prefilled fields with news info
     * @param id
     * @param var
     * @return
     */

    @RequestMapping(value = "/editNews", method = RequestMethod.GET)
    public ModelAndView openEditPage(@RequestParam Long id, @RequestParam String var, @RequestParam(required = false) Model modelMap) {
        ModelAndView model = new ModelAndView();
        List<Author> authorList = authorService.findAll();
        List<Tag> tagList = tagService.findAll();
        News news = newsService.findNewsById(id);
        if (modelMap != null) {
            String error = (String) modelMap.asMap().get("error");
            model.addObject("error", error);
        }
        model.addObject("authors",authorList);
        model.addObject("var",var);
        model.addObject("news",news);
        model.addObject("tags",tagList);
        model.addObject("edit",true);
        model.setViewName("addNews");
        return model;
    }


}
