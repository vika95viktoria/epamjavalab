<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>">
    <meta charset="UTF-8">


    <title>404 Error</title>
</head>
<body class="page404">
<div id="content">

    <div id="sorry"><spring:message code="error404"/></div>

</div>

</body>
</html>
