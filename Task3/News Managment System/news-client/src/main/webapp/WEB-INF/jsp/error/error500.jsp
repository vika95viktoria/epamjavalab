<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setBundle basename="pagecontent"/>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>">

    <meta charset="UTF-8">


    <title>500 Error</title>
</head>
<body>
<div>
<img src="https://www.richit.com.au/images/repairs/broken_computer.png"  style="position: absolute; top:20%; left: 30%">
<span id="sorry500" >
<br>
<fmt:message key="error500.first"/>
<br>
<fmt:message key="error500.second"/>
 </span>
</div>

</body>
</html>