(function () {
    'use strict';

    var newsApp = angular.module('newsApp');
    newsApp.controller('NewsViewController', ['$scope', '$routeParams', 'NewsViewService', 'CommentService', function ($scope, $routeParams, NewsViewService, CommentService) {
        getNewsPiece();
        if (typeof $routeParams.position !== "undefined") {
            $scope.position = $routeParams.position;
        }
        else {
            $scope.position = -1;
        }
        var tags = angular.fromJson($routeParams.searchTags);
        var tagNamesList = [];
        $scope.count = $routeParams.count;

        if (typeof tags !== "undefined" && tags.length > 0) {
            angular.forEach(tags, function (value) {
                tagNamesList.push(value.name);
            });
        }

        function getNewsPiece() {
            NewsViewService.getNewsById($routeParams.id)
                .then(
                    function (d) {
                        $scope.newsPiece = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching News by id ' + $routeParams.id);
                    });
        }

        $scope.getInfo = function (ind) {
            NewsViewService.getNewsFromChain(ind, tagNamesList, $routeParams.searchAuthor, $scope.position)
                .then(
                    function (d) {
                        $scope.newsPiece = d;
                        if (ind === 1) {
                            $scope.position = 1 + parseInt($scope.position);
                        }
                        else {
                            $scope.position = parseInt($scope.position) - 1;
                        }
                    },
                    function (errResponse) {
                        console.error('Error while fetching News by next/prev button ');
                    });
        }

        $scope.sendComment = function () {
            CommentService.addComment($scope.commentText, $scope.newsPiece.id)
                .then(
                    function (d) {
                        $scope.newsPiece.comments.push(d.commentModel);
                        $scope.commentText = '';
                    },
                    function (errResponse) {
                        console.error('Error while fetching News by next/prev button ');
                    });
        }

        $scope.deleteComment = function (comment) {
            CommentService.deleteComment(comment.id)
                .then(
                    function (d) {
                        var index = $scope.newsPiece.comments.indexOf(comment);
                        $scope.newsPiece.comments.splice(index, index + 1);
                    },
                    function (errResponse) {
                        console.error('Error while fetching News by next/prev button ');
                    });
        }


    }]);
})();
