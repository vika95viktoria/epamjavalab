package com.epam.news.service.impl;

import com.epam.news.dao.TagDAO;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.TagService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/31/2016.
 */
@Service
public class TagServiceImpl implements TagService {
    private static final Logger logger = Logger.getLogger(TagServiceImpl.class);
    @Autowired
    private TagDAO tagDAO;

    public void setTagDAO(TagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }

    /**
     * Add tags to news
     *
     * @param tagIds
     * @param newsId
     * @throws ServiceException
     */
    public void addTagsToNews(List<Long> tagIds, long newsId) throws ServiceException {
        try {
            for (Long id : tagIds) {
                tagDAO.addTagToNews(newsId, id);
            }
        } catch (DAOException e) {
            logger.error("Failed to add tag to news " + e);
            throw new ServiceException(e);
        }
    }

    /**
     * create or update tag
     * @param tag
     * @throws ServiceException
     */
    public void save(Tag tag) throws ServiceException {
        try {
            if (tag.getId() != 0) {
                tagDAO.update(tag);
            } else {
                tagDAO.create(tag);
            }
        } catch (DAOException e) {
            logger.error("Failed to save tag " + e);
            throw new ServiceException(e);
        }
    }


    /**
     * Return all tag info by id
     *
     * @param tagId
     * @return
     * @throws ServiceException
     */
    public Tag findTagById(long tagId) throws ServiceException {
        Tag tag;
        try {
            tag = tagDAO.findTagById(tagId);
        } catch (DAOException e) {
            logger.error("Failed to find tag by id " + tagId + " " + e);
            throw new ServiceException(e);
        }
        return tag;
    }

    /**
     * Delete tags related to news when you delete news
     *
     * @param newsId
     * @throws ServiceException
     */
    public void deleteTagsFromNews(long newsId) throws ServiceException {
        try {
            tagDAO.deleteAllTagsFromNews(newsId);
        } catch (DAOException e) {
            logger.error("Failed to delete tag from news by id " + newsId + " " + e);
            throw new ServiceException(e);
        }
    }

    /**
     * get list of all tags in database
     * @return
     * @throws ServiceException
     */
    public List<Tag> findAll() throws ServiceException {
        List<Tag> tags = new ArrayList<>();
        try {
            tags = tagDAO.findAll();
        } catch (DAOException e) {
            logger.error("Failed to get list of tags " + e);
            throw new ServiceException(e);
        }
        return tags;
    }

    /**
     * get list of all tags in database in some limits
     * @param start
     * @param end
     * @return
     * @throws ServiceException
     */
    public List<Tag> findAll(long start, long end) throws ServiceException {
        List<Tag> authors;
        try {
            authors = tagDAO.findAll(start, end);
        } catch (DAOException e) {
            logger.error("Failed to get list of tags " + e);
            throw new ServiceException(e);
        }
        return authors;
    }

    /**
     * get count of tags in database
     * @return
     * @throws ServiceException
     */
    public int getCountOfTags() throws ServiceException {
        try {
            return tagDAO.getCountOfTags();
        } catch (DAOException e) {
            logger.error("Failed to get count of tags " + e);
            throw new ServiceException(e);
        }
    }

    /**
     * delete tag from database
     * @param tag
     * @throws ServiceException
     */
    public void delete(Tag tag) throws ServiceException {
        try {
            tagDAO.delete(tag);
        } catch (DAOException e) {
            logger.error("Failed to delete tag " + e);
            throw new ServiceException(e);
        }
    }
}
