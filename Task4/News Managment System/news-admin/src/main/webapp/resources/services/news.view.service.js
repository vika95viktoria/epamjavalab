(function () {
    'use strict';

    var newsApp = angular.module('newsApp');

    newsApp.factory('NewsViewService', ['$http', '$q', function ($http, $q) {


        return {
            getNewsById: getNewsById,
            getNewsFromChain: getNewsFromChain
        };


        function getNewsById(id) {
            var deferred = $q.defer();
            $http.get('/admin/newsView', {
                    params: {
                        id: id
                    }
                })
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Users');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }

        function getNewsFromChain(ind, searchTags, searchAuthor, position) {
            var deferred = $q.defer();
            $http.get('/admin/stepNews', {
                    params: {
                        position: position,
                        tagNamesList: searchTags,
                        authorName: searchAuthor,
                        index: ind
                    }
                })
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Users');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }

    }]);
})();
