package com.epam.news.domain;

import org.hibernate.search.annotations.Field;

import javax.persistence.*;

/**
 * Created by Viktoryia_Khlystun on 5/25/2016.
 */
@Entity
@Table(name = "TAG")
public class Tag {

    @Id
    @GeneratedValue(generator = "TagGen")
    @SequenceGenerator(name = "TagGen", sequenceName = "TAG_SEQ")
    @Column(name = "TAG_ID")
    private long id;

    @Field
    @Column(name = "TAG_NAME")
    private String name;

    @Version
    private int version;

    public Tag() {
    }

    public Tag(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tag tag = (Tag) o;

        if (id != tag.id) return false;
        return true;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + name.hashCode();
        return result;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
