BEGIN
      FOR v_Count IN 1..10000 LOOP
       INSERT INTO NEWS(NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE,VERSION)
       VALUES (NEWS_SEQ.nextval,CONCAT('news title',v_count),CONCAT('short text',v_count),CONCAT('full text',v_count),sysdate,sysdate,0);
    END LOOP;
    END;
	
BEGIN
      FOR v_Count IN 1..10000 LOOP
       INSERT INTO AUTHOR(AUTHOR_ID,AUTHOR_NAME,VERSION)
       VALUES (AUTHOR_SEQ.nextval,CONCAT('author',v_count),0);
    END LOOP;
    END;
	
BEGIN
      FOR v_Count IN 1..10000 LOOP
       INSERT INTO TAG(TAG_ID,TAG_NAME,VERSION)
       VALUES (TAG_SEQ.nextval,CONCAT('tag',v_count),0);
    END LOOP;
    END;
	
	
CREATE OR REPLACE FUNCTION md5(p_in VARCHAR2)
       RETURN VARCHAR2
    IS
       l_hash   VARCHAR2 (2000);
    BEGIN
       l_hash :=
          RAWTOHEX
             (UTL_RAW.cast_to_raw
                  (DBMS_OBFUSCATION_TOOLKIT.md5 (input_string      => p_in)
                   )
             );
       RETURN l_hash;
    END;

BEGIN
      FOR v_Count IN 1..10000 LOOP
       INSERT INTO USERS(USER_ID,USER_NAME,LOGIN,PASSWORD)
       VALUES (USER_SEQ.nextval,CONCAT('user',v_count),CONCAT('login',v_count),md5(CONCAT('password',v_count)));
    END LOOP;
    END;
	
BEGIN
      FOR v_Count IN (select USER_ID from USERS where LOGIN like 'login%') LOOP
       INSERT INTO ROLES(USER_ID,ROLE_NAME)
       VALUES (v_Count.USER_ID, 'ROLE_USER');
    END LOOP;
    END;
	
	
BEGIN
      FOR v_Count IN (select NEWS_ID from NEWS where TITLE like '%title%') LOOP
       INSERT INTO COMMENTS(COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE)
       VALUES (COMMENT_SEQ.nextval,v_Count.NEWS_ID, CONCAT('comment',v_Count.NEWS_ID),sysdate);
    END LOOP;
    END;
	
	
DECLARE 
varLoop number:=1000;
varT number:= 0;
BEGIN
      FOR v_Count IN (select NEWS_ID from NEWS where TITLE like '%title%') LOOP
       INSERT INTO NEWS_TAG(NEWS_ID,TAG_ID)
       VALUES (v_Count.NEWS_ID, varLoop);
       INSERT INTO NEWS_AUTHOR(NEWS_ID,AUTHOR_ID)
       VALUES (v_Count.NEWS_ID,varLoop);
       varT:=varT + 1;
       IF MOD(varT,2) = 0 THEN varLoop:=varLoop + 1;
       END IF;
    END LOOP;
    END;