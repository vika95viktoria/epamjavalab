package com.epam.news.service.impl;

import com.epam.news.dao.TagDAO;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.TagService;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/31/2016.
 */

public class TagServiceImpl implements TagService {
    private TagDAO tagDAO;

    public void setTagDAO(TagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }

    /**
     * Add tags to news
     *
     * @param tagIds
     * @param newsId
     * @throws ServiceException
     */
    public void addTagsToNews(List<Long> tagIds, long newsId) throws ServiceException {
        try {
            for (Long id : tagIds) {
                tagDAO.addTagToNews(newsId, id);
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Create a record describing new tag
     *
     * @param tag
     * @throws ServiceException
     */
    public void create(Tag tag) throws ServiceException {
        try {
            tagDAO.create(tag);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Return all tag info by id
     *
     * @param tagId
     * @return
     * @throws ServiceException
     */
    public Tag findTagById(long tagId) throws ServiceException {
        Tag tag;
        try {
            tag = tagDAO.findTagById(tagId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return tag;
    }

    /**
     * Delete tags related to news when you delete news
     *
     * @param newsId
     * @throws ServiceException
     */
    public void deleteTagsFromNews(long newsId) throws ServiceException {
        try {
            tagDAO.deleteTagsFromNews(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
