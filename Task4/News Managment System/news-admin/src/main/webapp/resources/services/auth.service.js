(function () {
    'use strict';

    var newsApp = angular.module('newsApp');

    newsApp.factory('Auth', ['$http', '$location', function ($http, $location) {

        var user;

        return {
            setUser: setUser,
            isLoggedIn: isLoggedIn,
            resetUser: resetUser,
            logout: logout
        }

        function setUser(aUser) {
            user = aUser;
        }

        function isLoggedIn() {
            if (!!user) {
                return user;
            }
            return false;
        }

        function resetUser() {
            user = false;
        }

        function logout() {
            $http.post('/admin/logout', {}).success(function () {
                resetUser();
                $location.path("/login");
            }).error(function (data) {
                resetUser();
            });
        }

    }])
})();