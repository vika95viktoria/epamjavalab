package com.epam.news.dao;

import com.epam.news.domain.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/testContext.xml"})
@DatabaseSetup(value = "/data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class AuthorDAOTest {
    private final static Logger logger = Logger.getLogger(AuthorDAOTest.class);
    private static AuthorDAO authorDAO;
    private static ApplicationContext context;

    @BeforeClass
    public static void setUpAuthorDAO() {
        context = new ClassPathXmlApplicationContext("testContext.xml");
        authorDAO = (AuthorDAO) context.getBean("AuthorDAO");
    }

    @AfterClass
    public static void closeContext() {
        ((ConfigurableApplicationContext) context).close();
    }


    @Test
    public void findAuthorById() {
        Author author = authorDAO.findAuthorById(1);
        assertEquals(author.getId(), 1);
        assertEquals(author.getName(), "Polina Petrova");
    }

    @Test
    public void delete() {
        Author author = authorDAO.findAuthorById(1);
        Date expired = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            expired = formatter.parse(formatter.format(expired));
        } catch (ParseException e) {
            logger.error(e);
        }
        author.setExpired(expired);
        authorDAO.update(author);
        author = authorDAO.findAuthorById(1);
        assertEquals(expired, author.getExpired());
    }

    @Test
    public void update() {
        Author author = authorDAO.findAuthorById(1);
        author.setName("Alina");
        authorDAO.update(author);
        Author updatedAuthor = authorDAO.findAuthorById(1);
        assertEquals(author.getName(), updatedAuthor.getName());
        assertEquals(author.getId(), updatedAuthor.getId());
    }


}
