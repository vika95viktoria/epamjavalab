<%@ page language="java" contentType="text/html;charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
<head>
    <link rel="stylesheet" type="text/css"
          href="<c:url value="/resources/css/style.css"/>" />
    <title><tiles:insertAttribute name="title" ignore="true" /></title>
    <c:set var="title" scope="request"><tiles:getAsString name='title'/></c:set>
</head>
<body>
<div class="main">
    <div class="container bordered">
        <tiles:insertAttribute name="header" />
        <tiles:insertAttribute name="body" />
    </div>
    <tiles:insertAttribute name="footer" />
</div>
</body>
</html>