package com.epam.news.dao.hibernate;

import com.epam.news.dao.AbstractDAO;
import com.epam.news.dao.CommentDAO;
import com.epam.news.domain.Comment;
import com.epam.news.domain.News;
import com.epam.news.model.CommentModel;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */

@Repository
@Profile("hibernate")
public class CommentDAOImpl extends AbstractDAO implements CommentDAO {

    /**
     * Create comment and link it to news
     *
     * @param comment
     * @param newsId
     */
    public CommentModel create(Comment comment, long newsId) {
        Session session = sessionFactory.getCurrentSession();
        comment.setCreationDate(new Date());
        News news = session.load(News.class, newsId);
        comment.setNews(news);
        session.save(comment);
        return new CommentModel(comment);
    }

    /**
     * Return list of comments to news
     *
     * @param newsId
     * @return
     */
    public List<Comment> findCommentsByNewsId(long newsId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select n.comments from News as n where n.id = :newsId");
        query.setParameter("newsId", newsId);
        List<Comment> comments = query.list();
        return comments;
    }

    /**
     * Delete comment from database
     *
     * @param comment
     */


    public void delete(Comment comment) {
        Session session = sessionFactory.getCurrentSession();
        comment = session.load(Comment.class, comment.getId());
        session.delete(comment);
    }
}
