package com.epam.news.controller;

import com.epam.news.domain.News;
import com.epam.news.model.NewsModel;
import com.epam.news.service.NewsService;
import com.epam.news.util.NewsCreateValidator;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Created by Viktoryia_Khlystun on 7/4/2016.
 */
@Controller
public class NewsController {

    @Autowired
    private NewsService newsService;

    /**
     * Adds news to database and redirect to newsView page without links to previous and next news
     *
     * @param newsModel
     * @return
     */
    @RequestMapping(value = "/addNews", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
    public ModelAndView addNews(@ModelAttribute NewsModel newsModel, BindingResult result, RedirectAttributes redirectAttributes) {
        NewsCreateValidator validator = new NewsCreateValidator();
        validator.validate(newsModel, result);
        ModelAndView model = new ModelAndView();
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", "Not valid info");
            model.setViewName("redirect:/addNews");
        } else {
            newsService.createNews(newsModel);
            News news = newsService.findNewsByTitle(newsModel.getTitle());
            model.addObject(news);
            model.setViewName("newsView");
        }
        return model;
    }

    /**
     * Edits existin record in database and redirect to newsView page with links to previous and next news
     * @param newsModel
     * @param var
     * @return
     */
    @RequestMapping(value = "/editNews", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
    public String editNews(@ModelAttribute NewsModel newsModel, @RequestParam String var, BindingResult result, RedirectAttributes redirectAttributes) {
        NewsCreateValidator validator = new NewsCreateValidator();
        validator.validate(newsModel, result);
        News news;
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", "Not valid info");
            return "redirect:/addNews";
        } else {
            try {
                news = newsService.findNewsById(newsModel.getId());
                newsService.updateNews(newsModel);
            } catch (JpaOptimisticLockingFailureException | HibernateOptimisticLockingFailureException e) {
                redirectAttributes.addFlashAttribute("error", "News have been edited. Try again, please");
                return "redirect:/editNews?id=" + newsModel.getId() + "&var=" + var;
            } catch (IllegalArgumentException | ObjectNotFoundException e) {
                redirectAttributes.addFlashAttribute("error", "News have already been deleted.");
                return "redirect:/addNews";
            }
            String newsView = "redirect:/newsView?id=" + news.getId() + "&var=" + var;
            return newsView;
        }
    }

}
