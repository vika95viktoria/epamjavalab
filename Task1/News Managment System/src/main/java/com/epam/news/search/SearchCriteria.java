package com.epam.news.search;

import com.epam.news.domain.Author;
import com.epam.news.domain.Tag;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 6/1/2016.
 */
public class SearchCriteria {
    private Author author;
    private List<Tag> tags;

    public SearchCriteria() {
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}
