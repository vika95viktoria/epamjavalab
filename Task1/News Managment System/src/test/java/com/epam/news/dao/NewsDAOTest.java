package com.epam.news.dao;

import com.epam.news.domain.Author;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.search.SearchCriteria;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Viktoryia_Khlystun on 6/3/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/testContext.xml"})
@DatabaseSetup(value = "/data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class NewsDAOTest {
    private static NewsDAO newsDAO;
    private static ApplicationContext context;


    @BeforeClass
    public static void setUpAuthorDAO() {
        context = new ClassPathXmlApplicationContext("testContext.xml");
        newsDAO = (NewsDAO) context.getBean("NewsDAO");
    }

    @AfterClass
    public static void closeContext() {
        ((ConfigurableApplicationContext) context).close();
    }

    @Test
    public void getListOfNews() throws DAOException {
        List<News> news = newsDAO.getListOfNews();
        assertEquals(news.size(), 3);
    }

    @Test
    public void getCountOfNews() throws DAOException {
        int countOfNews = newsDAO.getCountOfNews();
        assertEquals(countOfNews, 3);
    }

    @Test
    public void getMostCommenting() throws DAOException {
        List<Long> mostCommenting = newsDAO.getIdOfMostCommenting();
        assertEquals(mostCommenting.get(0), new Long(1));
    }

    @Test
    public void findById() throws DAOException {
        News news = newsDAO.findNewsById(1);
        assertEquals(news.getId(), 1);
        assertEquals(news.getTitle(), "Paris");
        assertEquals(news.getShortText(), "Paris river to peak at six metres");
        assertEquals(news.getFullText(), "Floodwaters in Paris are forecast to peak on Friday with the River Seine due to reach 6m (19ft) above its normal level");
    }

    @Test
    public void create() throws DAOException {
        News news = new News();
        news.setTitle("New car");
        news.setShortText("Audi presents new model");
        news.setFullText("Audi presents today new model of the car.");
        assertEquals(3, newsDAO.getCountOfNews());
        newsDAO.create(news);
        assertEquals(4, newsDAO.getCountOfNews());
    }

    @Test
    public void delete() throws DAOException {
        News news = new News();
        news.setTitle("New car");
        news.setShortText("Audi presents new model");
        news.setFullText("Audi presents today new model of the car.");
        newsDAO.create(news);
        long toBeDeleted = 0;
        List<News> listOfNews = newsDAO.getListOfNews();
        for (News news1 : listOfNews) {
            if (news1.getTitle().equals(news.getTitle())) {
                toBeDeleted = news1.getId();
            }
        }
        assertEquals(4, newsDAO.getCountOfNews());
        newsDAO.delete(toBeDeleted);
        assertEquals(3, newsDAO.getCountOfNews());
    }

    @Test
    public void update() throws DAOException {
        News news = newsDAO.findNewsById(1);
        news.setTitle("Six metres");
        newsDAO.update(news);
        news = newsDAO.findNewsById(1);
        assertEquals(news.getTitle(), "Six metres");
        assertEquals(news.getId(), 1);
    }

    @Test
    public void searchByAuthor() throws DAOException {
        Author author = new Author();
        author.setName("Ali");
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAuthor(author);
        List<News> listOfNews = newsDAO.getListOfNews(searchCriteria);
        assertEquals(listOfNews.size(), 1);
        assertEquals(listOfNews.get(0).getId(), 1);
    }

    @Test
    public void searchWithoutParameters() throws DAOException {
        SearchCriteria searchCriteria = new SearchCriteria();
        List<News> listOfNews = newsDAO.getListOfNews(searchCriteria);
        assertEquals(listOfNews.size(), 0);
    }

    @Test
    public void searchByOneTag() throws DAOException {
        SearchCriteria searchCriteria = new SearchCriteria();
        Tag tag = new Tag();
        tag.setId(2);
        tag.setName("Politics");
        List<Tag> tags = new ArrayList<>();
        tags.add(tag);
        searchCriteria.setTags(tags);
        List<News> listOfNews = newsDAO.getListOfNews(searchCriteria);
        assertEquals(listOfNews.size(), 2);
    }

    @Test
    public void searchByTags() throws DAOException {
        SearchCriteria searchCriteria = new SearchCriteria();
        Tag tag = new Tag();
        tag.setName("Politics");
        Tag tag2 = new Tag();
        tag2.setName("World");
        List<Tag> tags = new ArrayList<>();
        tags.add(tag);
        tags.add(tag2);
        searchCriteria.setTags(tags);
        List<News> listOfNews = newsDAO.getListOfNews(searchCriteria);
        assertEquals(listOfNews.size(), 3);
    }

    @Test
    public void searchByOneTagAndAuthor() throws DAOException {
        SearchCriteria searchCriteria = new SearchCriteria();
        Tag tag = new Tag();
        tag.setName("Politics");
        List<Tag> tags = new ArrayList<>();
        tags.add(tag);
        Author author = new Author();
        author.setName("Ali");
        searchCriteria.setTags(tags);
        searchCriteria.setAuthor(author);
        List<News> listOfNews = newsDAO.getListOfNews(searchCriteria);
        assertEquals(listOfNews.size(), 1);
    }

    @Test
    public void searchByTagsAndAuthor() throws DAOException {
        SearchCriteria searchCriteria = new SearchCriteria();
        List<Tag> tags = new ArrayList<>();
        Tag tag = new Tag();
        tag.setName("Politics");
        Tag tag2 = new Tag();
        tag2.setName("World");
        tags.add(tag);
        tags.add(tag2);
        Author author = new Author();
        author.setName("Ivan");
        searchCriteria.setTags(tags);
        searchCriteria.setAuthor(author);
        List<News> listOfNews = newsDAO.getListOfNews(searchCriteria);
        assertEquals(listOfNews.size(), 2);
    }

}
