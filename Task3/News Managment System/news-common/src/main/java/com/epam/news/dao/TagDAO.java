package com.epam.news.dao;

import com.epam.news.domain.Tag;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
public interface TagDAO {

    Tag findTagById(long tagId);

    void create(Tag tag);

    List<Tag> findAll();

    int getCountOfTags();

    List<Tag> findAll(long start);

    void delete(Tag entity);

    void update(Tag tag);
}
