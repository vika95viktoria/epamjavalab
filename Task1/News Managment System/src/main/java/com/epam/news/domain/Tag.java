package com.epam.news.domain;

/**
 * Created by Viktoryia_Khlystun on 5/25/2016.
 */
public class Tag {
    private long id;
    private String name;

    public Tag() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
