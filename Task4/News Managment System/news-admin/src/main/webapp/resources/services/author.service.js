(function () {
    'use strict';

    var newsApp = angular.module('newsApp');

    newsApp.factory('AuthorService', ['$http', '$q', function ($http, $q) {

        return {
            getAuthors: getAuthors,
            getCount: getCount,
            createAuthor: createAuthor,
            updateAuthor: updateAuthor,
            deleteAuthor: deleteAuthor
        };


        function getAuthors(page) {
            var deferred = $q.defer();
            $http.get('/admin/authorPage', {
                    params: {
                        page: page
                    }
                })
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Users');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }

        function getCount() {
            var deferred = $q.defer();
            $http.get('/admin/authorCount')
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching count of Tags');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }

        function createAuthor(authorName) {
            var deferred = $q.defer();
            $http.post('/admin/addAuthor', $.param({
                authorName: authorName
            }), {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while adding Author ' + authorName);
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }

        function updateAuthor(author) {
            var deferred = $q.defer();
            $http.post('/admin/updateAuthor', author).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while updating Author ' + author.id);
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }

        function deleteAuthor(author) {
            var deferred = $q.defer();
            $http.post('/admin/deleteAuthor', author).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while deleting Author ' + author.id);
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }


    }])
})();
