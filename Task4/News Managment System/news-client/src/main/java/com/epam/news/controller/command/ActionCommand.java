package com.epam.news.controller.command;

import com.epam.news.service.AuthorService;
import com.epam.news.service.CommentService;
import com.epam.news.service.NewsService;
import com.epam.news.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Viktoryia_Khlystun on 6/23/2016.
 */


public abstract class ActionCommand {

    @Autowired
    protected NewsService newsService;

    @Autowired
    protected TagService tagService;

    @Autowired
    protected AuthorService authorService;

    @Autowired
    protected CommentService commentService;


    /**
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void action(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

}
