<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="newsContainer" style="border: dashed black 1px; height: 82%; width: 77%; ">
    <form action="<c:url value="/newsList" />" method="get">
        <div class="selector" style="text-align: center">
            <select id="authorSelect" style="display: inline-block; height: 27px" name="author">
                <option disabled selected><spring:message code="select.author"/></option>
                <c:forEach var="element" items="${authors}">
                    <option value="${element.name}">${element.name}</option>
                </c:forEach>
            </select>

            <select  multiple="multiple" style="display: inline-block" name="tag">
                <c:forEach var="element" items="${tags}">
                    <option value="${element.name}">${element.name}</option>
                </c:forEach>
            </select>
            <button id="resetButton" style="height: 27px" data-href="<c:url value="/newsList" />"><spring:message
                    code="button.reset"/></button>
            <button type="submit" style="height:27px"><spring:message code="button.filter"/></button>
        </div>
    </form>

    <form action="<c:url value="/deleteNews" />" method="post">
    <c:forEach var="element" items="${news}" varStatus="status">
        <div class="news">
            <span class="newsTitle">${element.title}</span>
            <span>(by </span>
            <c:forEach var="elem" items="${element.authors}" varStatus="loop">
                ${elem.name}
                <c:if test="${fn:length(element.authors) != loop.index+1}">
                    <span>, </span>
                </c:if>
            </c:forEach>
            <span>)</span>
            <a href="" class="newsDate">${element.modificationDate}</a>
            <br>
            <br>
            <br>
            <span> ${element.shortText}</span>
            <br>
            <div class="comments">
                <c:forEach var="elem" items="${element.tags}" varStatus="loop">
                    <span>${elem.name}</span>
                    <c:if test="${fn:length(element.tags) != loop.index+1}">
                        <span>,</span>
                    </c:if>
                </c:forEach>
                <span>  </span>
                <span>  </span>
                <span style="color:red; padding-left: 10px; padding-right: 10px"><spring:message
                        code="comments"/>(${fn:length(element.comments)})</span>
                <a href="<c:url value="/newsView?id=${element.id}&var=${status.index}" />"
                   style="color:blue"><spring:message code="view"/></a>
                <a href="<c:url value="/editNews?id=${element.id}&var=${status.index}" />"
                   style="color:blue"><spring:message code="edit"/></a>

                <input type="checkbox" name="deleteId" value="${element.id}">
            </div>
            <br>
            <br>
            <br>
        </div>
    </c:forEach>
    <div >
        <button type="submit" class="loginButton" style="float:right; margin-right: 9px "><spring:message
                code="delete"/></button>
    </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
    <div class="pages" style="bottom: -10%">
        <div style="display: inline-block;">
            <ul class="pagination">
                <form action="<c:url value="/newsList" />" method="get" id="pagingForm">
                    <c:forEach var="i" begin="1" end="${count}">
                        <li><button type="submit" onclick="setPage(this)" value="${i}" id = "button${i}">${i}</button></li>
                    </c:forEach>
                    <input type="hidden" name="page" id="pageNum">
                </form>
            </ul>
        </div>
    </div>
</div>
<script>

    $('#resetButton').click(function(e) {
        e.preventDefault(); e.stopPropagation();
        window.location.href = $(e.currentTarget).data().href;
    });

    $(function(){
        pagingUtil();
        parse();
    });
    $('#resetButton').click(function(e) {
        e.preventDefault(); e.stopPropagation();
        window.location.href = $(e.currentTarget).data().href;
    });
    function parse() {
        var result = [],
                tmp = [];
        var container = document.getElementById("pagingForm");
        var items = location.search.substr(1).split("&");
        for (var index = 0; index < items.length; index++) {
            tmp = items[index].split("=");
            if(!(tmp[0] == "page" || tmp[0] == "command")) {
                var x = document.createElement("input");
                x.setAttribute("type", "hidden");
                x.setAttribute("name", tmp[0]);
                x.setAttribute("value", tmp[1]);
                container.appendChild(x);
            }
        }
        return result;
    }
</script>