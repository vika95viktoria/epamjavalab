package com.epam.news.dao.jpa;

import com.epam.news.dao.AbstractDAO;
import com.epam.news.dao.TagDAO;
import com.epam.news.domain.Tag;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 7/19/2016.
 */
@Repository
@Profile("jpa")
public class TagDAOImpl extends AbstractDAO implements TagDAO {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Return all tag info by id
     *
     * @param tagId
     * @return
     */
    public Tag findTagById(long tagId) {
        Tag tag = (Tag) entityManager.find(Tag.class, tagId);
        return tag;
    }

    /**
     * get list of all tags in system
     *
     * @return
     */
    public List<Tag> findAll() {
        List<Tag> tags = entityManager
                .createQuery("Select tag from Tag tag", Tag.class)
                .getResultList();
        return tags;
    }

    /**
     * get count of tags in system
     *
     * @return
     */

    public int getCountOfTags() {
        int count = ((Long) entityManager
                .createQuery("Select COUNT(tag.id) from Tag tag").getSingleResult()).intValue();
        return count;
    }

    /**
     * get list of tags in limits
     *
     * @param start
     * @return
     */
    public List<Tag> findAll(long start) {
        TypedQuery<Tag> query = entityManager
                .createQuery("Select tag from Tag tag", Tag.class);
        query.setFirstResult((int) start);
        query.setMaxResults(7);
        List<Tag> tags = query.getResultList();
        return tags;
    }

    /**
     * Create new record in database with all tag info
     *
     * @param tag
     */
    public void create(Tag tag) {
        entityManager.persist(tag);
    }

    /**
     * delete tag from database
     *
     * @param entity
     */
    public void delete(Tag entity) {
        entity = entityManager.find(Tag.class, entity.getId());
        entityManager.remove(entity);
    }

    /**
     * update the name of tag
     *
     * @param tag
     */
    public void update(Tag tag) {
        String name = tag.getName();
        int version = tag.getVersion();
        tag = entityManager.find(Tag.class, tag.getId());
        tag.setName(name);
        tag.setVersion(version);
    }
}
