package com.epam.news.service;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.dao.CommentDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.dao.TagDAO;
import com.epam.news.domain.News;
import com.epam.news.model.NewsModel;
import com.epam.news.search.SearchCriteria;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
public interface NewsService {
    List<News> getNewsOrderedByComments(long start);

    void createNews(NewsModel newsModel);

    void deleteNews(Long newsId);

    News findNewsById(Long newsId);

    int getCountOfNews();

    void updateNews(NewsModel newsModel);

    List<News> searchForNews(SearchCriteria searchCriteria, long start, long end);

    List<News> getNewsByIds(List<Long> ids);

    void setNewsDAO(NewsDAO newsDAO);

    void setTagDAO(TagDAO tagDAO);

    void setAuthorDAO(AuthorDAO authorDAO);

    void setCommentDAO(CommentDAO commentDAO);

    int getCountOfNews(SearchCriteria searchCriteria);

    News findNewsByTitle(String title);
}
