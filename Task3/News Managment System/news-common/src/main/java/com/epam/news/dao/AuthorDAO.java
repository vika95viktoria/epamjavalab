package com.epam.news.dao;

import com.epam.news.domain.Author;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
public interface AuthorDAO {

    void create(Author author);

    Author findAuthorById(long authorId);

    void update(Author entity);

    List<Author> findAll();

    List<Author> findAll(long start);

    int getCountOfAuthors();

}
