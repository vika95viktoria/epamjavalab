package com.epam.news.dao.jpa;

import com.epam.news.dao.AbstractDAO;
import com.epam.news.dao.AuthorDAO;
import com.epam.news.domain.Author;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 7/19/2016.
 */
@Repository
@Profile("jpa")
public class AuthorDAOImpl extends AbstractDAO implements AuthorDAO {

    @PersistenceContext
    private EntityManager entityManager;


    /**
     * Return all author info by id
     *
     * @param authorId
     * @return
     */
    public Author findAuthorById(long authorId) {
        Author author = (Author) entityManager.find(Author.class, authorId);
        return author;
    }

    /**
     * Get list of all authors in system
     *
     * @return
     */
    public List<Author> findAll() {
        List<Author> authors = entityManager
                .createQuery("Select author from Author author", Author.class)
                .getResultList();
        return authors;
    }

    /**
     * get list of authors in limits
     *
     * @param start
     * @return
     */
    public List<Author> findAll(long start) {
        TypedQuery<Author> query = entityManager
                .createQuery("Select author from Author author", Author.class);
        query.setFirstResult((int) start);
        query.setMaxResults(7);
        List<Author> authors = query.getResultList();
        return authors;
    }

    /**
     * get count of tags in system
     *
     * @return
     */

    public int getCountOfAuthors() {
        int count = ((Long) entityManager
                .createQuery("Select COUNT(author.id) from Author author").getSingleResult()).intValue();
        return count;
    }

    /**
     * Add new record with author info to database
     *
     * @param author
     */
    public void create(Author author) {
        entityManager.persist(author);
    }


    /**
     * Change author's name
     *
     * @param entity
     */
    public void update(Author entity) {
        Date expired = entity.getExpired();
        String name = entity.getName();
        int version = entity.getVersion();
        entity = entityManager.find(Author.class, entity.getId());
        entity.setName(name);
        entity.setVersion(version);
        if (expired != null) {
            entity.setExpired(expired);
        }
    }

}
