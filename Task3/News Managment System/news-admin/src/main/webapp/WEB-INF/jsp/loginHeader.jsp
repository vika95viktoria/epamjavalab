<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<header>
    <div  class="admin">
        <div class="clearfix" style="width: 100%">
            <h1 style="color: blue; margin-left: 20px">${requestScope.title}</h1>
            <div class="language-box">

                <a href="?lang=en" style="color: blue;">EN</a>
                <a href="?lang=ru" style="color: blue;">RU</a>
            </div>
        </div>
    </div>
</header>