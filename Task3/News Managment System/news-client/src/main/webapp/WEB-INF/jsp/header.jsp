<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:setBundle basename="pagecontent"/>
<header>
    <div  class="admin">
        <div class="clearfix" style="width: 100%">
    <h1 style="color: blue; margin-left: 20px">${requestScope.title}</h1>
    <div class="language-box">
        <a href onclick="changeToEng()" style="color: blue;"><fmt:message key="label.eng"/></a>
        <a href onclick="changeToRus()" style="color: blue;"><fmt:message key="label.rus"/></a>
    </div>
    </div>
    </div>
</header>
