package com.epam.news.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

/**
 * Created by Viktoryia_Khlystun on 6/20/2016.
 */

@Controller
public class AdminController {

    @RequestMapping("/user")
    public Principal user(Principal user) {
        return user;
    }

}
