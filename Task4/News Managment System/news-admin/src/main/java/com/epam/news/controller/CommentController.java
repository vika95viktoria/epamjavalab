package com.epam.news.controller;

import com.epam.news.domain.Comment;
import com.epam.news.model.CommentModel;
import com.epam.news.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Viktoryia_Khlystun on 7/4/2016.
 */
@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    /**
     * Adds comment to database and returns newsView page with updates (new comment is in list of comments)
     *
     * @param commentText
     * @param newsId
     * @return
     */
    @RequestMapping(value = "/addComment", method = RequestMethod.POST)
    public CommentModel addComment(@RequestParam(value = "commentText") String commentText, @RequestParam(value = "newsId") Long newsId) {
        Comment comment = new Comment(commentText);
        CommentModel commentModel = commentService.addComment(newsId, comment);
        return commentModel;
    }

    /**
     * Deletes comment from database and returns newsView page with updates (deleted comment doesn;t display in the list of comments)
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/deleteComment", method = RequestMethod.POST)
    public ResponseEntity deleteComment(@RequestParam Long id) {
        Comment comment = new Comment(id);
        commentService.deleteComment(comment);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(comment);
    }
}
