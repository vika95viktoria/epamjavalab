package com.epam.news.util;

/**
 * Created by Viktoryia_Khlystun on 7/7/2016.
 */
public class RequestParser {
    /**
     * Fixes the problem with encoding char + in long search requests
     *
     * @param name
     * @return
     */
    public static String plusParser(String name){
        if(name != null) {
            if (name.contains("+")) {
                return name.replace("+", " ");
            }
            if (name.contains("%2B")) {
                return name.replace("%2B", " ");
            }
        }
        return name;
    }
}
