package com.epam.news.domain;

import com.epam.news.model.NewsModel;
import org.apache.log4j.Logger;
import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;

import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/25/2016.
 */

@Indexed
@Entity
@Table(name = "NEWS")
public class News {

    private static final Logger logger = Logger.getLogger(News.class);
    @DocumentId
    @Id
    @GeneratedValue(generator = "NewsGen")
    @SequenceGenerator(name = "NewsGen", sequenceName = "NEWS_SEQ")
    @Column(name = "NEWS_ID")
    private long id;
    private String title;
    @Column(name = "SHORT_TEXT")
    private String shortText;
    @Column(name = "FULL_TEXT")
    private String fullText;
    @Column(name = "CREATION_DATE")
    @Temporal(TemporalType.DATE)
    private Date creationDate;
    @Column(name = "MODIFICATION_DATE")
    @Temporal(TemporalType.DATE)
    private Date modificationDate;
    @IndexedEmbedded
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "NEWS_TAG",
            joinColumns = @JoinColumn(name = "NEWS_ID", referencedColumnName = "NEWS_ID"),
            inverseJoinColumns = @JoinColumn(name = "TAG_ID", referencedColumnName = "TAG_ID"))
    private List<Tag> tags;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "news")
    private List<Comment> comments;
    @IndexedEmbedded
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "NEWS_AUTHOR",
            joinColumns = @JoinColumn(name = "NEWS_ID", referencedColumnName = "NEWS_ID"),
            inverseJoinColumns = @JoinColumn(name = "AUTHOR_ID", referencedColumnName = "AUTHOR_ID"))
    private List<Author> authors;

    @Version
    private int version;

    public News() {
    }

    public News(NewsModel newsModel) {
        this.setVersion(newsModel.getVersion());
        this.setId(newsModel.getId());
        this.setFullText(newsModel.getFullText());
        this.setShortText(newsModel.getShortText());
        this.setTitle(newsModel.getTitle());
        Date creationDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            creationDate = formatter.parse(newsModel.getModificationDate());
        } catch (ParseException e) {
            logger.error(e);
        }
        this.setCreationDate(creationDate);
        this.setModificationDate(creationDate);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        News news = (News) o;

        if (id != news.id) return false;
        return title.equals(news.title);

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + title.hashCode();
        return result;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
