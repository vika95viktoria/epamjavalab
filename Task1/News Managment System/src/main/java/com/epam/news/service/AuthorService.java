package com.epam.news.service;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.domain.Author;
import com.epam.news.exception.ServiceException;

/**
 * Created by Viktoryia_Khlystun on 5/31/2016.
 */
public interface AuthorService {
    void addAuthorToNews(long authorId, long newsId) throws ServiceException;

    void create(Author author) throws ServiceException;

    void delete(Author author) throws ServiceException;

    void update(Author author) throws ServiceException;

    Author getAuthorById(long id) throws ServiceException;

    void setAuthorDAO(AuthorDAO authorDAO);
}
