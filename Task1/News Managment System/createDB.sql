--------------------------------------------------------
--  File created - Monday-June-06-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Sequence AUTHOR_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "VIKA"."AUTHOR_SEQ"  MINVALUE 4 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 64 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence COMMENT_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "VIKA"."COMMENT_SEQ"  MINVALUE 2 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 82 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence NEWS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "VIKA"."NEWS_SEQ"  MINVALUE 3 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 63 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence TAG_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "VIKA"."TAG_SEQ"  MINVALUE 6 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 66 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence USER_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "VIKA"."USER_SEQ"  MINVALUE 2 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 22 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table AUTHOR
--------------------------------------------------------

  CREATE TABLE "VIKA"."AUTHOR" 
   (	"AUTHOR_ID" NUMBER(20,0) NOT NULL, 
	"AUTHOR_NAME" VARCHAR2(30 BYTE) NOT NULL, 
	"EXPIRED" TIMESTAMP (6)
   ) 
--------------------------------------------------------
--  DDL for Table COMMENTS
--------------------------------------------------------

  CREATE TABLE "VIKA"."COMMENTS" 
   (	"COMMENT_ID" NUMBER(20,0) NOT NULL, 
	"NEWS_ID" NUMBER(20,0) NOT NULL, 
	"COMMENT_TEXT" NVARCHAR2(100) NOT NULL, 
	"CREATION_DATE" TIMESTAMP (6) NOT NULL
   ) 
--------------------------------------------------------
--  DDL for Table NEWS
--------------------------------------------------------

  CREATE TABLE "VIKA"."NEWS" 
   (	"NEWS_ID" NUMBER(20,0) NOT NULL, 
	"TITLE" NVARCHAR2(30) NOT NULL, 
	"SHORT_TEXT" NVARCHAR2(100) NOT NULL, 
	"FULL_TEXT" NVARCHAR2(2000) NOT NULL, 
	"CREATION_DATE" TIMESTAMP (6) NOT NULL, 
	"MODIFICATION_DATE" DATE NOT NULL
   ) 
--------------------------------------------------------
--  DDL for Table NEWS_AUTHOR
--------------------------------------------------------

  CREATE TABLE "VIKA"."NEWS_AUTHOR" 
   (	"NEWS_ID" NUMBER(20,0) NOT NULL, 
	"AUTHOR_ID" NUMBER(20,0) NOT NULL
   ) 
--------------------------------------------------------
--  DDL for Table NEWS_TAG
--------------------------------------------------------

  CREATE TABLE "VIKA"."NEWS_TAG" 
   (	"NEWS_ID" NUMBER(20,0) NOT NULL, 
	"TAG_ID" NUMBER(20,0) NOT NULL
   ) 
--------------------------------------------------------
--  DDL for Table ROLES
--------------------------------------------------------

  CREATE TABLE "VIKA"."ROLES" 
   (	"USER_ID" NUMBER(20,0) NOT NULL, 
	"ROLE_NAME" VARCHAR2(50 BYTE) NOT NULL
   ) 
--------------------------------------------------------
--  DDL for Table TAG
--------------------------------------------------------

  CREATE TABLE "VIKA"."TAG" 
   (	"TAG_ID" NUMBER(20,0) NOT NULL, 
	"TAG_NAME" VARCHAR2(30 BYTE) NOT NULL
   ) 
--------------------------------------------------------
--  DDL for Table USERS
--------------------------------------------------------

  CREATE TABLE "VIKA"."USERS" 
   (	"USER_ID" NUMBER(20,0) NOT NULL, 
	"USER_NAME" NVARCHAR2(50) NOT NULL, 
	"LOGIN" VARCHAR2(30 BYTE) NOT NULL, 
	"PASSWORD" VARCHAR2(30 BYTE) NOT NULL
   ) 
--------------------------------------------------------
--  DDL for Index USERS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "VIKA"."USERS_PK" ON "VIKA"."USERS" ("USER_ID") 
 
--------------------------------------------------------
--  DDL for Index TAG_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "VIKA"."TAG_PK" ON "VIKA"."TAG" ("TAG_ID") 
 
--------------------------------------------------------
--  DDL for Index AUTHOR_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "VIKA"."AUTHOR_PK" ON "VIKA"."AUTHOR" ("AUTHOR_ID") 

--------------------------------------------------------
--  DDL for Index NEWS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "VIKA"."NEWS_PK" ON "VIKA"."NEWS" ("NEWS_ID") 
  
--------------------------------------------------------
--  DDL for Index COMMENTS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "VIKA"."COMMENTS_PK" ON "VIKA"."COMMENTS" ("COMMENT_ID") 
  
--------------------------------------------------------
--  Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "VIKA"."USERS" ADD CONSTRAINT "USERS_PK" PRIMARY KEY ("USER_ID")

 

 
--------------------------------------------------------
--  Constraints for Table AUTHOR
--------------------------------------------------------

  ALTER TABLE "VIKA"."AUTHOR" ADD CONSTRAINT "AUTHOR_PK" PRIMARY KEY ("AUTHOR_ID")

  
--------------------------------------------------------
--  Constraints for Table NEWS
--------------------------------------------------------

  ALTER TABLE "VIKA"."NEWS" ADD CONSTRAINT "NEWS_PK" PRIMARY KEY ("NEWS_ID")
  
 

--------------------------------------------------------
--  Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "VIKA"."COMMENTS" ADD CONSTRAINT "COMMENTS_PK" PRIMARY KEY ("COMMENT_ID")
 
  
--------------------------------------------------------
--  Constraints for Table TAG
--------------------------------------------------------

  ALTER TABLE "VIKA"."TAG" ADD CONSTRAINT "TAG_PK" PRIMARY KEY ("TAG_ID")
 
 
--------------------------------------------------------
--  Ref Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "VIKA"."COMMENTS" ADD CONSTRAINT "COMMENTS_NEWS_FK" FOREIGN KEY ("NEWS_ID")
	  REFERENCES "VIKA"."NEWS" ("NEWS_ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_AUTHOR
--------------------------------------------------------

  ALTER TABLE "VIKA"."NEWS_AUTHOR" ADD CONSTRAINT "NEWS_AUTHOR_FK1" FOREIGN KEY ("AUTHOR_ID")
	  REFERENCES "VIKA"."AUTHOR" ("AUTHOR_ID") ENABLE;
  ALTER TABLE "VIKA"."NEWS_AUTHOR" ADD CONSTRAINT "NEWS_AUTHOR_FK2" FOREIGN KEY ("NEWS_ID")
	  REFERENCES "VIKA"."NEWS" ("NEWS_ID")  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_TAG
--------------------------------------------------------

  ALTER TABLE "VIKA"."NEWS_TAG" ADD CONSTRAINT "NEWS_TAG_FK1" FOREIGN KEY ("NEWS_ID")
	  REFERENCES "VIKA"."NEWS" ("NEWS_ID") ENABLE;
  ALTER TABLE "VIKA"."NEWS_TAG" ADD CONSTRAINT "NEWS_TAG_FK2" FOREIGN KEY ("TAG_ID")
	  REFERENCES "VIKA"."TAG" ("TAG_ID")  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ROLES
--------------------------------------------------------

  ALTER TABLE "VIKA"."ROLES" ADD CONSTRAINT "ROLES_FK1" FOREIGN KEY ("USER_ID")
	  REFERENCES "VIKA"."USERS" ("USER_ID") ENABLE;
