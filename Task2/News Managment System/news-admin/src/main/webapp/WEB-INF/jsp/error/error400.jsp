<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>">
    <meta charset="UTF-8">
    <title>400 Error</title>
</head>
<body>
<img src="https://raw.githubusercontent.com/vika95viktoria/EpamCourses/master/LowcostAvia/src/main/webapp/pictures/laptop.png"  style="position: absolute; top:30%; left: 20%; height: 600px">
<div >
<span id="sorry500">
<br>
<br>
    <spring:message code="error400.first"/>
<br>
<spring:message code="error400.second"/>
</span>
        <br>
    <span id="errorMessage">
        ${errorMessage}
    </span>

    </div>
</div>


</body>
</html>
