package com.epam.news.service;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.domain.Author;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/31/2016.
 */
public interface AuthorService {
    void delete(Author author);

    void save(Author author);

    Author getAuthorById(long id);

    void setAuthorDAO(AuthorDAO authorDAO);

    List<Author> findAll();

    List<Author> findAll(long start);

    int getCountOfAuthors();
}
