package com.epam.news.service.impl;

import com.epam.news.dao.CommentDAO;
import com.epam.news.domain.Comment;
import com.epam.news.model.CommentModel;
import com.epam.news.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Viktoryia_Khlystun on 5/31/2016.
 */
@Transactional
@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentDAO commentDAO;

    public void setCommentDAO(CommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

    /**
     * Add new comment to news
     *
     * @param newsId
     * @param comment
     */
    public CommentModel addComment(long newsId, Comment comment) {
        return commentDAO.create(comment, newsId);
    }

    /**
     * Delete comment from discussion
     *
     * @param comment
     */
    public void deleteComment(Comment comment) {
        commentDAO.delete(comment);
    }

}
