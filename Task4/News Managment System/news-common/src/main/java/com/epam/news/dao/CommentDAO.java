package com.epam.news.dao;

import com.epam.news.domain.Comment;
import com.epam.news.model.CommentModel;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
public interface CommentDAO {
    CommentModel create(Comment comment, long newsId);

    List<Comment> findCommentsByNewsId(long newsId);

    void delete(Comment comment);

}
