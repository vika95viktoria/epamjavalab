<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:setBundle basename="pagecontent"/>
<div class="newsContainer"
     style="border: dashed black 1px; width: 98%; height: 80%; margin: 0 auto; top: 2%">

    <form action="news?command=newsList" method="get">
        <div class="selector" style="text-align: center">
            <select id="authorSelect" style="display: inline-block; height: 27px"
                    name="author">
                <option disabled selected><fmt:message key="select.author"/></option>
                <c:forEach var="element" items="${authors}">
                    <option value="${element.name}">${element.name}</option>
                </c:forEach>
            </select> <select multiple="multiple" style="display: inline-block" name="tag">
            <c:forEach var="element" items="${tags}">
                <option value="${element.name}">${element.name}</option>
            </c:forEach>
        </select>
            <button id="resetButton" style="height: 27px"
                    data-href="/client/news?command=newsList">
                <fmt:message key="button.reset"/>
            </button>
            <button type="submit" style="height: 27px">
                <fmt:message key="button.filter"/>
            </button>
        </div>
    </form>
    <c:forEach var="element" items="${sessionScope.news}"
               varStatus="status">
        <div class="news">
            <span class="newsTitle">${element.title}</span> <span>(</span>
            <c:if test="${empty language || language=='EN'}">
                <span>by</span>
            </c:if>
            <c:forEach var="elem" items="${element.authors}" varStatus="loop">
                ${elem.name}
                <c:if test="${fn:length(element.authors) != loop.index+1}">
                    <span>, </span>
                </c:if>
            </c:forEach>
            <span>)</span> <a href="" class="newsDate">${element.modificationDate}</a>
            <br> <br> <br> <span> ${element.shortText}</span> <br>

            <div class="comments">
                <c:forEach var="elem" items="${element.tags}" varStatus="loop">
                    <span>${elem.name}</span>
                    <c:if test="${fn:length(element.tags) != loop.index+1}">
                        <span>,</span>
                    </c:if>
                </c:forEach>
                <span> </span> <span
                    style="color: red; padding-left: 10px; padding-right: 10px"><fmt:message
                    key="comments"/>(${fn:length(element.comments)})</span> <a
                    href="news?command=newsView&id=${element.id}&var=${status.index}"
                    style="color: blue"><fmt:message key="view"/></a>
            </div>
            <br> <br> <br>
        </div>
    </c:forEach>
    <div class="pages" style="bottom: -12%">
        <div style="display: inline-block;">
            <ul class="pagination">
                <form id="pagingForm" method="get" action="news">
                    <input type="hidden" name="command" value="newsList"/>
                    <c:forEach var="i" begin="1" end="${count}">

                        <li>
                            <button type="submit" onclick="setPage(this)"
                                    value="${i}" id="button${i}">${i}</button>
                        </li>
                    </c:forEach>
                    <input type="hidden" name="page" id="pageNum">
                </form>
            </ul>
        </div>
    </div>
</div>

<script>
    function setPage(pageNumber) {
        $("#pageNum").val(pageNumber.value);
        pagingUtil();
    }

    $('#resetButton').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        window.location.href = $(e.currentTarget).data().href;
    });
    function pagingUtil() {
        var url = window.location.href;
        var start = url.indexOf("page");
        var pageNum = url.substring(start + 5, start + 6);
        var flag = 0;
        $('.pagination button').each(function () {
            var num = $(this).val();
            if ((num == pageNum) || (num == '')) {
                if (num != "1") {
                    $('#button1').removeClass('active')
                }
                $(this).addClass('active');
                flag = flag + 1;
            } else {
                $(this).removeClass('active');
            }
            if (flag == 0) {
                $('#button1').addClass('active')
            }
        });
    }


    $(function () {
        pagingUtil();
        parse();
    });
    function parse() {
        var result = [],
                tmp = [];
        var container = document.getElementById("pagingForm");
        var items = location.search.substr(1).split("&");
        for (var index = 0; index < items.length; index++) {
            tmp = items[index].split("=");
            if (!(tmp[0] == "page" || tmp[0] == "command")) {
                var x = document.createElement("input");
                x.setAttribute("type", "hidden");
                x.setAttribute("name", tmp[0]);
                x.setAttribute("value", tmp[1]);
                container.appendChild(x);
            }
        }
        return result;
    }
</script>