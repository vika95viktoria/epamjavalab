package com.epam.news.exception;

/**
 * Created by Viktoryia_Khlystun on 10/13/2016.
 */
public class NewsConcurrentModificationException extends Exception {
    public NewsConcurrentModificationException() {
    }

    public NewsConcurrentModificationException(String message) {
        super(message);
    }

    public NewsConcurrentModificationException(String message, Throwable cause) {
        super(message, cause);
    }

    public NewsConcurrentModificationException(Throwable cause) {
        super(cause);
    }

    public NewsConcurrentModificationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
