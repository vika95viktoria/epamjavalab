package com.epam.news.dao.impl;

import com.epam.news.dao.AbstractDAO;
import com.epam.news.dao.CommentDAO;
import com.epam.news.domain.Comment;
import com.epam.news.exception.DAOException;
import org.springframework.jdbc.datasource.DataSourceUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.epam.news.util.DAOConstants.*;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
public class CommentDAOImpl extends AbstractDAO implements CommentDAO {

    private static final String SQL_INSERT_COMMENT = "insert into COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (COMMENT_SEQ.nextval,?,?,?)";
    private static final String SQL_DELETE_COMMENT = "delete from COMMENTS where comment_id = ?";
    private static final String SQL_UPDATE_COMMENT = "update COMMENTS set COMMENT_TEXT = ? where COMMENT_ID = ?";
    private static final String SQL_SELECT_COMMENTS_BY_NEWS = "select *  from COMMENTS where news_id = ?";
    private static final String SQL_SELECT_COMMENT_BY_ID = "select * from COMMENTS where COMMENT_ID=?";


    /**
     * Create comment and link it to news
     *
     * @param comment
     * @param newsId
     * @throws DAOException
     */
    public void create(Comment comment, long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_INSERT_COMMENT);
            statement.setLong(1, newsId);
            statement.setString(2, comment.getText());
            statement.setTimestamp(3, new Timestamp(new Date().getTime()));
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Return list of comments to news
     *
     * @param newsId
     * @return
     * @throws DAOException
     */
    public List<Comment> findCommentsByNewsId(long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        List<Comment> comments = new ArrayList<Comment>();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_SELECT_COMMENTS_BY_NEWS);
            statement.setLong(1, newsId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Comment comment = new Comment();
                comment.setId(resultSet.getLong(COMMENT_ID));
                comment.setText(resultSet.getString(COMMENT_TEXT));
                comment.setCreationDate(resultSet.getDate(CREATION_DATE));
                comments.add(comment);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return comments;
    }

    /**
     * Delete comment from database
     *
     * @param comment
     * @throws DAOException
     */

    public void delete(Comment comment) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE_COMMENT);
            statement.setLong(1, comment.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Edit comment text
     *
     * @param comment
     * @throws DAOException
     */
    public void update(Comment comment) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_UPDATE_COMMENT);
            statement.setString(1, comment.getText());
            statement.setLong(2, comment.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Return all comment info by id
     *
     * @param commentId
     * @return
     * @throws DAOException
     */
    public Comment findCommentById(long commentId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        Comment comment = new Comment();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_SELECT_COMMENT_BY_ID);
            statement.setLong(1, commentId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                comment.setId(resultSet.getLong(COMMENT_ID));
                comment.setText(resultSet.getString(COMMENT_TEXT));
                comment.setCreationDate(resultSet.getDate(CREATION_DATE));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return comment;
    }
}
