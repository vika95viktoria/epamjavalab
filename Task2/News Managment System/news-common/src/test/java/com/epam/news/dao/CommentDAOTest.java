package com.epam.news.dao;

import com.epam.news.domain.Comment;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import org.apache.commons.io.IOUtils;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.io.*;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Viktoryia_Khlystun on 6/3/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/testContext.xml"})
@DatabaseSetup(value = "/data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class CommentDAOTest {
    private static CommentDAO commentDAO;
    private static ApplicationContext context;


    @BeforeClass
    public static void setUpAuthorDAO() {
        context = new ClassPathXmlApplicationContext("testContext.xml");
        commentDAO = (CommentDAO) context.getBean("CommentDAO");
    }

    @AfterClass
    public static void closeContext() {
        ((ConfigurableApplicationContext) context).close();
    }

    @Test
    public void findCommentById() throws DAOException {
        Comment comment = commentDAO.findCommentById(1);
        assertEquals(comment.getId(), 1);
        assertEquals(comment.getText(), "Good news!");
    }

    @Test
    public void findCommentByNewsId() throws DAOException {
        List<Comment> comments = commentDAO.findCommentsByNewsId(1);
        assertEquals(comments.size(), 2);
    }
    private IDataSet dbUnitDataSet(InputStream stream) throws Exception {
        try {
            ReplacementDataSet dataSet = new ReplacementDataSet(new FlatXmlDataSet(stream));
            dataSet.addReplacementObject("[null]", null);
            return dataSet;
        } catch (Exception e) {
            throw new Exception();
        }
    }


    @Test
    public void delete() throws Exception {
        Comment comment = commentDAO.findCommentById(1);
        List<Comment> comments = commentDAO.findCommentsByNewsId(1);
        assertEquals(comments.size(), 2);
        commentDAO.delete(comment);
        comments = commentDAO.findCommentsByNewsId(1);
        assertEquals(comments.size(), 1);
    }

    @Test
    public void create() throws DAOException {
        List<Comment> comments = commentDAO.findCommentsByNewsId(1);
        assertEquals(comments.size(), 2);
        Comment comment = new Comment();
        comment.setText("It is really exiting!");
        commentDAO.create(comment, 1);
        comments = commentDAO.findCommentsByNewsId(1);
        for (Comment comment1 : comments) {
            if (comment1.getText().equals(comment.getText())) {
                comment = commentDAO.findCommentById(comment1.getId());
            }
        }
        assertEquals(comments.size(), 3);
        assertEquals(comment.getText(), "It is really exiting!");
    }

    @Test
    public void update() throws DAOException {
        Comment comment = commentDAO.findCommentById(1);
        comment.setText("Exiting");
        commentDAO.update(comment);
        comment = commentDAO.findCommentById(1);
        assertEquals(comment.getText(), "Exiting");

    }
}
