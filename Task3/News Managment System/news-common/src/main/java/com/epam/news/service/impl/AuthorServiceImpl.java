package com.epam.news.service.impl;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.domain.Author;
import com.epam.news.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/31/2016.
 */
@Transactional
@Service
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    private AuthorDAO authorDAO;

    public void setAuthorDAO(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }



    /**
     * create or update author
     * @param author
     */
    public void save(Author author) {
        if (author.getId() != 0) {
            authorDAO.update(author);
        } else {
            authorDAO.create(author);
        }

    }


    /**
     * Delete author
     *
     * @param author
     */
    public void delete(Author author) {
        authorDAO.update(author);
    }


    /**
     * Return author info by it's id
     *
     * @param id
     * @return
     */
    public Author getAuthorById(long id) {
        Author author = authorDAO.findAuthorById(id);
        return author;
    }

    /**
     * get list of all authors in system
     * @return
     */
    public List<Author> findAll() {
        List<Author> authors = authorDAO.findAll();
        return authors;
    }

    /**
     * get list of all authors in system in some limits
     * @param start
     * @return
     */
    public List<Author> findAll(long start) {
        List<Author> authors = authorDAO.findAll(start);
        return authors;
    }

    /**
     * get count of authors in system
     * @return
     */
    public int getCountOfAuthors() {
        return authorDAO.getCountOfAuthors();
    }
}
