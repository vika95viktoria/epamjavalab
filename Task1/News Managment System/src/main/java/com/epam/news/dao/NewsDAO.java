package com.epam.news.dao;

import com.epam.news.domain.News;
import com.epam.news.exception.DAOException;
import com.epam.news.search.SearchCriteria;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
public interface NewsDAO {
    News findNewsById(long id) throws DAOException;

    List<News> getListOfNews() throws DAOException;

    void create(News news) throws DAOException;

    void update(News news) throws DAOException;

    void delete(long id) throws DAOException;

    List<Long> getIdOfMostCommenting() throws DAOException;

    int getCountOfNews() throws DAOException;

    List<News> getListOfNews(SearchCriteria searchCriteria) throws DAOException;
}
