package com.epam.news;

import com.epam.news.domain.Author;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.search.SearchCriteria;
import com.epam.news.service.NewsService;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/26/2016.
 */
public class App {
    private static Logger logger = Logger.getLogger(App.class);

    static {
        PropertyConfigurator.configure("src/main/resources/log4j.properties");
    }

    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("file:src/main/webapp/WEB-INF/beans.xml");
        NewsService newsService = (NewsService) context.getBean("NewsService");
        SearchCriteria searchCriteria = new SearchCriteria();
        List<Tag> tags = new ArrayList<Tag>();
        Tag tag = new Tag();
        Author author = new Author();
        author.setName("van");
        searchCriteria.setAuthor(author);
        tag.setName("sport");
        tags.add(tag);
        searchCriteria.setTags(tags);
        try {
            List<News> news = newsService.searchForNews(searchCriteria);
            for (News news1 : news) {
                System.out.println(news1.getTitle());
            }
            System.out.println(newsService.getCountOfNews());
        } catch (ServiceException e) {
            logger.error(e);
        }
    }
}
