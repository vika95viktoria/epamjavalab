package com.epam.news.domain;

import com.epam.news.model.NewsModel;
import org.apache.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/25/2016.
 */
public class News {
    private static final Logger logger = Logger.getLogger(News.class);
    private long id;
    private String title;
    private String shortText;
    private String fullText;
    private Date creationDate;
    private Date modificationDate;
    private List<Tag> tags;
    private List<Comment> comments;
    private List<Author> authors;
    public News() {
    }

    public News(NewsModel newsModel) {
        this.setId(newsModel.getId());
        this.setFullText(newsModel.getFullText());
        this.setShortText(newsModel.getShortText());
        this.setTitle(newsModel.getTitle());
        Date creationDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            creationDate = formatter.parse(newsModel.getModificationDate());
        } catch (ParseException e) {
            logger.error(e);
        }
        this.setCreationDate(creationDate);
        this.setModificationDate(creationDate);
    }
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }
}
