(function () {
    'use strict';

    var newsApp = angular.module('newsApp');

    newsApp.config(function ($httpProvider, $routeProvider, csrfProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

        $routeProvider
            .when("/login", {
                controller: "LoginController",
                templateUrl: "resources/views/login.html"
            })
            .when("/newsList", {
                controller: "NewsController",
                templateUrl: "resources/views/newsList.html"
            })
            .when("/addNewsPage/:id?", {
                controller: "AddNewsPageController",
                templateUrl: "resources/views/addNews.html"
            })
            .when("/authorPage", {
                controller: "AuthorPageController",
                templateUrl: "resources/views/authorPage.html"
            })
            .when("/tagPage", {
                controller: "TagPageController",
                templateUrl: "resources/views/tagPage.html"
            })
            .when("/newsView/:id/:position?/:searchTags?/:searchAuthor?/:count?", {
                controller: "NewsViewController",
                templateUrl: "resources/views/newsView.html"
            });
        $routeProvider.otherwise({redirectTo: "/newsList"});

        csrfProvider.config({
            url: '/admin/login',
            maxRetries: 3,
            csrfHttpType: 'get',
            csrfTokenHeader: 'X-CSRF-TOKEN',
            httpTypes: ['PUT', 'POST', 'DELETE']
        });
    })

})();
