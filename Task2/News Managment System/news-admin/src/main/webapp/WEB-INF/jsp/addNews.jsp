<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="newsContainer" style="border: dashed black 1px; height: 83%; width: 78%;">
    <div style="display:inline-block">

        <c:if test="${not empty error}">
            <div class="error" style="margin-left: 20px; text-align: center">${error}</div>
        </c:if>

        <div class="labels">
            <div style=" margin: 11px;">
                <label for="title" style="font-weight:bold"><spring:message code="title"/></label>
            </div>
            <br>
            <div style=" margin: 11px; margin-top:24px">
                <label for="datepicker-12" style="font-weight:bold"><spring:message code="date"/></label>
            </div>
            <br>
            <div style=" margin: 11px; margin-top:50px">
                <label for="brief" style="font-weight:bold"><spring:message code="brief"/></label>
            </div>
            <br>
            <div style=" margin: 11px; margin-top:85px">
                <label for="content" style="font-weight:bold"><spring:message code="content"/></label>
            </div>
            <br>
        </div>

        <c:choose>
            <c:when test="${empty edit}">
                <form action="<c:url value="/addNews" />" method="post" style="display: inline-block">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <div class="inputs" >
                        <input id="title" type="text" name="title" style="width:100%" required>
                        <input type="text" id="datepicker-12" name="modificationDate" required>
                        <textarea id="brief" style="width:100%" rows="5" name="shortText" required></textarea>
                        <textarea id="content" style="width:100%" rows="15" name="fullText" required></textarea>
                    </div>
                    <div class="selectorAdmin">
                        <select multiple="multiple" style="display: inline-block;" name="authorIds" required>
                            <c:forEach var="element" items="${authors}">
                                <option value="${element.id}">${element.name}</option>
                            </c:forEach>
                        </select>
                        <select multiple="multiple" style="display: inline-block;" name="tagIds">
                            <c:forEach var="element" items="${tags}" varStatus="tagStatus">
                                <option value="${element.id}">${element.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <p>
                    <div >
                        <button type="submit" class="loginButton" style="float:right; margin-right: -9px ">
                            <spring:message code="save"/></button>
                    </div>
                    </p>
                </form>
            </c:when>
            <c:otherwise>
                <form action="<c:url value="/editNews?var=${var}" />" method="post" style="display: inline-block">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <input type="hidden" name="id" value="${news.id}"/>
                    <div class="inputs" >
                        <input id="title" type="text" name="title" value="${news.title}" style="width:100%"/>
                        <input type="text" id="datepicker-12" value="${news.modificationDate}" name="modificationDate">
                        <textarea id="brief" style="width:100%" rows="5" name="shortText">${news.shortText}</textarea>
                        <textarea id="content" style="width:100%" rows="15" name="fullText">${news.fullText}</textarea>
                    </div>
                    <div class="selectorAdmin">
                        <select multiple="multiple" style="display: inline-block;" name="authorIds">
                            <c:forEach var="element" items="${authors}">
                                <c:choose>
                                    <c:when test="${news.authors.contains(element)}">
                                        <option value="${element.id}" selected>${element.name}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${element.id}">${element.name}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                        <select multiple="multiple" style="display: inline-block;" name="tagIds">
                            <c:forEach var="element" items="${tags}">
                                <c:choose>
                                    <c:when test="${news.tags.contains(element)}">
                                        <option value="${element.id}" selected>${element.name}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${element.id}">${element.name}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                    <p>
                    <div >
                        <button type="submit" class="loginButton" style="float:right; margin-right: -9px ">
                            <spring:message code="save"/></button>
                    </div>
                    </p>
                </form>
            </c:otherwise>
        </c:choose>
    </div>
</div>
