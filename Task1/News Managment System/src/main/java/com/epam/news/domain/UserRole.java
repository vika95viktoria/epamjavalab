package com.epam.news.domain;

/**
 * Created by Viktoryia_Khlystun on 5/25/2016.
 */
enum UserRole {
    USER, ADMIN
}
