package com.epam.news.dao.impl;

import com.epam.news.dao.AbstractDAO;
import com.epam.news.dao.TagDAO;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DAOException;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.news.util.DAOConstants.*;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
@Component
public class TagDAOImpl extends AbstractDAO implements TagDAO {
    private static final String SQL_INSERT_TAG_FOR_NEWS_BY_NEWS_ID = "insert into NEWS_TAG (NEWS_ID,TAG_ID) VALUES (?,?)";
    private static final String SQL_INSERT_TAG_FOR_NEWS = "insert into NEWS_TAG (NEWS_ID,TAG_ID) VALUES ((select news_id from news where title = ?),?)";
    private static final String SQL_CREATE_TAG = "insert into TAG (TAG_ID,TAG_NAME) VALUES (TAG_SEQ.nextval,?)";
    private static final String SQL_SELECT_TAG_BY_ID = "select TAG_ID,TAG_NAME from TAG where TAG_ID=?";
    private static final String SQL_SELECT_TAG_FOR_NEWS_ID = "select TAG_ID,TAG_NAME from tag where tag_id in(select TAG_ID from NEWS_TAG where NEWS_ID = ?)";
    private static final String SQL_DELETE_TAGS_FOR_NEWS = "delete from NEWS_TAG where NEWS_ID = ?";
    private static final String SQL_FIND_ALL_TAGS = "select * from TAG";
    private static final String SQL_DELETE_TAG_FOR_NEWS_BY_TAG_ID = "delete from NEWS_TAG where NEWS_ID = ? and TAG_ID = ?";
    private static final String SQL_GET_COUNT = "select count(*) as count from TAG";
    private static final String SQL_FIND_ALL_TAGS_IN_LIMITS = "SELECT * FROM (SELECT ROWNUM ROW_NUM, SUBQ.* FROM (select TAG_ID,TAG_NAME from TAG) SUBQ WHERE ROWNUM <?)  where ROW_NUM > ?";
    private static final String SQL_DELETE_TAG = "delete from TAG where TAG_ID=?";
    private static final String SQL_UPDATE_TAG = "update TAG set TAG_NAME = ? where TAG_ID = ?";
    /**
     * Adding relation of tag and news into database when news is in process of creation
     *
     * @param news
     * @param tagId
     * @throws DAOException
     */
    public void addTagToNews(News news, long tagId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_INSERT_TAG_FOR_NEWS);
            statement.setString(1, news.getTitle());
            statement.setLong(2, tagId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Adding relation of tag and news into database when news is already created. (Link new tag to existing news)
     *
     * @param newsId
     * @param tagId
     * @throws DAOException
     */
    public void addTagToNews(long newsId, long tagId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_INSERT_TAG_FOR_NEWS_BY_NEWS_ID);
            statement.setLong(1, newsId);
            statement.setLong(2, tagId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Return list of tags for news
     *
     * @param newsId
     * @return
     * @throws DAOException
     */
    public List<Tag> findTagsByNewsId(long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        List<Tag> tags = new ArrayList<Tag>();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_SELECT_TAG_FOR_NEWS_ID);
            statement.setLong(1, newsId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Tag tag = new Tag();
                tag.setId(resultSet.getLong(TAG_ID));
                tag.setName(resultSet.getString(TAG_NAME));
                tags.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return tags;
    }

    /**
     * Return all tag info by id
     *
     * @param tagId
     * @return
     * @throws DAOException
     */
    public Tag findTagById(long tagId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        Tag tag = new Tag();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_SELECT_TAG_BY_ID);
            statement.setLong(1, tagId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                tag.setId(resultSet.getLong(TAG_ID));
                tag.setName(resultSet.getString(TAG_NAME));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return tag;
    }

    /**
     * Create new record in database with all tag info
     *
     * @param tag
     * @throws DAOException
     */
    public void create(Tag tag) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_CREATE_TAG);
            statement.setString(1, tag.getName());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * update the name of tag
     * @param tag
     * @throws DAOException
     */
    public void update(Tag tag) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_UPDATE_TAG);
            statement.setString(1, tag.getName());
            statement.setLong(2, tag.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Delete relation of news and all tags when you delete news
     *
     * @param newsId
     * @throws DAOException
     */
    public void deleteAllTagsFromNews(long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE_TAGS_FOR_NEWS);
            statement.setLong(1, newsId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * delete tag from some news
     * @param newsId
     * @param tagId
     * @throws DAOException
     */
    public void deleteTagFromNews(long newsId,long tagId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE_TAG_FOR_NEWS_BY_TAG_ID);
            statement.setLong(1, newsId);
            statement.setLong(2,tagId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * get list of all tags in system
     * @return
     * @throws DAOException
     */
    public List<Tag> findAll() throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        List<Tag> tags = new ArrayList<Tag>();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_FIND_ALL_TAGS);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Tag tag = new Tag();
                tag.setId(resultSet.getLong(TAG_ID));
                tag.setName(resultSet.getString(TAG_NAME));
                tags.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return tags;
    }

    /**
     * get list of tags in limits
     * @param start
     * @param end
     * @return
     * @throws DAOException
     */
    public List<Tag> findAll(long start, long end) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        List<Tag> tags = new ArrayList<Tag>();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_FIND_ALL_TAGS_IN_LIMITS);
            statement.setLong(1, end);
            statement.setLong(2, start);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Tag tag = new Tag();
                tag.setId(resultSet.getLong(TAG_ID));
                tag.setName(resultSet.getString(TAG_NAME));
                tags.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return tags;
    }


    /**
     * get count of tags in system
     * @return
     * @throws DAOException
     */

    public int getCountOfTags() throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        int count = 0;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_GET_COUNT);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                count = resultSet.getInt(COUNT);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return count;
    }

    /**
     * delete tag from database
     * @param entity
     * @throws DAOException
     */
    public void delete(Tag entity) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE_TAG);
            statement.setLong(1, entity.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }
}
