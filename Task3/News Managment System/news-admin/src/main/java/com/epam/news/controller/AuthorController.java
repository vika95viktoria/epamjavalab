package com.epam.news.controller;

import com.epam.news.domain.Author;
import com.epam.news.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Date;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 7/4/2016.
 */

@Controller
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    /**
     * Deletes author from database and redirects to authorPage with updates
     *
     * @param author
     * @return
     */
    @RequestMapping(value = "/deleteAuthor", method = RequestMethod.POST)
    public String expireAuthor(@ModelAttribute Author author, RedirectAttributes redirectAttributes) {
        author.setExpired(new Date());
        try {
            authorService.delete(author);
        } catch (JpaOptimisticLockingFailureException | HibernateOptimisticLockingFailureException e) {
            redirectAttributes.addFlashAttribute("error", "Author has been edited or deleted. Try again, please");
        }
        return "redirect:/authorPage";
    }

    /**
     * Changes author info in database and redirects to authorPage with updates
     * @param author
     * @return
     */
    @RequestMapping(value = "/updateAuthor", method = RequestMethod.POST)
    public String updateAuthor(@ModelAttribute Author author, RedirectAttributes redirectAttributes) {
        try {
            authorService.save(author);
        } catch (JpaOptimisticLockingFailureException | HibernateOptimisticLockingFailureException e) {
            redirectAttributes.addFlashAttribute("error", "Author has been edited or deleted. Try again, please");
        }
        return "redirect:/authorPage";
    }

    /**
     * Return authorPage with list of authors present in database corresponding to requested page
     * @param page
     * @return
     */
    @RequestMapping(value = "/authorPage", method = RequestMethod.GET)
    public ModelAndView authorPage(@RequestParam(required = false) Integer page) {
        ModelAndView model = new ModelAndView();
        int authorCount = 0;
        if (page != null) {
            page = page - 1;
        } else {
            page = 0;
        }
        List<Author> authorList = authorService.findAll(7 * page);
        authorCount = authorService.getCountOfAuthors();
        int pageCount = authorCount / 7;
        if (pageCount * 7 < authorCount) {
            pageCount++;
        }
        model.addObject("authors", authorList);
        model.addObject("count", pageCount);
        model.setViewName("authorPage");
        return model;
    }

    /**
     * Adds new author to database and redirects to authorPage with updates
     * @param authorName
     * @return
     */
    @RequestMapping(value = "/addAuthor", method = RequestMethod.POST)
    public String addAuthor(@RequestParam String authorName) {
        Author author = new Author();
        author.setName(authorName);
        authorService.save(author);
        return "redirect:/authorPage";
    }
}
