<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div style="border: dashed black 1px;position: relative; width: 98%; height: 84%; margin: 0 auto; top:2%">
    <form name='loginForm'
          action="<c:url value='/login' />" method='POST'>
        <div class="login"  >
            <c:if test="${not empty error}">
                <div class="error">${error}</div>
            </c:if>
            <div class="field">
                <label for="username"><spring:message code="login"/>:</label>
                <input type="text" id="username" name="username" />
            </div>
            <div class="field">
                <label for="password"><spring:message code="password"/>:</label>
                <input type="password" id="password" name="password"/>
            </div>
            <br>
            <button type="submit" class="loginButton"><spring:message code="button.login"/></button>
        </div>
        <input type="hidden"
               name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
</div>