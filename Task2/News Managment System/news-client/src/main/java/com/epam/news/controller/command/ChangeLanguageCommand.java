package com.epam.news.controller.command;

import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Viktoryia_Khlystun on 7/1/2016.
 */
@Component
public class ChangeLanguageCommand extends ActionCommand {
    /**
     * Switch the language
     *
     * @param request
     * @param response
     */
    @Override
    public void action(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String language = request.getParameter("language");
        if ("RU".equals(language)) {
            request.getSession().setAttribute("language", "RU");
        } else {
            request.getSession().setAttribute("language", "US");
        }
    }
}
