(function () {
    'use strict';

    var newsApp = angular.module('newsApp');
    newsApp.controller('AuthorPageController', ['$scope', 'AuthorService', function ($scope, AuthorService) {
        init();
        fetchInfo();

        function init() {
            $scope.authors = [];
            $scope.currentPage = 1;
            $scope.editMode = false;
            $scope.error = null;
        }

        function fetchInfo() {
            getAuthors();
            getCount();
        }

        function getAuthors(page) {
            if (typeof page !== "undefined") {
                $scope.currentPage = page;
            }
            AuthorService.getAuthors(page)
                .then(
                    function (d) {
                        $scope.authors = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Authors');
                    }
                );
        }

        $scope.getInfo = function (page) {
            getAuthors(page);
        }
        function getCount() {
            AuthorService.getCount()
                .then(
                    function (d) {
                        $scope.count = Math.ceil(d / 7);
                    },
                    function (errResponse) {
                        console.error('Error while fetching count of Authors');
                    }
                );
        }

        $scope.turnToEditMode = function () {
            $scope.editMode = true;
        }
        $scope.turnOffEditMode = function () {
            $scope.editMode = false;
        }

        $scope.createAuthor = function () {
            AuthorService.createAuthor($scope.authorName)
                .then(
                    function (d) {
                        $scope.authorName = '';
                        getCount();
                    },
                    function (errResponse) {
                        console.error('Error while creating Author');
                    }
                );
        }
        $scope.updateAuthor = function (author, name) {
            if (name != null && name != '') {
                author.name = name;
                AuthorService.updateAuthor(author)
                    .then(
                        function (d) {
                            angular.forEach($scope.authors, function (value) {
                                if (value.id === author.id) {
                                    value.name = author.name;
                                    value.version = value.version + 1;
                                }
                            });
                            $scope.editMode = false;
                            $scope.error = null;
                        },
                        function (errResponse) {
                            console.error('Error while editing Author');
                            $scope.error = errResponse.data[0];
                        }
                    );
            } else {
                $scope.error = "Field can't be empty"
            }
        }

        $scope.deleteAuthor = function (author) {
            AuthorService.deleteAuthor(author)
                .then(
                    function (d) {
                        var index = $scope.authors.indexOf(author);
                        $scope.authors.splice(index, index + 1);
                        $scope.editMode = false;
                        $scope.error = null;
                        getCount();
                    },
                    function (errResponse) {
                        console.error('Error while deleting Author');
                        $scope.error = errResponse.data[0];
                    }
                );
        }


    }]);
})();
