package com.epam.news.controller;

import com.epam.news.domain.Author;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 7/4/2016.
 */

@Controller
public class AuthorController {

    private static final Logger logger = Logger.getLogger(AuthorController.class);
    @Autowired
    private AuthorService authorService;

    /**
     * Deletes author from database and redirects to authorPage with updates
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/deleteAuthor", method = RequestMethod.POST)
    public String expireAuthor(@RequestParam Long id) {
        Author author = new Author();
        author.setId(id);
        try {
            authorService.delete(author);
        } catch (ServiceException e) {
            logger.error(e);
        }
        return "redirect:/authorPage";
    }

    /**
     * Changes author info in database and redirects to authorPage with updates
     * @param id
     * @param name
     * @return
     */
    @RequestMapping(value = "/updateAuthor", method = RequestMethod.POST)
    public String updateAuthor(@RequestParam Long id, @RequestParam String name) {
        Author author = new Author();
        author.setId(id);
        author.setName(name);
        try {
            authorService.save(author);
        } catch (ServiceException e) {
            logger.error(e);
        }
        return "redirect:/authorPage";
    }

    /**
     * Return authorPage with list of authors present in database corresponding to requested page
     * @param page
     * @return
     */
    @RequestMapping(value = "/authorPage", method = RequestMethod.GET)
    public ModelAndView authorPage(@RequestParam(required = false) Integer page) {
        ModelAndView model = new ModelAndView();
        int authorCount = 0;
        if (page != null) {
            page = page - 1;
        } else {
            page = 0;
        }
        List<Author> authorList = new ArrayList<>();
        try {

            authorList = authorService.findAll(8 * page, 8 * page + 9);
            authorCount = authorService.getCountOfAuthors();
        } catch (ServiceException e) {
            logger.error(e);
        }
        int pageCount = authorCount / 8;
        if (pageCount * 8 < authorCount) {
            pageCount++;
        }
        model.addObject("authors", authorList);
        model.addObject("count", pageCount);
        model.setViewName("authorPage");
        return model;
    }

    /**
     * Adds new author to database and redirects to authorPage with updates
     * @param authorName
     * @return
     */
    @RequestMapping(value = "/addAuthor", method = RequestMethod.POST)
    public String addAuthor(@RequestParam String authorName) {
        Author author = new Author();
        author.setName(authorName);
        try {
            authorService.save(author);
        } catch (ServiceException e) {
            logger.error(e);
        }
        return "redirect:/authorPage";
    }
}
