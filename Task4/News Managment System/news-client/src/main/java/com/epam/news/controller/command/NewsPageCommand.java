package com.epam.news.controller.command;

import com.epam.news.domain.News;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Viktoryia_Khlystun on 6/23/2016.
 */

@Component
public class NewsPageCommand extends ActionCommand {

    private static final Logger logger = Logger.getLogger(NewsPageCommand.class);

    /**
     * Return the news page with links on previous and next news
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void action(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long id = Long.parseLong(request.getParameter("id"));
        String var = request.getParameter("var");
        News news = new News();
        news = newsService.findNewsById(id);
        request.setAttribute("var", var);
        request.setAttribute("news", news);
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/newsViewPage.jsp");
        dispatcher.forward(request, response);
    }
}
