package com.epam.news.exception;

/**
 * Created by Viktoryia_Khlystun on 10/13/2016.
 */
public class DeletedNewsModificationException extends Exception {
    public DeletedNewsModificationException(String message, Throwable cause) {
        super(message, cause);
    }

    public DeletedNewsModificationException(String message) {
        super(message);
    }

    public DeletedNewsModificationException() {
    }

    public DeletedNewsModificationException(Throwable cause) {
        super(cause);
    }

    public DeletedNewsModificationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
