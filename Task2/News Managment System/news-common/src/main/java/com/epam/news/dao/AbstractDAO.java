package com.epam.news.dao;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
public abstract class AbstractDAO {
    private static Logger logger = Logger.getLogger(AbstractDAO.class);

    @Autowired
    protected DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * close statement without throwing the exception
     * @param statement
     */
    public void close(Statement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            logger.error(e);
        }
    }
}
