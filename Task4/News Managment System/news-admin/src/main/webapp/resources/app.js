(function () {
    'use strict';

    var newsApp = angular.module('newsApp', ['isteven-multi-select', 'ngRoute', 'spring-security-csrf-token-interceptor', 'ngMaterial', 'ngMessages']);

    newsApp.run(['$rootScope', '$location', 'Auth', function ($rootScope, $location, Auth) {
        $rootScope.$on('$routeChangeStart', function (event) {

            if (!Auth.isLoggedIn()) {
                console.log('DENY : Redirecting to Login');
                $location.path('/login');
            }
            else {
                console.log('ALLOW');
            }
        });
    }])
})();