package com.epam.news.model;

import com.epam.news.domain.Author;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;

import java.util.Date;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 10/5/2016.
 */
public class NewsModelList {
    private long id;
    private String title;
    private String shortText;
    private Date modificationDate;

    private List<Tag> tags;
    private List<Author> authors;

    private int commentsCount;

    public NewsModelList() {
    }

    public NewsModelList(News news, int count) {
        this.id = news.getId();
        this.title = news.getTitle();
        this.shortText = news.getShortText();
        this.modificationDate = news.getModificationDate();
        this.tags = news.getTags();
        this.authors = news.getAuthors();
        this.commentsCount = count;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }


    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }
}
