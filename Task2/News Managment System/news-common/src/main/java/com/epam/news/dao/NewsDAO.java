package com.epam.news.dao;

import com.epam.news.domain.News;
import com.epam.news.exception.DAOException;
import com.epam.news.search.SearchCriteria;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
public interface NewsDAO {
    News findNewsById(long id) throws DAOException;

    List<News> getListOfNews() throws DAOException;

    void create(News news) throws DAOException;

    void update(News news) throws DAOException;

    void delete(long id) throws DAOException;

    List<Long> getIdOfMostCommenting(long start, long end) throws DAOException;

    int getCountOfNews() throws DAOException;

    List<Long> getListOfNews(SearchCriteria searchCriteria) throws DAOException;

    List<News> getSubListOfNews(List<Long> ids) throws DAOException;

    List<Long> getIdOfMostCommenting(long start, long end,List<Long> ids) throws DAOException;

    int getCountOfListNews(SearchCriteria searchCriteria) throws DAOException;

    News findNewsByTitle(String title) throws DAOException;
}
