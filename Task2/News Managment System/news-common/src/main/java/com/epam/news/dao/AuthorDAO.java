package com.epam.news.dao;

import com.epam.news.domain.Author;
import com.epam.news.domain.News;
import com.epam.news.exception.DAOException;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
public interface AuthorDAO {
    void addAuthorToNews(News news, long authorId) throws DAOException;

    List<Author> findAuthorsByNewsId(long newsId) throws DAOException;

    void create(Author author) throws DAOException;

    Author findAuthorById(long authorId) throws DAOException;

    void delete(Author entity) throws DAOException;

    void update(Author entity) throws DAOException;

    void deleteAllAuthorsFromNews(long newsId) throws DAOException;

    void addAuthorToNews(long newsId, long authorId) throws DAOException;

    List<Author> findAll() throws DAOException;

    void deleteAuthorFromNews(long newsId,long authorId) throws DAOException;

    List<Author> findAll(long start,long end) throws DAOException;

    int getCountOfAuthors() throws DAOException;
}
