package com.epam.news.util;

import com.epam.news.model.NewsModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 7/14/2016.
 */
public class NewsCreateValidator {
    public List<String> validate(NewsModel newsModel) {
        List<String> errors = new ArrayList<>();
        if (newsModel.getId() < 0) {
            errors.add("Incorrect id value");
        }
        if (newsModel.getTitle() == null || "".equals(newsModel.getTitle())) {
            errors.add("Title is a mandatory field");
        }
        if (newsModel.getShortText() == null || "".equals(newsModel.getShortText())) {
            errors.add("Brief is a mandatory field");
        }
        if (newsModel.getFullText() == null || "".equals(newsModel.getFullText())) {
            errors.add("Content is a mandatory field");
        }
        if ("".equals(newsModel.getModificationDate())) {
            errors.add("Date is mandatory field");
        }
        if (newsModel.getAuthors().isEmpty()) {
            errors.add("News should have an author");
        }
        return errors;

    }
}
