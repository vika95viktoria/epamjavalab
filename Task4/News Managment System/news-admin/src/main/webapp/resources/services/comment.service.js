(function () {
    'use strict';

    var newsApp = angular.module('newsApp');

    newsApp.factory('CommentService', ['$http', '$q', function ($http, $q) {


        return {
            addComment: addComment,
            deleteComment: deleteComment
        };


        function addComment(commentText, newsId) {
            var deferred = $q.defer();
            $http.post('/admin/addComment', $.param({
                commentText: commentText,
                newsId: newsId
            }), {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }

        function deleteComment(id) {
            var deferred = $q.defer();

            $http.post('/admin/deleteComment', $.param({
                id: id
            }), {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }


    }]);
})();
