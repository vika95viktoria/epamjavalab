<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<fmt:setBundle basename="pagecontent"/>
<html>
<head>

    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>">

    <meta charset="UTF-8">


    <title>400 Error</title>
</head>
<body>
<img src="https://raw.githubusercontent.com/vika95viktoria/EpamCourses/master/LowcostAvia/src/main/webapp/pictures/laptop.png"  style="position: absolute; top:30%; left: 20%; height: 600px">
<div >
<span id="sorry500">
<br>
<br>
    <fmt:message key="error400.first"/>
<br>
<fmt:message key="error400.second"/>
</span>
        <br>
    <span id="errorMessage">
        ${errorMessage}
    </span>

    </div>
</div>


</body>
</html>
