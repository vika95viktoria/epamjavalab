package com.epam.news.service.impl;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.dao.CommentDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.dao.TagDAO;
import com.epam.news.domain.Author;
import com.epam.news.domain.Comment;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.model.NewsModel;
import com.epam.news.search.SearchCriteria;
import com.epam.news.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
@Transactional
@Component
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsDAO newsDAO;
    @Autowired
    private TagDAO tagDAO;
    @Autowired
    private AuthorDAO authorDAO;
    @Autowired
    private CommentDAO commentDAO;

    public void setNewsDAO(NewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }

    public void setTagDAO(TagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }

    public void setAuthorDAO(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    public void setCommentDAO(CommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

    /**
     * return list of news by ids
     * @param ids
     * @return
     */

    public List<News> getNewsByIds(List<Long> ids) {
        List<News> news = newsDAO.getSubListOfNews(ids);
        final List<Long> idOfMostCommenting = ids;
        Collections.sort(news, new Comparator<News>() {
            @Override
            public int compare(News o1, News o2) {
                return new Integer(idOfMostCommenting.indexOf(o1.getId())).compareTo(new Integer(idOfMostCommenting.indexOf(o2.getId())));
            }
        });
        for (News news1 : news) {
            List<Comment> comments = commentDAO.findCommentsByNewsId(news1.getId());
            news1.setComments(comments);
        }
        return news;
    }

    /**
     * Return ordered list of news with all info
     *
     * @return
     */
    public List<News> getNewsOrderedByComments(long start) {
        List<Long> newsIds = newsDAO.getIdOfMostCommenting(start);
        List<News> news = getNewsByIds(newsIds);
        return news;
    }


    /**
     * Create news with tags and authors
     *
     * @param newsModel
     */
    public void createNews(NewsModel newsModel) {
        List<Tag> tagList = new ArrayList<>();
        List<Author> authorList = new ArrayList<>();
        for (Long tag : newsModel.getTagIds()) {
            tagList.add(tagDAO.findTagById(tag));
        }
        for (Long author : newsModel.getAuthorIds()) {
            authorList.add(authorDAO.findAuthorById(author));
        }
        News news = new News(newsModel);
        news.setTags(tagList);
        news.setAuthors(authorList);
        newsDAO.create(news);
    }

    /**
     * Delete news and related records in database
     *
     * @param newsId
     */
    public void deleteNews(Long newsId) {
        newsDAO.delete(findNewsById(newsId));
    }

    /**
     * Return complete object of news with all related objects like tags, authors and comments
     *
     * @param newsId
     * @return
     */
    public News findNewsById(Long newsId) {
        News news = newsDAO.findNewsById(newsId);
        List<Comment> comments = commentDAO.findCommentsByNewsId(newsId);
        news.setComments(comments);
        return news;
    }

    /**
     * Return count of news
     *
     * @return
     */
    public int getCountOfNews() {
        return newsDAO.getCountOfNews();
    }

    /**
     * Return count of news according to search criteria
     * @param searchCriteria
     * @return
     * @throws ServiceException
     */
    public int getCountOfNews(SearchCriteria searchCriteria) {
        return newsDAO.getCountOfListNews(searchCriteria);
    }

    /**
     * Edit news info
     *
     * @param newsModel
     */
    public void updateNews(NewsModel newsModel) {
        List<Author> authorList = new ArrayList<>();
        List<Tag> tagList = new ArrayList<>();
        for (Long tag : newsModel.getTagIds()) {
            tagList.add(tagDAO.findTagById(tag));
        }
        for (Long author : newsModel.getAuthorIds()) {
            authorList.add(authorDAO.findAuthorById(author));
        }
        News news = new News(newsModel);
        news.setTags(tagList);
        news.setAuthors(authorList);
        newsDAO.update(news);
    }

    /**
     * return list of news fitted to serach parameters
     *
     * @param searchCriteria
     * @return
     * @throws ServiceException
     */
    public List<News> searchForNews(SearchCriteria searchCriteria, long start, long end) {
        List<Long> ids = newsDAO.getListOfNews(searchCriteria);
        List<News> news = new ArrayList<>();
        if (ids.size() > 0) {
            List<Long> idOfMostCommenting = newsDAO.getIdOfMostCommenting(start, end, ids);
            news = getNewsByIds(idOfMostCommenting);
        }
        return news;
    }

    /**
     * find news by it's title
     * @param title
     * @return
     */
    public News findNewsByTitle(String title) {
        News news = newsDAO.findNewsByTitle(title);
        List<Comment> comments = commentDAO.findCommentsByNewsId(news.getId());
        news.setComments(comments);
        return news;
    }
}
