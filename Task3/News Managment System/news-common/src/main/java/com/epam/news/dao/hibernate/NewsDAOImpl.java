package com.epam.news.dao.hibernate;

import com.epam.news.dao.AbstractDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.search.SearchCriteria;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/26/2016.
 */

@Repository
@Profile("hibernate")
public class NewsDAOImpl extends AbstractDAO implements NewsDAO {

    /**
     * Return news info without comments, tags and authors. Only news description
     *
     * @param id
     * @return
     */

    public News findNewsById(long id) {
        Session session = sessionFactory.getCurrentSession();
        News news = session.load(News.class, id);
        return news;
    }

    /**
     * Find news by title
     * @param title
     * @return
     */
    public News findNewsByTitle(String title) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select n from News as n where n.title = :newsTitle");
        query.setParameter("newsTitle", title);
        News news = (News) query.uniqueResult();
        return news;
    }



    /**
     * Get list of news, which id is present in param ids
     * @param ids
     * @return
     */
    public List<News> getSubListOfNews(List<Long> ids) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from News as n where n.id in (:newsIds)");
        query.setParameterList("newsIds", ids);
        List<News> news = query.list();
        return news;
    }

    /**
     * Create a record in database with news info, only news description? not including tags, authors and comments
     *
     * @param news
     */
    public void create(News news) {
        Session session = sessionFactory.getCurrentSession();
        session.save(news);
    }

    /**
     * Update news text, title and change modification date
     *
     * @param news
     */
    public void update(News news) {
        Session session = sessionFactory.getCurrentSession();
        news.setVersion(news.getVersion() + 1);
        session.update(news);
    }

    /**
     * Delete news by id from table with comments deleting on cascade
     *
     * @param news
     */
    public void delete(News news) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(news);
    }

    /**
     * Return sorted list of newsIds desc from most commenting
     *
     * @return
     */
    public List<Long> getIdOfMostCommenting(long start) {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(News.class, "n");
        criteria.createAlias("n.comments", "comment", Criteria.LEFT_JOIN);
        criteria.setProjection(Projections.projectionList().add(Projections.groupProperty("n.id"))
                .add(Projections.groupProperty("n.modificationDate"))
                .add(Projections.count("comment.id").as("numberOfComments")));
        criteria.addOrder(Order.desc("numberOfComments"));
        criteria.addOrder(Order.desc("n.modificationDate"));
        criteria.setFirstResult((int) start);
        criteria.setMaxResults(4);
        List<Object[]> list = criteria.list();
        List<Long> newsList = new ArrayList<>();
        for (Object[] o : list) {
            newsList.add((Long) o[0]);
        }
        return newsList;
    }

    /**
     * Get list of most commenting ids present in param ids in some limits
     * @param start
     * @param end
     * @param ids
     * @return
     */
    public List<Long> getIdOfMostCommenting(long start, long end, List<Long> ids) {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(News.class, "n");
        criteria.createAlias("n.comments", "comment", Criteria.LEFT_JOIN);
        criteria.setProjection(Projections.projectionList().add(Projections.groupProperty("n.id"))
                .add(Projections.groupProperty("n.modificationDate"))
                .add(Projections.count("comment.id").as("numberOfComments")));
        criteria.add(Restrictions.in("n.id", ids));
        criteria.addOrder(Order.desc("numberOfComments"));
        criteria.addOrder(Order.desc("n.modificationDate"));
        criteria.setFirstResult((int) start);
        criteria.setMaxResults(4);
        List<Object[]> list = criteria.list();
        List<Long> newsList = new ArrayList<>();
        for (Object[] o : list) {
            newsList.add((Long) o[0]);
        }
        return newsList;
    }

    /**
     * Return the count of records in database in table news
     *
     * @return
     */
    public int getCountOfNews() {
        Session session = sessionFactory.getCurrentSession();
        Criteria crit = session.createCriteria(News.class);
        crit.setProjection(Projections.rowCount());
        int count = ((Long) crit.uniqueResult()).intValue();
        return count;
    }

    /**
     * Return list of news fitted some search parameters like author name or tags
     *
     * @param searchCriteria
     * @return
     */
    public List<Long> getListOfNews(SearchCriteria searchCriteria) {
        List<News> result = buildQuery(searchCriteria).list();
        List<Long> newsIds = new ArrayList<>();
        for (News news : result) {
            newsIds.add(news.getId());
        }
        return newsIds;
    }

    /**
     * return count of news according to search criteria
     * @param searchCriteria
     * @return
     */
    public int getCountOfListNews(SearchCriteria searchCriteria) {
        int count = buildQuery(searchCriteria).getResultSize();
        return count;
    }

    private org.hibernate.search.FullTextQuery buildQuery(SearchCriteria searchCriteria) {
        Session session = sessionFactory.getCurrentSession();
        FullTextSession fullTextSession = Search.getFullTextSession(session);
        QueryBuilder qb = fullTextSession.getSearchFactory().buildQueryBuilder().forEntity(News.class).get();
        StringBuilder tagUtil = new StringBuilder();
        org.apache.lucene.search.Query searchQuery = qb.all().createQuery();
        if (searchCriteria.getAuthor() != null) {
            if (searchCriteria.getTags() != null) {
                for (Tag tag : searchCriteria.getTags()) {
                    tagUtil.append(tag.getName() + " ");
                }
                searchQuery = qb.bool()
                        .must(qb.keyword().onField("authors.name").matching(searchCriteria.getAuthor().getName()).createQuery())
                        .must(qb.keyword().onField("tags.name").matching(tagUtil.toString()).createQuery())
                        .createQuery();
            } else {
                searchQuery = qb.keyword().onField("authors.name").matching(searchCriteria.getAuthor().getName()).createQuery();
            }
        } else if (searchCriteria.getTags() != null) {
            for (Tag tag : searchCriteria.getTags()) {
                tagUtil.append(tag.getName() + " ");
            }
            searchQuery = qb.keyword().onField("tags.name").matching(tagUtil.toString()).createQuery();
        }
        org.hibernate.search.FullTextQuery query = fullTextSession.createFullTextQuery(searchQuery, News.class);
        return query;
    }

}
