function changeToRus() {
    var s = "news?command=changeLanguage&language=RU";
    $.ajax({
        type: "POST",
        url: s,
        processData: false,
        success: function (data) {
            location.reload();
        }
    });
}
function changeToEng() {
    var s = "news?command=changeLanguage&language=EN";
    $.ajax({
        type: "POST",
        url: s,
        processData: false,
        success: function (data) {
            location.reload();
        }
    });
}
