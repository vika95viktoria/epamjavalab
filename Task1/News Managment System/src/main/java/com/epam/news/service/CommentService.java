package com.epam.news.service;

import com.epam.news.dao.CommentDAO;
import com.epam.news.domain.Comment;
import com.epam.news.exception.ServiceException;

/**
 * Created by Viktoryia_Khlystun on 5/31/2016.
 */
public interface CommentService {
    void addComment(long newsId, Comment comment) throws ServiceException;

    void deleteComment(Comment comment) throws ServiceException;

    Comment findCommentById(long id) throws ServiceException;

    void update(Comment comment) throws ServiceException;

    void setCommentDAO(CommentDAO commentDAO);
}
