package com.epam.news.dao;

import com.epam.news.domain.Comment;
import com.epam.news.exception.DAOException;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
public interface CommentDAO {
    void create(Comment comment, long newsId) throws DAOException;

    List<Comment> findCommentsByNewsId(long newsId) throws DAOException;

    void delete(Comment comment) throws DAOException;

    void update(Comment comment) throws DAOException;

    Comment findCommentById(long commentId) throws DAOException;
}
