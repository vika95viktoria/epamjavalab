package com.epam.news.service;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.dao.CommentDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.dao.TagDAO;
import com.epam.news.domain.News;
import com.epam.news.model.NewsModel;
import com.epam.news.model.NewsModelList;
import com.epam.news.search.SearchCriteria;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
public interface NewsService {
    List<NewsModelList> getNewsOrderedByComments(int start);

    long createNews(NewsModel newsModel);

    void deleteNews(Long[] newsIds);

    News findNewsById(Long newsId);

    int getCountOfNews();

    long updateNews(NewsModel newsModel);

    List<NewsModelList> searchForNews(SearchCriteria searchCriteria, int start, int end);

    News getNewsFromChain(SearchCriteria searchCriteria, int position, int index);

    News getNewsFromChain(int position, int index);

    void setNewsDAO(NewsDAO newsDAO);

    void setTagDAO(TagDAO tagDAO);

    void setAuthorDAO(AuthorDAO authorDAO);

    void setCommentDAO(CommentDAO commentDAO);

    int getCountOfNews(SearchCriteria searchCriteria);

    News findNewsByTitle(String title);
}
