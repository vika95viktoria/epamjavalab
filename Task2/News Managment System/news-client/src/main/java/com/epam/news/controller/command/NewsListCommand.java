package com.epam.news.controller.command;

import com.epam.news.domain.Author;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.search.SearchCriteria;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Viktoryia_Khlystun on 6/23/2016.
 */

@Component
public class NewsListCommand extends ActionCommand {


    /**
     * Return list of news according to parameters such as page or search criteria
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void action(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {
        List<String> tagNames = new ArrayList<>();
        Map<String, String[]> parameters = request.getParameterMap();
        for (String parameter : parameters.keySet()) {
            if (parameter.toLowerCase().startsWith("tag")) {
                String[] values = parameters.get(parameter);
                for (String value : values) {
                    tagNames.add(value);
                }
            }
        }
        String authorName = request.getParameter("author");
        SearchCriteria searchCriteria = null;
        if (!tagNames.isEmpty() || authorName != null) {
            searchCriteria = new SearchCriteria();
            if (authorName != null && !"".equals(authorName)) {
                Author author = new Author();
                author.setName(authorName);
                searchCriteria.setAuthor(author);
            }
            if (!tagNames.isEmpty()) {
                List<Tag> tags = new ArrayList<>();
                for (String tagName : tagNames) {
                    Tag tag = new Tag();
                    tag.setName(tagName);
                    tags.add(tag);
                }
                searchCriteria.setTags(tags);
            }
        }
        List<News> newsList = new ArrayList<>();
        List<Tag> tagList = new ArrayList<>();
        List<Author> authorList = new ArrayList<>();
        int page = 0;
        int newsCount = 0;
        String parameter = request.getParameter("page");
        if (parameter != null) {
            page = Integer.parseInt(parameter) - 1;
        }
        if (searchCriteria != null) {
            newsList = newsService.searchForNews(searchCriteria, 4 * page, 4 * page + 5);
            newsCount = newsService.getCountOfNews(searchCriteria);
        } else {
            newsList = newsService.getNewsOrderedByComments(4 * page, 4 * page + 5);
            newsCount = newsService.getCountOfNews();
        }
        tagList = tagService.findAll();
        authorList = authorService.findAll();
        int pageCount = newsCount / 4;
        if (newsCount % 4 != 0) {
            pageCount++;
        }
        HttpSession session = request.getSession();
        session.setAttribute("news", newsList);
        request.setAttribute("authors", authorList);
        request.setAttribute("tags", tagList);
        request.setAttribute("count", pageCount);
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/newsList.jsp");
        dispatcher.forward(request, response);
    }
}
