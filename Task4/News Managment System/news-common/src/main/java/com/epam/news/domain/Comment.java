package com.epam.news.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Viktoryia_Khlystun on 5/25/2016.
 */

@Entity
@Table(name = "COMMENTS")
public class Comment {

    @Id
    @GeneratedValue(generator = "CommentGen")
    @SequenceGenerator(name = "CommentGen", sequenceName = "COMMENT_SEQ")
    @Column(name = "COMMENT_ID")
    private long id;

    @Column(name = "COMMENT_TEXT")
    private String text;

    @Column(name = "CREATION_DATE")
    @Temporal(TemporalType.DATE)
    private Date creationDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NEWS_ID")
    private News news;


    public Comment() {
    }

    public Comment(long id) {
        this.id = id;
    }

    public Comment(String text) {
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @JsonIgnore
    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

}
