package com.epam.news.service.impl;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.domain.Author;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/31/2016.
 */
@Service
public class AuthorServiceImpl implements AuthorService {
    private static final Logger logger = Logger.getLogger(AuthorServiceImpl.class);
    @Autowired
    private AuthorDAO authorDAO;

    public void setAuthorDAO(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    /**
     * Add author to news
     *
     * @param authorId
     * @param newsId
     * @throws ServiceException
     */

    public void addAuthorToNews(long authorId, long newsId) throws ServiceException {
        try {
            authorDAO.addAuthorToNews(newsId, authorId);
        } catch (DAOException e) {
            logger.error("Failed to link author to news " + e);
            throw new ServiceException(e);
        }
    }

    /**
     * create or update author
     * @param author
     * @throws ServiceException
     */
    public void save(Author author) throws ServiceException {
        try {
            if (author.getId() != 0) {
                authorDAO.update(author);
            } else {
                authorDAO.create(author);
            }

        } catch (DAOException e) {
            logger.error("Failed to save author " + e);
            throw new ServiceException(e);
        }
    }


    /**
     * Delete author
     *
     * @param author
     * @throws ServiceException
     */
    public void delete(Author author) throws ServiceException {
        try {
            authorDAO.delete(author);
        } catch (DAOException e) {
            logger.error("Failed to delete author " + e);
            throw new ServiceException(e);
        }
    }


    /**
     * Return author info by it's id
     *
     * @param id
     * @return
     * @throws ServiceException
     */
    public Author getAuthorById(long id) throws ServiceException {
        Author author;
        try {
            author = authorDAO.findAuthorById(id);
        } catch (DAOException e) {
            logger.error("Failed to find  author by id " + id + " " + e);
            throw new ServiceException(e);
        }
        return author;
    }

    /**
     * get list of all authors in system
     * @return
     * @throws ServiceException
     */
    public List<Author> findAll() throws ServiceException {
        List<Author> authors;
        try {
            authors = authorDAO.findAll();
        } catch (DAOException e) {
            logger.error("Failed to get list of authors " + e);
            throw new ServiceException(e);
        }
        return authors;
    }

    /**
     * get list of all authors in system in some limits
     * @param start
     * @param end
     * @return
     * @throws ServiceException
     */
    public List<Author> findAll(long start,long end) throws ServiceException {
        List<Author> authors;
        try {
            authors = authorDAO.findAll(start,end);
        } catch (DAOException e) {
            logger.error("Failed to get list of authors " + e);
            throw new ServiceException(e);
        }
        return authors;
    }

    /**
     * get count of authors in system
     * @return
     * @throws ServiceException
     */
    public int getCountOfAuthors() throws ServiceException {
        try {
            return authorDAO.getCountOfAuthors();
        } catch (DAOException e) {
            logger.error("Failed to get count of authors " + e);
            throw new ServiceException(e);
        }
    }
}
