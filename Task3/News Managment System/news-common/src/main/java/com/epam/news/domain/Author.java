package com.epam.news.domain;

import org.hibernate.search.annotations.Field;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Viktoryia_Khlystun on 5/25/2016.
 */
@Entity
@Table(name = "AUTHOR")
public class Author {

    @Id
    @GeneratedValue(generator = "AuthorGen")
    @SequenceGenerator(name = "AuthorGen", sequenceName = "AUTHOR_SEQ")
    @Column(name = "AUTHOR_ID")
    private long id;

    @Field
    @Column(name = "AUTHOR_NAME")
    private String name;

    @Temporal(TemporalType.DATE)
    private Date expired;

    @Version
    private int version;

    public Author() {
    }

    public Author(long id, String name, Date expired) {
        this.id = id;
        this.name = name;
        this.expired = expired;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getExpired() {
        return expired;
    }

    public void setExpired(Date expired) {
        this.expired = expired;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;

        if (id != author.id) return false;
        return true;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + name.hashCode();
        result = 31 * result + (expired != null ? expired.hashCode() : 0);
        return result;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
