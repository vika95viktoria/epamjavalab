package com.epam.news.controller;

import com.epam.news.domain.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.TagService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 7/4/2016.
 */

@Controller
public class TagController {

    private static final Logger logger = Logger.getLogger(TagController.class);
    @Autowired
    private TagService tagService;

    /**
     * returns tag page with list of tags corresponding to the page
     *
     * @param page
     * @return
     */

    @RequestMapping(value = "/tagPage", method = RequestMethod.GET)
    public ModelAndView tagPage(@RequestParam(required = false) Integer page) {
        ModelAndView model = new ModelAndView();
        int tagCount = 0;
        if (page != null) {
            page = page - 1;
        } else {
            page = 0;
        }
        List<Tag> tagList = new ArrayList<>();
        try {
            tagList = tagService.findAll(7 * page, 7 * page + 8);
            tagCount = tagService.getCountOfTags();
        } catch (ServiceException e) {
            logger.error(e);
        }
        int pageCount = tagCount / 7;
        if (pageCount * 7 < tagCount) {
            pageCount++;
        }
        model.addObject("tags", tagList);
        model.addObject("count", pageCount);
        model.setViewName("tagPage");
        return model;
    }

    /**
     * deletes tag from database and redirect to the tagPage
     * @param id
     * @return
     */
    @RequestMapping(value = "/deleteTag", method = RequestMethod.POST)
    public String deleteTag(@RequestParam Long id) {
        Tag tag = new Tag();
        tag.setId(id);
        try {
            tagService.delete(tag);
        } catch (ServiceException e) {
            logger.error(e);
        }
        return "redirect:/tagPage";
    }

    /**
     * Updates the value of tag and redirect to the tagPage
     * @param id
     * @param name
     * @return
     */
    @RequestMapping(value = "/updateTag", method = RequestMethod.POST)
    public String updateTag(@RequestParam Long id, @RequestParam String name) {
        Tag tag = new Tag();
        tag.setId(id);
        tag.setName(name);
        try {
            tagService.save(tag);
        } catch (ServiceException e) {
            logger.error(e);
        }
        return "redirect:/tagPage";
    }

    /**
     * Adds new tag to the database and redirect to the tagPage
     * @param tagName
     * @return
     */
    @RequestMapping(value = "/addTag", method = RequestMethod.POST)
    public String addAuthor(@RequestParam String tagName) {
        Tag tag = new Tag();
        tag.setName(tagName);
        try {
            tagService.save(tag);
        } catch (ServiceException e) {
            logger.error(e);
        }
        return "redirect:/tagPage";
    }
}
