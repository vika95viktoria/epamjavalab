<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<fmt:setBundle basename="pagecontent"/>
<div style="padding: 10px; border: dashed black 1px; height: 85%; width: 77%;" class="newsContainer" >
    <div style="width:98%; margin: 0 auto; padding-top: 20px">
        <span class="newsTitle">${news.title}</span>
        <span>(by </span>
        <c:forEach var="elem" items="${news.authors}" varStatus="loop">
            ${elem.name}
            <c:if test="${fn:length(news.authors) != loop.index+1}">
                <span>, </span>
            </c:if>
        </c:forEach>
        <span>)</span>
        <a href="" class="newsDate">${news.creationDate}</a>
        <br>
        <br>
        <br>
<span> ${news.fullText}
</span>
        <div class="clearfix">
            <div style="width:80%; float: left" >
<c:forEach var="elem" items="${news.comments}" varStatus="loop">
    <br>
                <a href="" style="float:left; color: black">${elem.creationDate}</a>
                <br>
                <div class="comment">
                    <form action="<c:url value="/deleteComment?id=${elem.id}&newsId=${news.id}&var=${var}" />" method="post">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <input type="image" src="https://cdn0.iconfinder.com/data/icons/navigation-set-arrows-part-one/32/CloseCancelSquare-128.png" name="saveForm" class="btTxt submit" id="saveForm" style="float: right; width: 16px; height: 15px; display: block"/>
                    <span >${elem.text}</span>
                    </form>
                </div>
</c:forEach>
                <form action="<c:url value="/addComment" />" method="post">
<textarea style="width:99%; margin-top: 20px;" rows="10" name="commentText">
</textarea>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <input type="hidden" name="newsId" value="${news.id}"/>
                    <input type="hidden" name="var" value="${var}">
                    <button type="submit" class="loginButton" style="float: right; margin-top: 20px"><spring:message
                            code="comment.post"/></button>
                </form>
            </div>
        </div>
    </div>
    <div >
        <c:if test="${not empty var}">
        <c:if test="${var!=0}">
            <div style="float:left; margin-top: 40px;">
                <a href="<c:url value="/newsView?id=${sessionScope.news[var-1].id}&var=${var-1}"/>"
                   style="color: blue;"><spring:message code="previous"/></a>
            </div>
        </c:if>
        <c:if test="${news.id != sessionScope.news[fn:length(sessionScope.news)-1].id}">
            <div style="float:right; margin-top: 40px;">
                <a href="<c:url value="/newsView?id=${sessionScope.news[var+1].id}&var=${var+1}"/>"
                   style="color: blue; "><spring:message code="next"/></a>
            </div>
        </c:if>
        </c:if>
    </div>
</div>
