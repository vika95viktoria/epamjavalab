package com.epam.news.service;


import com.epam.news.dao.CommentDAO;
import com.epam.news.domain.Comment;
import com.epam.news.service.impl.CommentServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by Viktoryia_Khlystun on 6/3/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {
    private static CommentService mockedCommentService;
    private static CommentDAO mockedCommentDAO;
    private static Comment comment;

    @Before
    public void setUp() {
        mockedCommentDAO = mock(CommentDAO.class);
        mockedCommentService = new CommentServiceImpl();
        mockedCommentService.setCommentDAO(mockedCommentDAO);
        comment = new Comment();
        Date date = new Date();
        comment.setId(1);
        comment.setText("Nice news");
        comment.setCreationDate(date);

    }

    @After
    public void reset() {
        Mockito.reset(mockedCommentDAO);
    }


    @Test
    public void delete() {
        mockedCommentService.deleteComment(comment);
        verify(mockedCommentDAO).delete(comment);
    }

    @Test
    public void create() {
        mockedCommentService.addComment(1, comment);
        verify(mockedCommentDAO).create(comment, 1);
    }

}
