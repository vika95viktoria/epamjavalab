package com.epam.news.service;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.domain.Author;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.impl.AuthorServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


/**
 * Created by Viktoryia_Khlystun on 6/3/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {
    private static AuthorService mockedAuthorService;
    private static AuthorDAO mockedAuthorDAO;
    private static Author author;
    private static Author authorForCreate;

    @Before
    public void setUp() throws DAOException {
        mockedAuthorDAO = mock(AuthorDAO.class);
        mockedAuthorService = new AuthorServiceImpl();
        mockedAuthorService.setAuthorDAO(mockedAuthorDAO);
        author = new Author();
        author.setId(111);
        author.setName("Vika");
        authorForCreate = new Author();
        authorForCreate.setName("Name");
        when(mockedAuthorDAO.findAuthorById(111)).thenReturn(author);

    }

    @After
    public void reset() {
        Mockito.reset(mockedAuthorDAO);
    }

    @Test
    public void getAuthorById() throws ServiceException, DAOException {
        Author mockedAuthor = mockedAuthorService.getAuthorById(111);
        assertEquals(mockedAuthor.getId(), 111);
        assertEquals(mockedAuthor.getName(), "Vika");
    }

    @Test
    public void update() throws ServiceException, DAOException {
        mockedAuthorService.save(author);
        verify(mockedAuthorDAO).update(author);
    }


    @Test
    public void create() throws ServiceException, DAOException {
        mockedAuthorService.save(authorForCreate);
        verify(mockedAuthorDAO).create(authorForCreate);
    }


}
