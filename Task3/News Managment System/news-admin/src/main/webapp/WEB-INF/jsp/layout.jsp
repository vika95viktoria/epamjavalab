<%@ page language="java" contentType="text/html;charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:choose>
    <c:when test="${not empty lang and lang=='ru'}">
        <fmt:setLocale value="ru" scope="session"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="en" scope="session"/>
    </c:otherwise>
</c:choose>
<html>
<head>
    <link rel="stylesheet" type="text/css"
          href="<c:url value="/resources/css/style.css"/>" />
    <link rel="stylesheet" type="text/css"
          href="<c:url value="/resources/css/jquery.multiselect.css"/>" />
    <link rel="stylesheet" type="text/css"
          href="<c:url value="/resources/css/jquery.bxslider.css"/>" />
    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    <title><tiles:insertAttribute name="title" ignore="true" /></title>
    <c:set var="title" scope="request"><tiles:getAsString name='title'/></c:set>
</head>
<body>
<div class="main">
    <div class="container bordered">
<tiles:insertAttribute name="header" />
        <tiles:insertAttribute name="menu" />
<tiles:insertAttribute name="body" />
    </div>
<tiles:insertAttribute name="footer" />
</div>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="<c:url value="/resources/js/jquery.bxslider.js"/>"></script>
<script src="<c:url value="/resources/js/jquery.multiselect.js"/>"></script>
<script src="<c:url value="/resources/js/scripts.js"/>"></script>
<script type="text/javascript">
    $(function() {
        $( "#datepicker-12" ).datepicker({
            dateFormat: "dd/mm/yy",
            todayHighlight: true,
            minDate: new Date()
        });
        $( "#datepicker-12" ).datepicker("setDate", new Date());
        $('select[multiple]').multiselect();

    });
</script>
</body>
</html>