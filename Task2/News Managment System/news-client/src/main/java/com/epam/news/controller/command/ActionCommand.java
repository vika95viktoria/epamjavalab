package com.epam.news.controller.command;

import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;
import com.epam.news.service.CommentService;
import com.epam.news.service.NewsService;
import com.epam.news.service.TagService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Viktoryia_Khlystun on 6/23/2016.
 */


public abstract class ActionCommand {

    private static  final Logger logger = Logger.getLogger(ActionCommand.class);

    @Autowired
    protected NewsService newsService;

    @Autowired
    protected TagService tagService;

    @Autowired
    protected AuthorService authorService;

    @Autowired
    protected CommentService commentService;


    /**
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void action(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {
    }

    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            action(request, response);
        } catch (IOException | ServiceException | ServletException e) {
            logger.error(e);
            RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/error/error500.jsp");
            dispatcher.forward(request, response);
        }
    }
}
