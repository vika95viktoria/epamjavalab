(function () {
    'use strict';

    var newsApp = angular.module('newsApp');

    newsApp.factory('NewsService', ['$http', '$q', function ($http, $q) {


        return {
            getAllTags: getAllTags,
            getAllAuthors: getAllAuthors,
            getNews: getNews,
            getCount: getCount,
            addNews: addNews,
            deleteNews: deleteNews,
            editNews: editNews
        };


        function getAllTags() {
            var deferred = $q.defer();
            $http.get('/admin/tags')
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Users');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }

        function getAllAuthors() {
            var deferred = $q.defer();
            $http.get('/admin/authors')
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Users');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }

        function getCount(authorName, tagNamesList) {
            var deferred = $q.defer();
            $http.get('/admin/count', {
                    params: {
                        authorName: authorName,
                        tagNamesList: tagNamesList
                    }
                })
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching count of News');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }

        function getNews(page, authorName, tagNamesList) {
            var deferred = $q.defer();
            $http.get('/admin/newsList', {
                    params: {
                        page: page,
                        authorName: authorName,
                        tagNamesList: tagNamesList
                    }
                })
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Users');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }

        function addNews(newsModel) {
            var deferred = $q.defer();
            $http.post('/admin/addNews', newsModel
            ).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while adding news');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }

        function deleteNews(newsIds) {
            var deferred = $q.defer();
            $http.post('/admin/deleteNews', $.param({
                    newsIds: newsIds
                }),
                {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                }
            ).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while adding news');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }

        function editNews(newsModel) {
            var deferred = $q.defer();
            $http.post('/admin/editNews', newsModel
            ).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while adding news');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }
    }]);
})();