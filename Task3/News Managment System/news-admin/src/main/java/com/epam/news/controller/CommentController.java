package com.epam.news.controller;

import com.epam.news.domain.Comment;
import com.epam.news.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Viktoryia_Khlystun on 7/4/2016.
 */
@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    /**
     * Adds comment to database and returns newsView page with updates (new comment is in list of comments)
     *
     * @param commentText
     * @param newsId
     * @param var
     * @return
     */
    @RequestMapping(value = "/addComment", method = RequestMethod.POST)
    public String addComment(@RequestParam String commentText, @RequestParam Long newsId, @RequestParam String var) {
        Comment comment = new Comment();
        comment.setText(commentText);
        commentService.addComment(newsId, comment);
        String newsView = "redirect:/newsView?id=" + newsId + "&var=" + var;
        return newsView;
    }

    /**
     * Deletes comment from database and returns newsView page with updates (deleted comment doesn;t display in the list of comments)
     * @param id
     * @param newsId
     * @param var
     * @return
     */
    @RequestMapping(value = "/deleteComment", method = RequestMethod.POST)
    public String deleteComment(@RequestParam Long id, @RequestParam Long newsId, @RequestParam String var) {
        Comment comment = new Comment();
        comment.setId(id);
        commentService.deleteComment(comment);
        String newsView = "redirect:/newsView?id=" + newsId + "&var=" + var;
        return newsView;
    }
}
