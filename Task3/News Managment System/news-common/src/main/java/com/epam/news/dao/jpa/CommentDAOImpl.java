package com.epam.news.dao.jpa;

import com.epam.news.dao.AbstractDAO;
import com.epam.news.dao.CommentDAO;
import com.epam.news.domain.Comment;
import com.epam.news.domain.News;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 7/19/2016.
 */
@Repository
@Profile("jpa")
public class CommentDAOImpl extends AbstractDAO implements CommentDAO {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Return list of comments to news
     *
     * @param newsId
     * @return
     */
    public List<Comment> findCommentsByNewsId(long newsId) {
        Query entityManagerQuery = entityManager
                .createQuery("select comment from Comment comment  where comment.news.id = :newsId");
        entityManagerQuery.setParameter("newsId", newsId);
        List<Comment> comments = entityManagerQuery.getResultList();
        return comments;
    }

    /**
     * Delete comment from database
     *
     * @param comment
     */


    public void delete(Comment comment) {
        comment = entityManager.find(Comment.class, comment.getId());
        entityManager.remove(comment);
    }

    /**
     * Create comment and link it to news
     *
     * @param comment
     * @param newsId
     */
    public void create(Comment comment, long newsId) {
        News news = (News) entityManager.find(News.class, newsId);
        comment.setCreationDate(new Date());
        comment.setNews(news);
        entityManager.persist(comment);
    }

}
