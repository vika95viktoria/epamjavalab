package com.epam.news.dao.impl;

import com.epam.news.dao.AbstractDAO;
import com.epam.news.dao.TagDAO;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DAOException;
import org.springframework.jdbc.datasource.DataSourceUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.news.util.DAOConstants.TAG_ID;
import static com.epam.news.util.DAOConstants.TAG_NAME;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
public class TagDAOImpl extends AbstractDAO implements TagDAO {
    private static final String SQL_INSERT_TAG_FOR_NEWS_BY_NEWS_ID = "insert into NEWS_TAG (NEWS_ID,TAG_ID) VALUES (?,?)";
    private static final String SQL_INSERT_TAG_FOR_NEWS = "insert into NEWS_TAG (NEWS_ID,TAG_ID) VALUES ((select news_id from news where title = ?),?)";
    private static final String SQL_CREATE_TAG = "insert into TAG (TAG_ID,TAG_NAME) VALUES (TAG_SEQ.nextval,?)";
    private static final String SQL_SELECT_TAG_BY_ID = "select * from TAG where TAG_ID=?";
    private static final String SQL_SELECT_TAG_FOR_NEWS_ID = "select * from tag where tag_id in(select TAG_ID from NEWS_TAG where NEWS_ID = ?)";
    private static final String SQL_DELETE_TAGS_FOR_NEWS = "delete from NEWS_TAG where NEWS_ID = ?";


    /**
     * Adding relation of tag and news into database when news is in process of creation
     *
     * @param news
     * @param tagId
     * @throws DAOException
     */
    public void addTagToNews(News news, long tagId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_INSERT_TAG_FOR_NEWS);
            statement.setString(1, news.getTitle());
            statement.setLong(2, tagId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Adding relation of tag and news into database when news is already created. (Link new tag to existing news)
     *
     * @param newsId
     * @param tagId
     * @throws DAOException
     */
    public void addTagToNews(long newsId, long tagId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_INSERT_TAG_FOR_NEWS_BY_NEWS_ID);
            statement.setLong(1, newsId);
            statement.setLong(2, tagId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Return list of tags for news
     *
     * @param newsId
     * @return
     * @throws DAOException
     */
    public List<Tag> findTagsByNewsId(long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        List<Tag> tags = new ArrayList<Tag>();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_SELECT_TAG_FOR_NEWS_ID);
            statement.setLong(1, newsId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Tag tag = new Tag();
                tag.setId(resultSet.getLong(TAG_ID));
                tag.setName(resultSet.getString(TAG_NAME));
                tags.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return tags;
    }

    /**
     * Return all tag info by id
     *
     * @param tagId
     * @return
     * @throws DAOException
     */
    public Tag findTagById(long tagId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        Tag tag = new Tag();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_SELECT_TAG_BY_ID);
            statement.setLong(1, tagId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                tag.setId(resultSet.getLong(TAG_ID));
                tag.setName(resultSet.getString(TAG_NAME));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return tag;
    }

    /**
     * Create new record in database with all tag info
     *
     * @param tag
     * @throws DAOException
     */
    public void create(Tag tag) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_CREATE_TAG);
            statement.setString(1, tag.getName());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Delete relation of news and all tags when you delete news
     *
     * @param newsId
     * @throws DAOException
     */
    public void deleteTagsFromNews(long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE_TAGS_FOR_NEWS);
            statement.setLong(1, newsId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }
}
