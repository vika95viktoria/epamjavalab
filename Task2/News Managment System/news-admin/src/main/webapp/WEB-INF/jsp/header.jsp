<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<header>
    <div  class="admin">
        <div class="clearfix" style="width: 100%">
            <div class="titleNews">
                <h1 style="color: blue; margin-left: 20px">${requestScope.title}</h1>
            </div>
            <div class="language-box">
                <span style="font-weight:bold"><spring:message code="admin.hello"/> <sec:authentication
                        property="principal.username"/></span>
                <form action="<c:url value='/logout' />" method="post" style="display:inline-block;">
                    <input type="hidden" name="${_csrf.parameterName}"
                           value="${_csrf.token}" />
                    <button class="logoutButton" type="submit"><spring:message code="header.logout"/></button>
                </form>
                <br>

                <div class="language-box">
                    <a href="?lang=en" style="color: blue;">EN</a>
                    <a href="?lang=ru" style="color: blue;">RU</a>
                </div>
            </div>
        </div>
    </div>
</header>
