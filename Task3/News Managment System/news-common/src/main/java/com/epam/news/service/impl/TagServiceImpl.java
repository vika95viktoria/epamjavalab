package com.epam.news.service.impl;

import com.epam.news.dao.TagDAO;
import com.epam.news.domain.Tag;
import com.epam.news.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/31/2016.
 */
@Transactional
@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagDAO tagDAO;

    public void setTagDAO(TagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }



    /**
     * create or update tag
     * @param tag
     */
    public void save(Tag tag) {
        if (tag.getId() != 0) {
            tagDAO.update(tag);
        } else {
            tagDAO.create(tag);
        }
    }


    /**
     * Return all tag info by id
     *
     * @param tagId
     * @return
     */
    public Tag findTagById(long tagId) {
        Tag tag = tagDAO.findTagById(tagId);
        return tag;
    }


    /**
     * get list of all tags in database
     * @return
     */
    public List<Tag> findAll() {
        List<Tag> tags = tagDAO.findAll();
        return tags;
    }

    /**
     * get list of all tags in database in some limits
     * @param start
     * @return
     */
    public List<Tag> findAll(long start) {
        List<Tag> authors = tagDAO.findAll(start);
        return authors;
    }

    /**
     * get count of tags in database
     * @return
     */
    public int getCountOfTags() {
        return tagDAO.getCountOfTags();
    }

    /**
     * delete tag from database
     * @param tag
     */
    public void delete(Tag tag) {
        tagDAO.delete(tag);
    }
}
