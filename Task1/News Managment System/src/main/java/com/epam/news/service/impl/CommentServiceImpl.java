package com.epam.news.service.impl;

import com.epam.news.dao.CommentDAO;
import com.epam.news.domain.Comment;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.CommentService;

/**
 * Created by Viktoryia_Khlystun on 5/31/2016.
 */

public class CommentServiceImpl implements CommentService {
    private CommentDAO commentDAO;

    public void setCommentDAO(CommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

    /**
     * Add new comment to news
     *
     * @param newsId
     * @param comment
     * @throws ServiceException
     */
    public void addComment(long newsId, Comment comment) throws ServiceException {
        try {
            commentDAO.create(comment, newsId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Delete comment from discussion
     *
     * @param comment
     * @throws ServiceException
     */
    public void deleteComment(Comment comment) throws ServiceException {
        try {
            commentDAO.delete(comment);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Return comment info by id
     *
     * @param id
     * @return
     * @throws ServiceException
     */
    public Comment findCommentById(long id) throws ServiceException {
        Comment comment;
        try {
            comment = commentDAO.findCommentById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return comment;
    }

    /**
     * Edit comment text
     *
     * @param comment
     * @throws ServiceException
     */
    public void update(Comment comment) throws ServiceException {
        try {
            commentDAO.update(comment);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
