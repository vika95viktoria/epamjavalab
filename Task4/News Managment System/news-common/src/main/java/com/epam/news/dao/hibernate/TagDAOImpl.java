package com.epam.news.dao.hibernate;

import com.epam.news.dao.AbstractDAO;
import com.epam.news.dao.TagDAO;
import com.epam.news.domain.Tag;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */

@Repository
@Profile("hibernate")
public class TagDAOImpl extends AbstractDAO implements TagDAO {

    /**
     * Return all tag info by id
     *
     * @param tagId
     * @return
     */
    public Tag findTagById(long tagId) {
        Session session = sessionFactory.getCurrentSession();
        Tag tag = session.get(Tag.class, tagId);
        return tag;
    }

    /**
     * Create new record in database with all tag info
     *
     * @param tag
     */
    public void create(Tag tag) {
        Session session = sessionFactory.getCurrentSession();
        session.save(tag);

    }

    /**
     * update the name of tag
     *
     * @param tag
     */
    public void update(Tag tag) {
        Session session = sessionFactory.getCurrentSession();
        session.update(tag);
    }


    /**
     * get list of all tags in system
     *
     * @return
     */
    public List<Tag> findAll() {
        Session session = sessionFactory.getCurrentSession();
        List<Tag> tags = session.createQuery("from Tag").list();
        return tags;
    }

    /**
     * get list of tags in limits
     *
     * @param start
     * @return
     */
    public List<Tag> findAll(long start) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Tag");
        query.setFirstResult((int) start);
        query.setMaxResults(7);
        List<Tag> tags = query.list();
        return tags;
    }


    /**
     * get count of tags in system
     *
     * @return
     */

    public int getCountOfTags() {
        Session session = sessionFactory.getCurrentSession();
        Criteria crit = session.createCriteria(Tag.class);
        crit.setProjection(Projections.rowCount());
        int count = ((Long) crit.uniqueResult()).intValue();
        return count;
    }

    /**
     * delete tag from database
     *
     * @param entity
     */
    public void delete(Tag entity) {
        Session session = sessionFactory.getCurrentSession();
        entity = session.load(Tag.class, entity.getId());
        session.delete(entity);
    }
}
