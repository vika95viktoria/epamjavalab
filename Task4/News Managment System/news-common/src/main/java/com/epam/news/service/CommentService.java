package com.epam.news.service;

import com.epam.news.dao.CommentDAO;
import com.epam.news.domain.Comment;
import com.epam.news.model.CommentModel;

/**
 * Created by Viktoryia_Khlystun on 5/31/2016.
 */
public interface CommentService {
    CommentModel addComment(long newsId, Comment comment);

    void deleteComment(Comment comment);

    void setCommentDAO(CommentDAO commentDAO);
}
