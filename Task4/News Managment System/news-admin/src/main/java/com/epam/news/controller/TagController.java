package com.epam.news.controller;

import com.epam.news.domain.Tag;
import com.epam.news.service.TagService;
import com.epam.news.util.ErrorListCreator;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.hibernate5.HibernateOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 7/4/2016.
 */

@Controller
public class TagController {

    private static final int ITEM_PER_PAGE = 7;
    @Autowired
    private TagService tagService;

    /**
     * returns tag page with list of tags corresponding to the page
     *
     * @param page
     * @return
     */

    @ResponseBody
    @RequestMapping(value = "/tagPage", method = RequestMethod.GET)
    public List<Tag> tagPage(@RequestParam(required = false) Integer page) {
        if (page != null) {
            page = page - 1;
        } else {
            page = 0;
        }
        List<Tag> tagList = tagService.findAll(ITEM_PER_PAGE * page);
        return tagList;
    }

    @ResponseBody
    @RequestMapping(value = {"/tagCount"}, method = RequestMethod.GET)
    public int tagCount() {
        return tagService.getCountOfTags();
    }

    @ResponseBody
    @RequestMapping(value = "/tags", method = RequestMethod.GET)
    public List<Tag> tagList() {
        List<Tag> tagList = tagService.findAll();
        return tagList;
    }

    /**
     * deletes tag from database
     *
     * @param tag
     * @return
     */
    @RequestMapping(value = "/deleteTag", method = RequestMethod.POST)
    public ResponseEntity deleteTag(@RequestBody Tag tag) {
        try {
            tagService.delete(tag);
        } catch (IllegalArgumentException | ObjectNotFoundException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(ErrorListCreator.getErrorList("Tag has already been deleted."));
        }
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(tag);
    }

    /**
     * Updates the value of tag
     *
     * @param tag
     * @return
     */
    @RequestMapping(value = "/updateTag", method = RequestMethod.POST)
    public ResponseEntity updateTag(@RequestBody Tag tag) {
        try {
            tagService.save(tag);
        } catch (JpaOptimisticLockingFailureException | HibernateOptimisticLockingFailureException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(ErrorListCreator.getErrorList("Tag has been edited or deleted. Try again, please"));
        }
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(tag);
    }

    /**
     * Adds new tag to the database
     *
     * @param tagName
     * @return
     */
    @RequestMapping(value = "/addTag", method = RequestMethod.POST)
    public ResponseEntity addTag(@RequestParam String tagName) {
        Tag tag = new Tag(tagName);
        tagService.save(tag);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(tag);
    }
}
