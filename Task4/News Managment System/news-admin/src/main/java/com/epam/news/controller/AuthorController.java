package com.epam.news.controller;

import com.epam.news.domain.Author;
import com.epam.news.service.AuthorService;
import com.epam.news.util.ErrorListCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.hibernate5.HibernateOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 7/4/2016.
 */

@Controller
public class AuthorController {

    private static final int ITEM_PER_PAGE = 7;
    @Autowired
    private AuthorService authorService;

    /**
     * Deletes author from database and redirects to authorPage with updates
     *
     * @param author
     * @return
     */
    @RequestMapping(value = "/deleteAuthor", method = RequestMethod.POST)
    public ResponseEntity expireAuthor(@RequestBody Author author) {
        author.setExpired(new Date());
        try {
            authorService.delete(author);
        } catch (JpaOptimisticLockingFailureException | HibernateOptimisticLockingFailureException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(ErrorListCreator.getErrorList("Author has been edited or deleted. Try again, please"));
        }
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(author);
    }

    /**
     * Changes author info in database and redirects to authorPage with updates
     *
     * @param author
     * @return
     */
    @RequestMapping(value = "/updateAuthor", method = RequestMethod.POST, headers = {})
    public ResponseEntity updateAuthor(@RequestBody Author author) {
        try {
            authorService.save(author);
        } catch (JpaOptimisticLockingFailureException | HibernateOptimisticLockingFailureException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(ErrorListCreator.getErrorList("Author has been edited or deleted. Try again, please"));
        }
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(author);
    }

    /**
     * Return authorPage with list of authors present in database corresponding to requested page
     *
     * @param page
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/authorPage", method = RequestMethod.GET)
    public List<Author> authorPage(@RequestParam(required = false) Integer page) {
        if (page != null) {
            page = page - 1;
        } else {
            page = 0;
        }
        List<Author> authorList = authorService.findAll(ITEM_PER_PAGE * page);
        return authorList;
    }

    @ResponseBody
    @RequestMapping(value = {"/authorCount"}, method = RequestMethod.GET)
    public int tagCount() {
        return authorService.getCountOfAuthors();
    }

    @ResponseBody
    @RequestMapping(value = "/authors", method = RequestMethod.GET)
    public List<Author> authorList() {
        List<Author> authorList = authorService.findAll();
        return authorList;
    }

    /**
     * Adds new author to database and redirects to authorPage with updates
     *
     * @param authorName
     * @return
     */
    @RequestMapping(value = "/addAuthor", method = RequestMethod.POST)
    public ResponseEntity addAuthor(@RequestParam String authorName) {
        Author author = new Author(authorName);
        authorService.save(author);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(author);
    }
}
