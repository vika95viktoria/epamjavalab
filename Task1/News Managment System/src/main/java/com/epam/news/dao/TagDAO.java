package com.epam.news.dao;

import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DAOException;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
public interface TagDAO {
    void addTagToNews(News news, long tagId) throws DAOException;

    List<Tag> findTagsByNewsId(long newsId) throws DAOException;

    Tag findTagById(long tagId) throws DAOException;

    void create(Tag tag) throws DAOException;

    void deleteTagsFromNews(long newsId) throws DAOException;

    void addTagToNews(long newsId, long tagId) throws DAOException;
}
