package com.epam.news.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

/**
 * Created by Viktoryia_Khlystun on 6/3/2016.
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/testContext.xml"})
@DatabaseSetup(value = "/data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class NewsDAOTest {
    private static NewsDAO newsDAO;
    private static ApplicationContext context;


    @BeforeClass
    public static void setUpAuthorDAO() {
        context = new ClassPathXmlApplicationContext("testContext.xml");
        newsDAO = (NewsDAO) context.getBean("NewsDAO");
    }

    @AfterClass
    public static void closeContext() {
        ((ConfigurableApplicationContext) context).close();
    }


    @Test
    public void getCountOfNews() {
        int countOfNews = newsDAO.getCountOfNews();
        assertEquals(countOfNews, 3);
    }

   /* @Test
    public void getMostCommenting() throws DAOException {
        List<Long> mostCommenting = newsDAO.getMostCommenting(0);
        assertEquals(mostCommenting.get(0), new Long(1));
    }

    @Test
    public void findById() throws DAOException {
        News news = newsDAO.findNewsById(1);
        assertEquals(news.getId(), 1);
        assertEquals(news.getTitle(), "Paris");
        assertEquals(news.getShortText(), "Paris river to peak at six metres");
        assertEquals(news.getFullText(), "Floodwaters in Paris are forecast to peak on Friday with the River Seine due to reach 6m (19ft) above its normal level");
    }


    @Test
    public void update() throws DAOException {
        News news = newsDAO.findNewsById(1);
        news.setTitle("Six metres");
        newsDAO.update(news);
        news = newsDAO.findNewsById(1);
        assertEquals(news.getTitle(), "Six metres");
        assertEquals(news.getId(), 1);
    }

    @Test
    public void searchByAuthor() throws DAOException {
        Author author = new Author();
        author.setName("Ali");
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAuthor(author);
        List<Long> listOfNews = newsDAO.getListOfNews(searchCriteria);
        assertEquals(listOfNews.size(), 1);
    }

    @Test
    public void searchWithoutParameters() throws DAOException {
        SearchCriteria searchCriteria = new SearchCriteria();
        List<Long> listOfNews = newsDAO.getListOfNews(searchCriteria);
        assertEquals(listOfNews.size(), 0);
    }

    @Test
    public void searchByOneTag() throws DAOException {
        SearchCriteria searchCriteria = new SearchCriteria();
        Tag tag = new Tag();
        tag.setId(2);
        tag.setName("Politics");
        List<Tag> tags = new ArrayList<>();
        tags.add(tag);
        searchCriteria.setTags(tags);
        List<Long> listOfNews = newsDAO.getListOfNews(searchCriteria);
        assertEquals(listOfNews.size(), 2);
    }

    @Test
    public void searchByTags() throws DAOException {
        SearchCriteria searchCriteria = new SearchCriteria();
        Tag tag = new Tag();
        tag.setName("Politics");
        Tag tag2 = new Tag();
        tag2.setName("World");
        List<Tag> tags = new ArrayList<>();
        tags.add(tag);
        tags.add(tag2);
        searchCriteria.setTags(tags);
        List<Long> listOfNews = newsDAO.getListOfNews(searchCriteria);
        assertEquals(listOfNews.size(), 3);
    }

    @Test
    public void searchByOneTagAndAuthor() throws DAOException {
        SearchCriteria searchCriteria = new SearchCriteria();
        Tag tag = new Tag();
        tag.setName("Politics");
        List<Tag> tags = new ArrayList<>();
        tags.add(tag);
        Author author = new Author();
        author.setName("Ali");
        searchCriteria.setTags(tags);
        searchCriteria.setAuthor(author);
        List<Long> listOfNews = newsDAO.getListOfNews(searchCriteria);
        assertEquals(listOfNews.size(), 1);
    }

    @Test
    public void searchByTagsAndAuthor() throws DAOException {
        SearchCriteria searchCriteria = new SearchCriteria();
        List<Tag> tags = new ArrayList<>();
        Tag tag = new Tag();
        tag.setName("Politics");
        Tag tag2 = new Tag();
        tag2.setName("World");
        tags.add(tag);
        tags.add(tag2);
        Author author = new Author();
        author.setName("Ivan");
        searchCriteria.setTags(tags);
        searchCriteria.setAuthor(author);
        List<Long> listOfNews = newsDAO.getListOfNews(searchCriteria);
        assertEquals(listOfNews.size(), 2);
    }
*/
}
