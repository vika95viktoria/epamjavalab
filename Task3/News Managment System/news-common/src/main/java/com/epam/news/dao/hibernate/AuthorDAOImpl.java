package com.epam.news.dao.hibernate;

import com.epam.news.dao.AbstractDAO;
import com.epam.news.dao.AuthorDAO;
import com.epam.news.domain.Author;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */

@Repository
@Profile("hibernate")
public class AuthorDAOImpl extends AbstractDAO implements AuthorDAO {

    /**
     * Get count of all authors in system
     *
     * @return
     */
    public int getCountOfAuthors() {
        Session session = sessionFactory.getCurrentSession();
        Criteria crit = session.createCriteria(Author.class);
        crit.setProjection(Projections.rowCount());
        int count = ((Long) crit.uniqueResult()).intValue();
        return count;
    }


    /**
     * Add new record with author info to database
     *
     * @param author
     */
    public void create(Author author) {
        Session session = sessionFactory.getCurrentSession();
        session.save(author);
    }

    /**
     * Return all author info by id
     *
     * @param authorId
     * @return
     */
    public Author findAuthorById(long authorId) {
        Session session = sessionFactory.getCurrentSession();
        Author author = session.get(Author.class, authorId);
        return author;
    }


    /**
     * Change author's name
     *
     * @param entity
     */
    public void update(Author entity) {
        Session session = sessionFactory.getCurrentSession();
        session.update(entity);
    }


    /**
     * Get list of all authors in system
     *
     * @return
     */
    public List<Author> findAll() {
        Session session = sessionFactory.getCurrentSession();
        List<Author> authors = session.createQuery("from Author").list();
        return authors;
    }

    /**
     * Get list of all authors in system in some limits (for pagination)
     *
     * @param start
     * @return
     */

    public List<Author> findAll(long start) {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Author.class);
        criteria.setFirstResult((int) start);
        criteria.setMaxResults(7);
        List<Author> authors = criteria.list();
        return authors;
    }
}
