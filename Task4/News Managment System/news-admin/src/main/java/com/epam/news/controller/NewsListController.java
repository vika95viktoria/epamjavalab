package com.epam.news.controller;

import com.epam.news.domain.Author;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.model.NewsModelList;
import com.epam.news.search.SearchCriteria;
import com.epam.news.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 7/4/2016.
 */
@Controller
public class NewsListController {

    private static final int ITEM_PER_PAGE = 4;
    @Autowired
    private NewsService newsService;

    /**
     * Returns newsList page with list of news corresponding to incoming parameters such as page and/or s/earch criteria
     *
     * @param page
     * @param authorName
     * @param tagNamesList
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/", "/newsList"}, method = RequestMethod.GET)
    public List<NewsModelList> newsList(@RequestParam(required = false) Integer page, @RequestParam(required = false) String authorName, @RequestParam(required = false, value = "tagNamesList") String[] tagNamesList) {
        SearchCriteria searchCriteria = createSearchCriteria(authorName, tagNamesList);
        List<NewsModelList> newsList;
        if (page != null) {
            page = page - 1;
        } else {
            page = 0;
        }
        if (searchCriteria != null) {
            newsList = newsService.searchForNews(searchCriteria, ITEM_PER_PAGE * page, ITEM_PER_PAGE * page + ITEM_PER_PAGE);
        } else {
            newsList = newsService.getNewsOrderedByComments(ITEM_PER_PAGE * page);
        }

        return newsList;
    }


    @ResponseBody
    @RequestMapping(value = {"/count"}, method = RequestMethod.GET)
    public int newsCount(@RequestParam(required = false) String authorName, @RequestParam(required = false, value = "tagNamesList") String[] tagNamesList) {
        SearchCriteria searchCriteria = createSearchCriteria(authorName, tagNamesList);
        int newsCount;
        if (searchCriteria != null) {
            newsCount = newsService.getCountOfNews(searchCriteria);
        } else {
            newsCount = newsService.getCountOfNews();
        }
        return newsCount;
    }

    /**
     * Returns newsView page
     *
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/newsView", method = RequestMethod.GET)
    public News openViewPage(@RequestParam Long id) {
        News news = newsService.findNewsById(id);
        return news;
    }

    /**
     * Returns newsView page for next/prev buttons
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/stepNews", method = RequestMethod.GET)
    public News openNews(@RequestParam int position, @RequestParam int index, @RequestParam(required = false) String authorName, @RequestParam(required = false, value = "tagNamesList") String[] tagNamesList) {
        News news;
        SearchCriteria searchCriteria = createSearchCriteria(authorName, tagNamesList);
        if (searchCriteria != null) {
            news = newsService.getNewsFromChain(searchCriteria, position, index);
        } else {
            news = newsService.getNewsFromChain(position, index);
        }
        return news;
    }

    /**
     * Deeletes news from database and redirect to the newsList page
     *
     * @param newsIds
     * @return
     */
    @RequestMapping(value = "/deleteNews", method = RequestMethod.POST)
    public ResponseEntity deleteNews(@RequestParam(value = "newsIds[]", required = false) Long[] newsIds) {
        newsService.deleteNews(newsIds);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(newsService.getNewsOrderedByComments(0));
    }

    private SearchCriteria createSearchCriteria(String authorName, String[] tagNamesList) {
        SearchCriteria searchCriteria = null;
        List<String> tagNames = new ArrayList<>();
        if (tagNamesList != null) {
            tagNames = Arrays.asList(tagNamesList);
        }
        if (!tagNames.isEmpty() || authorName != null && !"".equals(authorName)) {
            searchCriteria = new SearchCriteria();
            if (authorName != null && !"".equals(authorName)) {
                Author author = new Author();
                author.setName(authorName);
                searchCriteria.setAuthor(author);
            }
            if (!tagNames.isEmpty()) {
                List<Tag> tags = new ArrayList<>();
                for (String tagName : tagNames) {
                    Tag tag = new Tag();
                    tag.setName(tagName);
                    tags.add(tag);
                }
                searchCriteria.setTags(tags);
            }
        }
        return searchCriteria;
    }
}
