<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:choose>
    <c:when test="${not empty language && language=='RU'}">
        <fmt:setLocale value="ru_RU" scope="session"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="en_US" scope="session"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="pagecontent"/>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery.multiselect.css"/>">
    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script src="<c:url value="/resources/js/jquery.multiselect.js"/>"></script>
    <script src="<c:url value="/resources/js/scripts.js"/>"></script>
    <title><tiles:insertAttribute name="title" ignore="true" /></title>
    <c:set var="title" scope="request"><tiles:getAsString name='title'/></c:set>
</head>
<body>
<div class="main">
    <div class="container bordered">
<tiles:insertAttribute name="header" />
<tiles:insertAttribute name="body" />
    </div>
<tiles:insertAttribute name="footer" />
</div>
<script type="application/javascript">
    $(function() {
        $('select[multiple]').multiselect();
    });
</script>
</body>
</html>