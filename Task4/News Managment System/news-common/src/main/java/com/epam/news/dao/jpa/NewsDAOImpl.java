package com.epam.news.dao.jpa;

import com.epam.news.dao.AbstractDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.domain.Author;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.model.NewsModelList;
import com.epam.news.search.SearchCriteria;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 7/19/2016.
 */
@Repository
@Profile("jpa")
public class NewsDAOImpl extends AbstractDAO implements NewsDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public News findNewsById(long id) {
        News news = (News) entityManager.find(News.class, id);
        return news;
    }

    /**
     * Find news by title
     *
     * @param title
     * @return
     */
    public News findNewsByTitle(String title) {
        javax.persistence.Query entityManagerQuery = entityManager.createQuery("select n from News as n where n.title = :newsTitle");
        entityManagerQuery.setParameter("newsTitle", title);
        News news = (News) entityManagerQuery.getSingleResult();
        return news;
    }

    /**
     * Get list of news, which id is present in param ids
     *
     * @param ids
     * @return
     */
    public List<News> getSubListOfNews(List<Long> ids) {
        Query entityManagerQuery = entityManager
                .createQuery("Select n from News n where n.id in :newsIds");
        entityManagerQuery.setParameter("newsIds", ids);
        List<News> news = entityManagerQuery.getResultList();
        return news;
    }

    /**
     * Create a record in database with news info, only news description? not including tags, authors and comments
     *
     * @param news
     */
    public long create(News news) {
        entityManager.persist(news);
        return news.getId();

    }

    /**
     * Update news text, title and change modification date
     *
     * @param news
     */
    public long update(News news) {
        String title = news.getTitle();
        String shortText = news.getShortText();
        String fullText = news.getFullText();
        List<Author> authors = news.getAuthors();
        List<Tag> tags = news.getTags();
        int version = news.getVersion();
        Date modificationDate = new Date();
        news = (News) entityManager.find(News.class, news.getId());
        news.setTitle(title);
        news.setModificationDate(modificationDate);
        news.setShortText(shortText);
        news.setFullText(fullText);
        news.setAuthors(authors);
        news.setTags(tags);
        news.setVersion(version);
        return news.getId();
    }

    /**
     * Delete news by id from table with comments deleting on cascade
     *
     * @param news
     */
    public void delete(News news) {
        news = (News) entityManager.find(News.class, news.getId());
        entityManager.remove(news);
    }

    public void delete(Long[] newsIds) {
       /* Session session = sessionFactory.getCurrentSession();
        session.delete(news);*/
    }

    /**
     * Return sorted list of newsIds desc from most commenting
     *
     * @return
     */
    public List<NewsModelList> getMostCommenting(int start) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root news = criteriaQuery.from(News.class);
        criteriaQuery.select(news.get("id"));
        criteriaQuery.groupBy(news.get("id"), news.get("modificationDate"));
        criteriaQuery.orderBy(criteriaBuilder.desc(criteriaBuilder.size(news.<Collection>get("comments"))), criteriaBuilder.desc(news.get("modificationDate")));
        TypedQuery query = entityManager.createQuery(criteriaQuery);
        query.setFirstResult((int) start);
        query.setMaxResults(4);
        List<Long> resultList = query.getResultList();
        List<NewsModelList> newsList = new ArrayList<>();
        return newsList;
    }


    //TODO
    public News getNextNews(int position, int index) {
        return null;
    }

    public News getNextNews(SearchCriteria searchCriteria, int position, int index) {
        return null;
    }

    /**
     * Get list of most commenting ids present in param ids in some limits
     *
     * @param start
     * @param end
     * @param ids
     * @return
     */
    public List<Long> getMostCommenting(long start, long end, List<Long> ids) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root news = criteriaQuery.from(News.class);
        criteriaQuery.select(news.get("id")).where(news.in(ids));
        criteriaQuery.groupBy(news.get("id"), news.get("modificationDate"));
        criteriaQuery.orderBy(criteriaBuilder.desc(criteriaBuilder.size(news.<Collection>get("comments"))), criteriaBuilder.desc(news.get("modificationDate")));
        TypedQuery query = entityManager.createQuery(criteriaQuery);
        query.setFirstResult((int) start);
        query.setMaxResults(4);
        List<Long> resultList = query.getResultList();
        return resultList;
    }

    /**
     * Return the count of records in database in table news
     *
     * @return
     */
    public int getCountOfNews() {
        int count = ((Long) entityManager.createQuery("select count(n) from News n").getSingleResult()).intValue();
        return count;
    }

    //TODO
    public List<NewsModelList> getListOfNews(SearchCriteria searchCriteria, int start, int end) {
        List<NewsModelList> newsList = new ArrayList<>();
        List<Long> result = buildJPAQuery(searchCriteria).getResultList();
        return newsList;
    }

    /**
     * return count of news according to search criteria
     *
     * @param searchCriteria
     * @return
     */
    public int getCountOfListNews(SearchCriteria searchCriteria) {
        return buildCountQuery(searchCriteria);
    }


    private javax.persistence.Query buildJPAQuery(SearchCriteria searchCriteria) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root news = criteriaQuery.from(News.class);
        criteriaQuery.distinct(true);
        if (searchCriteria.getAuthor() != null) {
            if (searchCriteria.getTags() != null) {
                List<String> tags = new ArrayList<>();
                for (Tag tag : searchCriteria.getTags()) {
                    tags.add(tag.getName());
                }
                criteriaQuery.select(news.get("id")).where(news.<Collection>get("tags").get("name").in(tags),
                        criteriaBuilder.like(news.<Collection>get("authors").get("name"), searchCriteria.getAuthor().getName()));

            } else {
                criteriaQuery.select(news.get("id")).where(criteriaBuilder.like(news.<Collection>get("authors").get("name"), searchCriteria.getAuthor().getName()));
            }
        } else if (searchCriteria.getTags() != null) {
            List<String> tags = new ArrayList<>();
            for (Tag tag : searchCriteria.getTags()) {
                tags.add(tag.getName());
            }
            criteriaQuery.select(news.get("id")).where(news.<Collection>get("tags").get("name").in(tags));

        }
        TypedQuery query = entityManager.createQuery(criteriaQuery);
        return query;
    }

    private int buildCountQuery(SearchCriteria searchCriteria) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root news = criteriaQuery.from(News.class);
        criteriaQuery.distinct(true);
        if (searchCriteria.getAuthor() != null) {
            if (searchCriteria.getTags() != null) {
                List<String> tags = new ArrayList<>();
                for (Tag tag : searchCriteria.getTags()) {
                    tags.add(tag.getName());
                }
                criteriaQuery.select(criteriaBuilder.count(news.get("id"))).where(news.<Collection>get("tags").get("name").in(tags),
                        criteriaBuilder.like(news.<Collection>get("authors").get("name"), searchCriteria.getAuthor().getName()));

            } else {
                criteriaQuery.select(criteriaBuilder.count(news.get("id"))).where(criteriaBuilder.like(news.<Collection>get("authors").get("name"), searchCriteria.getAuthor().getName()));
            }
        } else if (searchCriteria.getTags() != null) {
            List<String> tags = new ArrayList<>();
            for (Tag tag : searchCriteria.getTags()) {
                tags.add(tag.getName());
            }
            criteriaQuery.select(criteriaBuilder.count(news.get("id"))).where(news.<Collection>get("tags").get("name").in(tags));
        }

        TypedQuery query = entityManager.createQuery(criteriaQuery);
        int count = ((Long) query.getSingleResult()).intValue();
        return count;
    }

}
