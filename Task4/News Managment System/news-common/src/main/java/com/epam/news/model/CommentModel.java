package com.epam.news.model;

import com.epam.news.domain.Comment;

import java.util.Date;

/**
 * Created by Viktoryia_Khlystun on 10/10/2016.
 */
public class CommentModel {
    private long id;

    private String text;


    private Date creationDate;

    public CommentModel() {
    }

    public CommentModel(Comment comment) {
        this.id = comment.getId();
        this.creationDate = comment.getCreationDate();
        this.text = comment.getText();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
