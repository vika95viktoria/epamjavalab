package com.epam.news.controller.command;

import com.epam.news.domain.Comment;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Viktoryia_Khlystun on 6/28/2016.
 */
@Component
public class PostCommentCommand extends ActionCommand {

    /**
     * Add comment to database and redirect to newsList
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void action(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String commentText = request.getParameter("comment");
        String var = request.getParameter("var");
        String newsId = request.getParameter("newsId");
        Comment comment = new Comment();
        comment.setText(commentText);
        commentService.addComment(Long.parseLong(newsId), comment);
        String newsView = "?command=newsView&id="+newsId+"&var="+var;
        response.sendRedirect(newsView);
    }
}
