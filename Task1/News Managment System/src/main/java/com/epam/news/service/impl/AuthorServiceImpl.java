package com.epam.news.service.impl;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.domain.Author;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;

/**
 * Created by Viktoryia_Khlystun on 5/31/2016.
 */

public class AuthorServiceImpl implements AuthorService {
    private AuthorDAO authorDAO;

    public void setAuthorDAO(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    /**
     * Add author to news
     *
     * @param authorId
     * @param newsId
     * @throws ServiceException
     */

    public void addAuthorToNews(long authorId, long newsId) throws ServiceException {
        try {
            authorDAO.addAuthorToNews(newsId, authorId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Create new author
     *
     * @param author
     * @throws ServiceException
     */
    public void create(Author author) throws ServiceException {
        try {
            authorDAO.create(author);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Delete author
     *
     * @param author
     * @throws ServiceException
     */
    public void delete(Author author) throws ServiceException {
        try {
            authorDAO.delete(author);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Edit author name
     *
     * @param author
     * @throws ServiceException
     */
    public void update(Author author) throws ServiceException {
        try {
            authorDAO.update(author);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Return author info by it id
     *
     * @param id
     * @return
     * @throws ServiceException
     */
    public Author getAuthorById(long id) throws ServiceException {
        Author author;
        try {
            author = authorDAO.findAuthorById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return author;
    }
}
