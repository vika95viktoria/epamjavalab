package com.epam.news.controller;

import com.epam.news.controller.command.ActionCommand;
import com.epam.news.controller.command.ActionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Viktoryia_Khlystun on 6/20/2016.
 */
@WebServlet("/news")
public class NewsServlet extends HttpServlet {

    @Autowired
    private ActionFactory actionFactory;


    /**
     * Initialize application context and allow to autowire beans
     *
     * @param config
     * @throws ServletException
     */
    @Override
    public void init(final ServletConfig config) throws ServletException {
        super.init(config);
        WebApplicationContext springContext = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        final AutowireCapableBeanFactory beanFactory = springContext.getAutowireCapableBeanFactory();
        beanFactory.autowireBean(this);
    }

    /**
     * Handles all get requests
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        actionFactory.createActionFactory();
        String action = request.getParameter("command");
        ActionCommand command = actionFactory.defineGetCommand(action);
        command.execute(request,response);
    }

    /**
     * Handles all post requests
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        actionFactory.createActionFactory();
        String action = request.getParameter("command");
        ActionCommand command = actionFactory.definePostCommand(action);
<<<<<<< HEAD
        command.execute(request, response);
=======
        command.action(request, response);

>>>>>>> develop
    }

}
