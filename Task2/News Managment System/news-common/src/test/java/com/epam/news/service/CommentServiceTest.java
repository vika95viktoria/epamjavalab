package com.epam.news.service;


import com.epam.news.dao.CommentDAO;
import com.epam.news.dao.impl.CommentDAOImpl;
import com.epam.news.domain.Comment;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.impl.CommentServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Viktoryia_Khlystun on 6/3/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {
    private static CommentService mockedCommentService;
    private static CommentDAO mockedCommentDAO;
    private static Comment comment;

    @Before
    public void setUp() throws DAOException {
        mockedCommentDAO = mock(CommentDAO.class);
        mockedCommentService = new CommentServiceImpl();
        mockedCommentService.setCommentDAO(mockedCommentDAO);
        comment = new Comment();
        Date date = new Date();
        comment.setId(1);
        comment.setText("Nice news");
        comment.setCreationDate(date);
        when(mockedCommentDAO.findCommentById(1)).thenReturn(comment);

    }

    @After
    public void reset() {
        Mockito.reset(mockedCommentDAO);
    }

    @Test
    public void getCommentById() throws ServiceException, DAOException {
        Comment mockedComment = mockedCommentService.findCommentById(1);
        assertEquals(mockedComment.getId(), 1);
        assertEquals(mockedComment.getText(), "Nice news");
        assertEquals(mockedComment.getCreationDate(), comment.getCreationDate());
    }

    @Test
    public void update() throws ServiceException, DAOException {
        mockedCommentService.update(comment);
        verify(mockedCommentDAO).update(comment);
    }

    @Test
    public void delete() throws ServiceException, DAOException {
        mockedCommentService.deleteComment(comment);
        verify(mockedCommentDAO).delete(comment);
    }

    @Test
    public void create() throws ServiceException, DAOException {
        mockedCommentService.addComment(1, comment);
        verify(mockedCommentDAO).create(comment, 1);
    }

}
