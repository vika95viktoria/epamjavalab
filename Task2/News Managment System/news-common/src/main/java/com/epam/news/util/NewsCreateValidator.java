package com.epam.news.util;

import com.epam.news.domain.News;
import com.epam.news.model.NewsModel;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by Viktoryia_Khlystun on 7/14/2016.
 */
public class NewsCreateValidator implements Validator {
    public boolean supports(Class clazz) {
        return News.class.equals(clazz);
    }

    public void validate(Object obj, Errors e) {
        NewsModel newsModel = (NewsModel) obj;
        if (newsModel.getId() < 0) {
            e.rejectValue("id", "negative value");
        }
        if ("".equals(newsModel.getTitle())) {
            e.rejectValue("title", "not present");
        }
        if ("".equals(newsModel.getShortText())) {
            e.rejectValue("shortText", "not present");
        }
        if ("".equals(newsModel.getFullText())) {
            e.rejectValue("fullText", "not present");
        }
        if ("".equals(newsModel.getModificationDate())) {
            e.rejectValue("modificationDate", "not present");
        }
        if (newsModel.getAuthorIds().isEmpty()) {
            e.rejectValue("authorIds", "not present");
        }

    }
}
