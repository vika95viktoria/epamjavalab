package com.epam.news.dao.impl;

import com.epam.news.dao.AbstractDAO;
import com.epam.news.dao.AuthorDAO;
import com.epam.news.domain.Author;
import com.epam.news.domain.News;
import com.epam.news.exception.DAOException;
import org.springframework.jdbc.datasource.DataSourceUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.news.util.DAOConstants.*;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
public class AuthorDAOImpl extends AbstractDAO implements AuthorDAO {
    private static final String SQL_INSERT_AUTHOR_FOR_NEWS_BY_NEWS_ID = "insert into NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) VALUES (?,?)";
    private static final String SQL_INSERT_AUTHOR_FOR_NEWS = "insert into NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) VALUES ((select news_id from news where title = ?),?)";
    private static final String SQL_CREATE_AUTHOR = "insert into author (author_id, author_name) values (AUTHOR_SEQ.nextval,?)";
    private static final String SQL_DELETE_AUTHOR = "update AUTHOR set EXPIRED = ? where AUTHOR_ID = ?";
    private static final String SQL_UPDATE_AUTHOR = "update AUTHOR set AUTHOR_NAME = ? where AUTHOR_ID = ?";
    private static final String SQL_SELECT_AUTHOR_BY_ID = "select * from AUTHOR where AUTHOR_ID = ?";
    private static final String SQL_SELECT_AUTHOR_FOR_NEWS_ID = "select * from author where AUTHOR_ID in(select AUTHOR_ID from NEWS_AUTHOR where NEWS_ID = ?)";
    private static final String SQL_DELETE_AUTHORS_FOR_NEWS = "delete from NEWS_AUTHOR where NEWS_ID = ?";

    /**
     * Adding relation of author and news into database when news is in process of creation
     *
     * @param news
     * @param authorId
     * @throws DAOException
     */
    public void addAuthorToNews(News news, long authorId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_INSERT_AUTHOR_FOR_NEWS);
            statement.setString(1, news.getTitle());
            statement.setLong(2, authorId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Adding relation of author and news into database when news is already created. (Link new author to existing news)
     *
     * @param newsId
     * @param authorId
     * @throws DAOException
     */
    public void addAuthorToNews(long newsId, long authorId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_INSERT_AUTHOR_FOR_NEWS_BY_NEWS_ID);
            statement.setLong(1, newsId);
            statement.setLong(2, authorId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Return the list of authors of news
     *
     * @param newsId
     * @return
     * @throws DAOException
     */
    public List<Author> findAuthorsByNewsId(long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        List<Author> authors = new ArrayList<Author>();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_SELECT_AUTHOR_FOR_NEWS_ID);
            statement.setLong(1, newsId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Author author = new Author();
                author.setId(resultSet.getLong(AUTHOR_ID));
                author.setName(resultSet.getString(AUTHOR_NAME));
                if (resultSet.getDate(EXPIRED) != null) {
                    author.setExpired(resultSet.getDate(EXPIRED));
                }
                authors.add(author);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return authors;
    }

    /**
     * Add new record with author info to database
     *
     * @param author
     * @throws DAOException
     */
    public void create(Author author) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_CREATE_AUTHOR);
            statement.setString(1, author.getName());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Return all author info by id
     *
     * @param authorId
     * @return
     * @throws DAOException
     */
    public Author findAuthorById(long authorId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        Author author = new Author();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_ID);
            statement.setLong(1, authorId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                author.setId(resultSet.getLong(AUTHOR_ID));
                author.setName(resultSet.getString(AUTHOR_NAME));
                if (resultSet.getDate(EXPIRED) != null) {
                    author.setExpired(resultSet.getDate(EXPIRED));
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return author;
    }

    /**
     * Make author expired
     *
     * @param entity
     * @throws DAOException
     */
    public void delete(Author entity) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE_AUTHOR);
            statement.setTimestamp(1, new Timestamp(entity.getExpired().getTime()));
            statement.setLong(2, entity.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Change author's name
     *
     * @param entity
     * @throws DAOException
     */
    public void update(Author entity) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_UPDATE_AUTHOR);
            statement.setString(1, entity.getName());
            statement.setLong(2, entity.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Delete relation of news and all authors when you delete news
     *
     * @param newsId
     * @throws DAOException
     */
    public void deleteAuthorFromNews(long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE_AUTHORS_FOR_NEWS);
            statement.setLong(1, newsId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

}
