package com.epam.news.service;

/**
 * Created by Viktoryia_Khlystun on 6/3/2016.
 */

import com.epam.news.dao.AuthorDAO;
import com.epam.news.dao.CommentDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.dao.TagDAO;
import com.epam.news.dao.impl.AuthorDAOImpl;
import com.epam.news.dao.impl.CommentDAOImpl;
import com.epam.news.dao.impl.NewsDAOImpl;
import com.epam.news.dao.impl.TagDAOImpl;
import com.epam.news.domain.Author;
import com.epam.news.domain.Comment;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.search.SearchCriteria;
import com.epam.news.service.impl.NewsServiceImpl;
import com.epam.news.util.BuilderTestInfo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {
    private static NewsService mockedNewsService;
    private static NewsDAO mockedNewsDAO;
    private static CommentDAO mockedCommentDAO;
    private static AuthorDAO mockedAuthorDAO;
    private static TagDAO mockedTagDAO;
    private static List<News> news = BuilderTestInfo.buildNewsSet();
    private static SearchCriteria searchCriteria = new SearchCriteria();

    @Before
    public void setUp() throws DAOException, ServiceException {
        mockedNewsDAO = mock(NewsDAO.class);
        mockedTagDAO = mock(TagDAO.class);
        mockedCommentDAO = mock(CommentDAO.class);
        mockedAuthorDAO = mock(AuthorDAO.class);
        mockedNewsService = new NewsServiceImpl();
        mockedNewsService.setNewsDAO(mockedNewsDAO);
        mockedNewsService.setTagDAO(mockedTagDAO);
        mockedNewsService.setCommentDAO(mockedCommentDAO);
        mockedNewsService.setAuthorDAO(mockedAuthorDAO);
        when(mockedNewsDAO.findNewsById(1)).thenReturn(BuilderTestInfo.buildNewsSet().get(0));
        when(mockedTagDAO.findTagsByNewsId(1)).thenReturn(BuilderTestInfo.buildTagSet());
        when(mockedAuthorDAO.findAuthorsByNewsId(1)).thenReturn(BuilderTestInfo.buildAuthorSet());
        when(mockedCommentDAO.findCommentsByNewsId(1)).thenReturn(new ArrayList<Comment>());
        when(mockedNewsDAO.findNewsById(2)).thenReturn(news.get(0));
        when(mockedTagDAO.findTagsByNewsId(2)).thenReturn(BuilderTestInfo.buildTagSet());
        when(mockedAuthorDAO.findAuthorsByNewsId(2)).thenReturn(BuilderTestInfo.buildAuthorSet());
        when(mockedCommentDAO.findCommentsByNewsId(2)).thenReturn(BuilderTestInfo.buildCommentSet());
        when(mockedNewsDAO.getIdOfMostCommenting()).thenReturn(BuilderTestInfo.buildSetOfMostCommenting());
        when(mockedNewsService.getNews()).thenReturn(news);
        when(mockedNewsDAO.getCountOfNews()).thenReturn(1000);
        when(mockedNewsDAO.getListOfNews(searchCriteria)).thenReturn(null);

    }

    @After
    public void reset() {
        Mockito.reset(mockedNewsDAO);
    }

    @Test
    public void getNewsById() throws ServiceException, DAOException {
        News mockedNews = mockedNewsService.findNewsById(1L);
        assertEquals(mockedNews.getId(), 1);
        assertEquals(mockedNews.getTitle(), "New title");
        assertEquals(mockedNews.getShortText(), "Short text");
        assertEquals(mockedNews.getFullText(), "Full text");
        assertEquals(mockedNews.getComments().size(), 0);
        assertEquals(mockedNews.getAuthors().get(0).getName(), "John Doe");
        assertEquals(mockedNews.getTags().size(), 2);
        assertEquals(mockedNews.getTags().get(1).getName(), "Cats");
    }

    @Test
    public void getListOfNewsOrderedByComments() throws ServiceException, DAOException {
        List<News> mockedNews = mockedNewsService.getNewsOrderedByComments();
        assertEquals(mockedNews.get(0).getId(), 2);
        assertEquals(mockedNews.get(1).getId(), 1);
        assertEquals(mockedNews.size(), 2);
        assertEquals(mockedNews.get(0).getComments().size(), 2);
    }

    @Test
    public void getNews() throws ServiceException, DAOException {
        List<News> mockedNews = mockedNewsService.getNews();
        assertEquals(mockedNews.size(), 2);
        assertEquals(mockedNews.get(0).getAuthors().get(0).getName(), "John Doe");
    }

    @Test
    public void create() throws ServiceException, DAOException {
        News newsPiece = news.get(0);
        newsPiece.setTags(BuilderTestInfo.buildTagSet());
        newsPiece.setAuthors(BuilderTestInfo.buildAuthorSet());
        mockedNewsService.createNews(newsPiece);
        verify(mockedNewsDAO).create(newsPiece);
        verify(mockedAuthorDAO).addAuthorToNews(newsPiece, newsPiece.getAuthors().get(0).getId());
        verify(mockedTagDAO).addTagToNews(newsPiece, 1);
        verify(mockedTagDAO).addTagToNews(newsPiece, 2);
    }

    @Test
    public void delete() throws ServiceException, DAOException {
        mockedNewsService.deleteNews(1L);
        verify(mockedNewsDAO).delete(1);
        verify(mockedAuthorDAO).deleteAuthorFromNews(1);
        verify(mockedTagDAO).deleteTagsFromNews(1);
        verify(mockedTagDAO).deleteTagsFromNews(1);
    }

    @Test
    public void getCountOfNews() throws ServiceException, DAOException {
        int countOfNews = mockedNewsService.getCountOfNews();
        assertEquals(1000, countOfNews);
    }

    @Test
    public void update() throws ServiceException, DAOException {
        mockedNewsService.updateNews(news.get(0));
        verify(mockedNewsDAO).update(news.get(0));
    }

    @Test
    public void searchNews() throws ServiceException, DAOException {
        List<News> news = mockedNewsService.searchForNews(searchCriteria);
        assertTrue(news == null);
    }

}
