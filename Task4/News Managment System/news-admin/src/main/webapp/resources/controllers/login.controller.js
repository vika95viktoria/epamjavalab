(function () {
    'use strict';

    var newsApp = angular.module('newsApp');
    newsApp.controller('LoginController', ['$scope', '$http', '$location', 'Auth', function ($scope, $http, $location, Auth) {

        authenticate();
        $scope.credentials = {};
        function authenticate(callback) {
            $http.get('/admin/user').success(function (data) {
                if (typeof data.principal !== "undefined" && data.principal.name == $scope.credentials.username) {
                    Auth.setUser($scope.credentials);
                    $scope.user = Auth.isLoggedIn();
                } else {
                    Auth.resetUser();
                }
                callback && callback();
            }).error(function () {
                Auth.resetUser();
                callback && callback();
            });
        }


        $scope.logout = function () {
            Auth.logout();
        }

        $scope.onLogin = function () {
            $http.post('/admin/login', $.param($scope.credentials), {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "X-Login-Ajax-call": 'true'
                }
            }).success(function (data) {
                authenticate(function () {
                    if (Auth.isLoggedIn()) {
                        Auth.setUser($scope.credentials.username);
                        $location.path("/newsList");
                        $scope.error = false;
                    } else {
                        $location.path("/login");
                        $scope.error = true;
                    }
                });
            }).error(function (data) {
                $location.path("/login");
                $scope.error = true;
                Auth.resetUser();
                $scope.credentials.username = '';
                $scope.credentials.password = '';
            })
        };
    }]);
})();
