package com.epam.news.service;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.dao.CommentDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.dao.TagDAO;
import com.epam.news.domain.News;
import com.epam.news.exception.ServiceException;
import com.epam.news.search.SearchCriteria;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
public interface NewsService {
    List<News> getNewsOrderedByComments() throws ServiceException;

    void createNews(News news) throws ServiceException;

    void deleteNews(Long newsId) throws ServiceException;

    News findNewsById(Long newsId) throws ServiceException;

    int getCountOfNews() throws ServiceException;

    void updateNews(News news) throws ServiceException;

    List<News> searchForNews(SearchCriteria searchCriteria) throws ServiceException;

    List<News> getNews() throws ServiceException;

    void setNewsDAO(NewsDAO newsDAO);

    void setTagDAO(TagDAO tagDAO);

    void setAuthorDAO(AuthorDAO authorDAO);

    void setCommentDAO(CommentDAO commentDAO);
}
