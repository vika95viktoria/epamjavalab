package com.epam.news.service;

/**
 * Created by Viktoryia_Khlystun on 6/3/2016.
 */

import com.epam.news.dao.AuthorDAO;
import com.epam.news.dao.CommentDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.dao.TagDAO;
import com.epam.news.domain.Comment;
import com.epam.news.domain.News;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.model.NewsModel;
import com.epam.news.search.SearchCriteria;
import com.epam.news.service.impl.NewsServiceImpl;
import com.epam.news.util.BuilderTestInfo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {
    private static NewsService mockedNewsService;
    private static NewsDAO mockedNewsDAO;
    private static CommentDAO mockedCommentDAO;
    private static AuthorDAO mockedAuthorDAO;
    private static TagDAO mockedTagDAO;
    private static List<News> news = BuilderTestInfo.buildNewsSet();
    private static SearchCriteria searchCriteria = new SearchCriteria();

    @Before
    public void setUp() throws DAOException, ServiceException {
        mockedNewsDAO = mock(NewsDAO.class);
        mockedTagDAO = mock(TagDAO.class);
        mockedCommentDAO = mock(CommentDAO.class);
        mockedAuthorDAO = mock(AuthorDAO.class);
        mockedNewsService = new NewsServiceImpl();
        mockedNewsService.setNewsDAO(mockedNewsDAO);
        mockedNewsService.setTagDAO(mockedTagDAO);
        mockedNewsService.setCommentDAO(mockedCommentDAO);
        mockedNewsService.setAuthorDAO(mockedAuthorDAO);
        when(mockedNewsDAO.findNewsById(1)).thenReturn(BuilderTestInfo.buildNewsSet().get(0));
        when(mockedCommentDAO.findCommentsByNewsId(1)).thenReturn(new ArrayList<Comment>());
        when(mockedNewsDAO.findNewsById(2)).thenReturn(news.get(0));
        when(mockedCommentDAO.findCommentsByNewsId(2)).thenReturn(BuilderTestInfo.buildCommentSet());
        when(mockedNewsDAO.getIdOfMostCommenting(0)).thenReturn(BuilderTestInfo.buildSetOfMostCommenting());
        when(mockedNewsDAO.getCountOfNews()).thenReturn(1000);
        when(mockedNewsDAO.getListOfNews(searchCriteria)).thenReturn(null);

    }

    @After
    public void reset() {
        Mockito.reset(mockedNewsDAO);
    }

    @Test
    public void getNewsById() throws ServiceException, DAOException {
        News mockedNews = mockedNewsService.findNewsById(1L);
        assertEquals(mockedNews.getId(), 1);
        assertEquals(mockedNews.getTitle(), "New title");
        assertEquals(mockedNews.getShortText(), "Short text");
        assertEquals(mockedNews.getFullText(), "Full text");
        assertEquals(mockedNews.getComments().size(), 0);
        assertEquals(mockedNews.getAuthors().get(0).getName(), "John Doe");
        assertEquals(mockedNews.getTags().size(), 2);
        assertEquals(mockedNews.getTags().get(1).getName(), "Cats");
    }



    @Test
    public void create() throws ServiceException, DAOException {
        News newsPiece = news.get(0);
        NewsModel newsModel = new NewsModel();
        newsModel.setId(newsPiece.getId());
        newsModel.setTitle(newsPiece.getTitle());
        newsModel.setShortText(newsPiece.getShortText());
        newsModel.setTagIds(new ArrayList<Long>());
        newsModel.setAuthorIds(new ArrayList<Long>());
        newsModel.setModificationDate("12/07/2016");
        mockedNewsService.createNews(newsModel);
        verify(mockedNewsDAO).create(newsPiece);
    }

    @Test
    public void delete() throws ServiceException, DAOException {
        mockedNewsService.deleteNews(1L);
        verify(mockedNewsDAO).delete(mockedNewsDAO.findNewsById(1));
    }

    @Test
    public void getCountOfNews() throws ServiceException, DAOException {
        int countOfNews = mockedNewsService.getCountOfNews();
        assertEquals(1000, countOfNews);
    }



    @Test
    public void searchNews() throws ServiceException, DAOException {
        List<News> news = mockedNewsService.searchForNews(searchCriteria,1,10);
        assertTrue(news.isEmpty());
    }

}
