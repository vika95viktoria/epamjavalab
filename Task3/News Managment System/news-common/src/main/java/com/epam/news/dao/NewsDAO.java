package com.epam.news.dao;

import com.epam.news.domain.News;
import com.epam.news.search.SearchCriteria;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */
public interface NewsDAO {
    News findNewsById(long id);

    void create(News news);

    void update(News news);

    void delete(News news);

    List<Long> getIdOfMostCommenting(long start);

    int getCountOfNews();

    List<Long> getListOfNews(SearchCriteria searchCriteria);

    List<News> getSubListOfNews(List<Long> ids);

    List<Long> getIdOfMostCommenting(long start, long end, List<Long> ids);

    int getCountOfListNews(SearchCriteria searchCriteria);

    News findNewsByTitle(String title);
}
