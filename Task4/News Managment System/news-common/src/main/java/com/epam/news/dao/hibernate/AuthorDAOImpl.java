package com.epam.news.dao.hibernate;

import com.epam.news.dao.AbstractDAO;
import com.epam.news.dao.AuthorDAO;
import com.epam.news.domain.Author;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/30/2016.
 */

@Repository
@Profile("hibernate")
public class AuthorDAOImpl extends AbstractDAO implements AuthorDAO {

    /**
     * Get count of all authors in system
     *
     * @return
     */
    public int getCountOfAuthors() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(a) from Author as a where a.expired is null");
        int count = ((Long) query.uniqueResult()).intValue();
        return count;
    }


    /**
     * Add new record with author info to database
     *
     * @param author
     */
    public void create(Author author) {
        Session session = sessionFactory.getCurrentSession();
        session.save(author);
    }

    /**
     * Return all author info by id
     *
     * @param authorId
     * @return
     */
    public Author findAuthorById(long authorId) {
        Session session = sessionFactory.getCurrentSession();
        Author author = session.get(Author.class, authorId);
        return author;
    }


    /**
     * Change author's name
     *
     * @param entity
     */
    public void update(Author entity) {
        Session session = sessionFactory.getCurrentSession();
        session.update(entity);
    }


    /**
     * Get list of all authors in system
     *
     * @return
     */
    public List<Author> findAll() {
        Session session = sessionFactory.getCurrentSession();
        List<Author> authors = session.createQuery("from Author").list();
        return authors;
    }

    /**
     * Get list of all authors in system in some limits (for pagination)
     *
     * @param start
     * @return
     */

    public List<Author> findAll(long start) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select a from Author as a where a.expired is null");
        query.setFirstResult((int) start);
        query.setMaxResults(7);
        List<Author> authors = query.list();
        return authors;
    }
}
