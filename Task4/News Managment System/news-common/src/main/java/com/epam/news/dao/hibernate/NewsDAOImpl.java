package com.epam.news.dao.hibernate;

import com.epam.news.dao.AbstractDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.model.NewsModelList;
import com.epam.news.search.SearchCriteria;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/26/2016.
 */

@Repository
@Profile("hibernate")
public class NewsDAOImpl extends AbstractDAO implements NewsDAO {

    private static final String SEARCH_NEWS_BY_TAGS = "select count(c.id) as countc,n from News n join n.tags t left outer join n.comments c where  t.name in (:tags) group by n.id,n.version,n.title,n.shortText,n.fullText,n.creationDate,n.modificationDate order by countc desc,n.modificationDate desc ";
    private static final String SEARCH_NEWS_BY_AUTHOR = "select count(c.id) as countc,n from News n join n.authors a  left outer join n.comments c where a.name = :name  group by n.id,n.version,n.title,n.shortText,n.fullText,n.creationDate,n.modificationDate order by countc desc,n.modificationDate desc ";
    private static final String SEARCH_NEWS_BY_AUTHOR_AND_TAGS = "select count(c.id) as countc,n from News n join n.authors a join n.tags t left outer join n.comments c where a.name = :name and t.name in (:tags) group by n.id,n.version,n.title,n.shortText,n.fullText,n.creationDate,n.modificationDate order by countc desc,n.modificationDate desc ";
    private static final String COUNT_NEWS_BY_TAGS = "select count(n)  from News n join n.tags t where  t.name in (:tags)";
    private static final String COUNT_NEWS_BY_AUTHOR = "select count(n)  from News n join n.authors a  where a.name = :name";
    private static final String COUNT_NEWS_BY_AUTHOR_AND_TAGS = "select count(n) from News n join n.authors a join n.tags t where a.name = :name and t.name in (:tags)";

    /**
     * Return news info without comments, tags and authors. Only news description
     *
     * @param id
     * @return
     */

    public News findNewsById(long id) {
        Session session = sessionFactory.getCurrentSession();
        News news = session.get(News.class, id);
        return news;
    }

    /**
     * Find news by title
     *
     * @param title
     * @return
     */
    public News findNewsByTitle(String title) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select n from News as n where n.title = :newsTitle");
        query.setParameter("newsTitle", title);
        News news = (News) query.uniqueResult();
        return news;
    }


    /**
     * Create a record in database with news info, only news description? not including tags, authors and comments
     *
     * @param news
     */
    public long create(News news) {
        Session session = sessionFactory.getCurrentSession();
        session.save(news);
        return news.getId();
    }

    /**
     * Update news text, title and change modification date
     *
     * @param news
     */
    public long update(News news) {
        Session session = sessionFactory.getCurrentSession();
        session.update(news);
        return news.getId();
    }

    /**
     * Delete news by id from table with comments deleting on cascade
     *
     * @param news
     */
    public void delete(News news) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(news);
    }

    public void delete(Long[] newsIds) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("delete from News n where n.id in (:newsIds)");
        query.setParameterList("newsIds", newsIds);
        query.executeUpdate();
    }

    /**
     * Return sorted list of newsIds desc from most commenting
     *
     * @return
     */
    public List<NewsModelList> getMostCommenting(int start) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(c.id) as countc,n from News n  left outer join n.comments c  group by n.id,n.version,n.title,n.shortText,n.fullText,n.creationDate,n.modificationDate order by countc desc,n.modificationDate desc");
        query.setFirstResult(start);
        query.setMaxResults(4);
        List<Object[]> resultList = query.list();
        List<NewsModelList> result = new ArrayList<>();
        for (Object[] object : resultList) {
            result.add(new NewsModelList((News) object[1], ((Long) object[0]).intValue()));
        }
        return result;
    }

    public News getNextNews(int position, int index) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(c.id) as countc,n from News n  left outer join n.comments c  group by n.id,n.version,n.title,n.shortText,n.fullText,n.creationDate,n.modificationDate order by countc desc,n.modificationDate desc");
        if (index == 1) {
            query.setFirstResult(position);
        } else {
            query.setFirstResult(position - 2);
        }
        query.setMaxResults(1);
        List<Object[]> resultList = query.list();
        News news = (News) resultList.get(0)[1];
        Hibernate.initialize(news.getComments());
        return news;
    }

    public News getNextNews(SearchCriteria searchCriteria, int position, int index) {
        Query query = getQuery(searchCriteria, 1);
        if (index == 1) {
            query.setFirstResult(position);
        } else {
            query.setFirstResult(position - 2);
        }
        query.setMaxResults(1);
        List<Object[]> resultList = query.list();
        News news = (News) resultList.get(0)[1];
        Hibernate.initialize(news.getComments());
        return news;
    }


    /**
     * Return the count of records in database in table news
     *
     * @return
     */
    public int getCountOfNews() {
        Session session = sessionFactory.getCurrentSession();
        Criteria crit = session.createCriteria(News.class);
        crit.setProjection(Projections.rowCount());
        int count = ((Long) crit.uniqueResult()).intValue();
        return count;
    }

    /**
     * Return list of news fitted some search parameters like author name or tags
     *
     * @param searchCriteria
     * @return
     */
    public List<NewsModelList> getListOfNews(SearchCriteria searchCriteria, int start, int end) {
        Query query = getQuery(searchCriteria, 1);
        query.setFirstResult(start);
        query.setMaxResults(end - start);
        List<Object[]> resultList = query.list();
        List<NewsModelList> result = new ArrayList<>();
        for (Object[] object : resultList) {
            result.add(new NewsModelList((News) object[1], ((Long) object[0]).intValue()));
        }
        return result;

    }

    /**
     * return count of news according to search criteria
     *
     * @param searchCriteria
     * @return
     */
    public int getCountOfListNews(SearchCriteria searchCriteria) {
        int count = ((Long) getQuery(searchCriteria, 0).uniqueResult()).intValue();
        return count;
    }


    private Query getQuery(SearchCriteria searchCriteria, int a) {
        if (a == 1) {
            return buildQuery(searchCriteria, SEARCH_NEWS_BY_TAGS, SEARCH_NEWS_BY_AUTHOR, SEARCH_NEWS_BY_AUTHOR_AND_TAGS);
        } else {
            return buildQuery(searchCriteria, COUNT_NEWS_BY_TAGS, COUNT_NEWS_BY_AUTHOR, COUNT_NEWS_BY_AUTHOR_AND_TAGS);
        }
    }

    private Query buildQuery(SearchCriteria searchCriteria, String tagQuery, String authorQuery, String tagAuthorQuery) {
        Session session = sessionFactory.getCurrentSession();
        Query query;
        if (searchCriteria.getTags() != null) {
            List<String> tagNames = new ArrayList<>();
            for (Tag tag : searchCriteria.getTags()) {
                tagNames.add(tag.getName());
            }
            if (searchCriteria.getAuthor() != null && !"".equals(searchCriteria.getAuthor())) {
                String author = searchCriteria.getAuthor().getName();
                query = session.createQuery(tagAuthorQuery);
                query.setParameter("name", author);
                query.setParameterList("tags", tagNames);
            } else {
                query = session.createQuery(tagQuery);
                query.setParameterList("tags", tagNames);
            }
        } else {
            String author = searchCriteria.getAuthor().getName();
            query = session.createQuery(authorQuery);
            query.setParameter("name", author);
        }
        return query;
    }


}
