package com.epam.news.service;

import com.epam.news.dao.TagDAO;
import com.epam.news.domain.Tag;
import com.epam.news.exception.ServiceException;

import java.util.List;

/**
 * Created by Viktoryia_Khlystun on 5/31/2016.
 */
public interface TagService {
    void addTagsToNews(List<Long> tagIds, long newsId) throws ServiceException;

    void create(Tag tag) throws ServiceException;

    Tag findTagById(long tagId) throws ServiceException;

    void deleteTagsFromNews(long newsId) throws ServiceException;

    void setTagDAO(TagDAO tagDAO);
}
